import sys, os, cmd, threading, code, re
from xml.dom.minidom import parseString
from optparse import OptionParser

option_0 = { 'name' : ('-d', '--directory'),  'help' : 'directory : use this directory', 'nargs' : 1 }
option_1 = { 'name' : ('-o', '--output'),  'help' : 'output : output folder name', 'nargs' : 1 }
options = [option_0,option_1]

# the path for apks inside the root folder
oat_paths = []

def run_oat_analysis(oat, output):
	oatName = os.path.splitext(os.path.basename(oat))[0]
	print "Preprocessing " + oat
	print "...... dextra_ing"
	dextra_cmd = "./dextra.ELF64 -dextract " + oat
    	print dextra_cmd
	os.system(dextra_cmd)
	mkdir_cmd = "mkdir " + output
	os.system(mkdir_cmd)
	for name in os.walk("./").next()[2]:
		if name.endswith(".dex"):
			print name
        		dexname = name.split('@')[2][:-3] + "dex"
			if "framework.jar:classes2" in dexname:
				dexname = "framework2.dex"
			mv_cmd = "mv " + name + " " + output + "/" + dexname
			os.system(mv_cmd)


# Parse the directory to get all the APKs
def list_paths(root):
	for path, dirs, files in os.walk(root, topdown=False):
    		for name in files:
			if name.endswith(".oat"):
        			oat_paths.append(os.path.join(path, name))


# Where everything begins
def main(options, arguments) :
	# set the root folder
	root = options.directory
	output = options.output
	list_paths(root)
	for oat in oat_paths:
		run_oat_analysis(oat, output)
	

# parse the input and start the main() function
if __name__ == "__main__" :
    parser = OptionParser()
    for option in options :
        param = option['name']
        del option['name']
        parser.add_option(*param, **option)

    options, arguments = parser.parse_args()
    sys.argv[:] = arguments
    main(options, arguments)
