import sys, os, cmd, threading, code, re
from xml.dom.minidom import parseString
from optparse import OptionParser

option_0 = { 'name' : ('-d', '--directory'),  'help' : 'directory : use this directory', 'nargs' : 1 }
options = [option_0]

# the path for apks inside the root folder
dex_paths = []

def run_dex_analysis(dex,imageName):
	dexName = os.path.splitext(os.path.basename(dex))[0]
    	print "Preprocessing " + dex
    	print "...... decompiling" 
	decomp_cmd = "sh ./dex2jar-2.0/d2j-dex2jar.sh -f " + dex + " -o ../../badImages/" + imageName + ".zip/JARs/" + dexName  + ".jar"
    	print decomp_cmd
	os.system(decomp_cmd) 


# Parse the directory to get all the APKs
def list_paths(root):
	for path, dirs, files in os.walk(root, topdown=False):
    		for name in files:
			if name.endswith(".dex"):
        			dex_paths.append(os.path.join(path, name))


# Where everything begins
def main(options, arguments) :
	# set the root folder
	root = options.directory
	list_paths(root)
	imageName = os.path.splitext(os.path.basename(root))[0]
	for dex in dex_paths:
		run_dex_analysis(dex,imageName)
	

# parse the input and start the main() function
if __name__ == "__main__" :
    parser = OptionParser()
    for option in options :
        param = option['name']
        del option['name']
        parser.add_option(*param, **option)

    options, arguments = parser.parse_args()
    sys.argv[:] = arguments
    main(options, arguments)
