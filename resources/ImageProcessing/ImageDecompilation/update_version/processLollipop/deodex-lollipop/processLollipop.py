import sys, os, cmd, threading, code, re
from xml.dom.minidom import parseString
from optparse import OptionParser

option_0 = { 'name' : ('-d', '--directory'),  'help' : 'directory : use this directory', 'nargs' : 1 }
options = [option_0]

# the path for apks inside the root folder
jar_paths = []
jar_names = []
input_paths = []
app_names = []
framework_names = []


def run_image_analysis(image, root, missing_jars):
	print "trying to patch " + image + " with following jars: "
	print missing_jars
	for jar in missing_jars:
		if "services.jar" in jar or "framework" in jar:
			oatcmd = "./oat2dex " + image + "JARs/" + jar[:-3] + "odex" + " ../../framework/odex"		
			dex2jarcmd = "sh ../../dex2jar-2.0/d2j-dex2jar.sh " + image + "JARs/" + jar[:-3] + "dex" + " -o " + root + "/framework/" + jar
			os.system(oatcmd)
			os.system(dex2jarcmd)
		else:
			oatcmd = "./oat2dex " + image + "APKs/" + jar[:-3] + "odex" + " ../../framework/odex"		
			dex2jarcmd = "sh ../../dex2jar-2.0/d2j-dex2jar.sh " + image + "APKs/" + jar[:-3] + "dex" + " -o " + root + "/apps/" + jar
			os.system(oatcmd)
			os.system(dex2jarcmd)
	rmcmd = "rm -rf ../../framework/*"
	os.system(rmcmd)

def run_input_analysis(input_file,imageName):
	f = open(input_file, 'r')
	for line in f.readlines():
		if "Application" in line:
			if "apps" in line:
				app_names.append(line.split('/')[1].rstrip())
			if "framework" in line:
				framework_names.append(line.split('/')[1].rstrip())

# Parse the directory to get all the APKs
def list_paths(root):
	for path, dirs, files in os.walk(root, topdown=False):
    		for name in files:
			if name.endswith(".jar"):
        			jar_paths.append(os.path.join(path, name))
				jar_names.append(name)
			if "input.txt" in name:
        			input_paths.append(os.path.join(path, name))


def hasOAT(root):
	for path, dirs, files in os.walk(root, topdown=False):
    		for name in files:
			if "boot.oat" in name:
				oatfile = os.path.join(path, name)
				cpcmd = "cp " + oatfile + " ../../framework/"
				os.system(cpcmd)
				oatcmd = "./oat2dex boot ../../framework/boot.oat"
				os.system(oatcmd)
        			return True
	return False


def fixInput(missing_jars):
	for input_file in input_paths:
		f = open(input_file, 'r')
		f1 = open(input_file[:-9] + "newInput.txt",'w')
		jars = []
		others = []
		for line in f.readlines():
			flag = False
			for jar in missing_jars:
				if jar in line:
					flag = True
			if not flag: 
				if "Primordial" in line:
					others.append(line)
				else:
					jars.append(line)
		f.close()		
		for other in others:
			f1.write(other)
		for jar in jars:
			f1.write(jar)
		f1.close()
		mvcmd = "mv " + input_file[:-9] + "newInput.txt " + input_file
		print mvcmd
		os.system(mvcmd)
			

# Where everything begins
def main(options, arguments) :
	# set the root folder
	root = options.directory
	list_paths(root)
	imageName = os.path.basename(root)
	for input_file in input_paths:
		run_input_analysis(input_file,imageName)
	merge_names = app_names + framework_names 
	missing_jars = [item for item in merge_names if item not in jar_names]
	# there are missing jars, we need to generate them using the processLollipop utility
	print imageName
	image = "../../../../badImages/" + imageName + "/"
	print image
	print missing_jars
	if "services.jar" in missing_jars:
		if hasOAT(image):
			run_image_analysis(image, root, missing_jars)
#	fixInput(missing_jars)

# parse the input and start the main() function
if __name__ == "__main__" :
    parser = OptionParser()
    for option in options :
        param = option['name']
        del option['name']
        parser.add_option(*param, **option)

    options, arguments = parser.parse_args()
    sys.argv[:] = arguments
    main(options, arguments)
