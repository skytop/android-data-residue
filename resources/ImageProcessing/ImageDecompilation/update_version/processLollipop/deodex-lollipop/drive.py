import sys, os, cmd, threading, code, re
from xml.dom.minidom import parseString
from optparse import OptionParser

option_0 = { 'name' : ('-d', '--directory'),  'help' : 'directory : use this directory', 'nargs' : 1 }
options = [option_0]

# the path for images inside the root folder
image_paths = []

def run_image_analysis(image):
	imageName = os.path.splitext(os.path.basename(image))[0]
	print image
	print imageName
	lollipopcmd = "python processLollipop.py -d " + image
	os.system(lollipopcmd)
	print lollipopcmd


# Parse the directory to get all the APKs
def list_paths(root):
    	for dir in os.walk(root).next()[1]:
       		image_paths.append(os.path.join(root, dir))



# Where everything begins
def main(options, arguments) :
	# set the root folder
	root = options.directory
	list_paths(root)
	for image in image_paths:
		run_image_analysis(image)
	

# parse the input and start the main() function
if __name__ == "__main__" :
    parser = OptionParser()
    for option in options :
        param = option['name']
        del option['name']
        parser.add_option(*param, **option)

    options, arguments = parser.parse_args()
    sys.argv[:] = arguments
    main(options, arguments)
