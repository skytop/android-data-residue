import sys, os, cmd, threading, code, re
from xml.dom.minidom import parseString
from optparse import OptionParser

option_0 = { 'name' : ('-d', '--directory'),  'help' : 'directory : use this directory', 'nargs' : 1 }
option_1 = { 'name' : ('-f', '--framework'),  'help' : 'framework : use for odex files', 'nargs' : 1 }
option_2 = { 'name' : ('-o', '--output'),  'help' : 'output : output folder name', 'nargs' : 1 }
options = [option_0,option_1,option_2]

# the path for apks inside the root folder
jar_paths = []
jar_pure_name = []
odex_paths = []

def run_jar_analysis(jar, framework, output, valid):
	jarName = os.path.splitext(os.path.basename(jar))[0]
    	if "framework" not in jarName and "services" not in jarName:
        	return
	odex = jar[:-3] + "odex"
	print "Preprocessing " + jar
	print "...... decompiling"
    	if odex in odex_paths:
            	odex_paths.remove(odex)
		print jar + " <-> " + odex
		baksmali_cmd = "java -jar baksmali-2.1.1.jar -d " + framework + " -c " + framework + "/boot.oat -x " + odex
  	      	print baksmali_cmd
		os.system(baksmali_cmd)
		smali_cmd = "java -jar smali-2.1.1.jar -o classes.dex out"
        	print smali_cmd
		os.system(smali_cmd) 
		decomp_cmd = "sh ./dex2jar-2.0/d2j-dex2jar.sh classes.dex -o ./intermediate/" + jarName + ".jar"
        	print decomp_cmd
		os.system(decomp_cmd) 
		remove_cmd = "rm -rf ./out"
        	print remove_cmd
		os.system(remove_cmd)
		remove_cmd = "rm classes.dex"
    		print remove_cmd
		os.system(remove_cmd)
	else:
		decomp_cmd = "cp " + jar + " ./intermediate/" + jarName + ".jar"
        	print decomp_cmd
		os.system(decomp_cmd)
	cp_cmd = "cp ./intermediate/" + jarName + ".jar " + output + "framework/"
    	print cp_cmd
	os.system(cp_cmd)
	with open("./" + output + "/input.txt", "a") as inputfile:
		inputfile.write("Application,Java,jarFile,framework/" + jarName + ".jar\n")
	with open("./" + output + "/frameworkInput.txt", "a") as inputfile:
		inputfile.write("Application,Java,jarFile,framework/" + jarName + ".jar\n")

def run_odex_analysis(odex, framework, output, valid):
	odexName = os.path.splitext(os.path.basename(odex))[0]
	if not valid:
		return
    	if "framework" not in odexName and "services" not in odexName:
        	return
    	print "Preprocessing " + odex
    	print "...... decompiling"
	baksmali_cmd = "java -jar baksmali-2.1.1.jar -d " + framework + " -c " + framework + "/boot.oat -x " + odex
    	print baksmali_cmd
	os.system(baksmali_cmd)
	smali_cmd = "java -jar smali-2.1.1.jar -o classes.dex out"
    	print smali_cmd
	os.system(smali_cmd) 
	decomp_cmd = "sh ./dex2jar-2.0/d2j-dex2jar.sh classes.dex -o ./intermediate/" + odexName  + ".jar"
    	print decomp_cmd
	os.system(decomp_cmd) 
	remove_cmd = "rm -rf ./out"
    	print remove_cmd
	os.system(remove_cmd)
	remove_cmd = "rm classes.dex"
    	print remove_cmd
	os.system(remove_cmd)
	cp_cmd = "cp ./intermediate/" + odexName + ".jar " + output + "framework/"
    	print cp_cmd
	os.system(cp_cmd)
	with open("./" + output + "/input.txt", "a") as inputfile:
		inputfile.write("Application,Java,jarFile,framework/" + odexName + ".jar\n")
	with open("./" + output + "/frameworkInput.txt", "a") as inputfile:
		inputfile.write("Application,Java,jarFile,framework/" + odexName + ".jar\n")

# Parse the directory to get all the APKs
def list_paths(root):
	for path, dirs, files in os.walk(root, topdown=False):
    		for name in files:
			if name.endswith(".jar"):
        			jar_paths.append(os.path.join(path, name))
				jar_pure_name.append(name)
			if name.endswith(".odex"):
        			odex_paths.append(os.path.join(path, name))

# Parse the directory to get all the APKs
def validate_framework_path():
	if "core.jar" in jar_pure_name and "bouncycastle.jar" in jar_pure_name and "ext.jar" in jar_pure_name and "framework.jar" in jar_pure_name and "android.policy.jar" in jar_pure_name and "services.jar" in jar_pure_name and "core-junit.jar" in jar_pure_name:
		return True
	else:
		return False

# Where everything begins
def main(options, arguments) :
	# set the root folder
	root = options.directory
	framework_path = options.framework
	output = options.output
	mkdir_cmd = "mkdir " + output
	os.system(mkdir_cmd)
	os.system(mkdir_cmd + "/framework")
	list_paths(root)
	with open("./" + output + "/input.txt", "a") as inputfile:
		inputfile.write("Primordial,Java,stdlib,none\n")
 		inputfile.write("Primordial,Java,jarFile,primordial.jar.model\n")
	with open("./" + output + "/frameworkInput.txt", "a") as inputfile:
		inputfile.write("Primordial,Java,stdlib,none\n")
 		inputfile.write("Primordial,Java,jarFile,primordial.jar.model\n")
	valid = validate_framework_path()
	print valid
	for jar in jar_paths:
		run_jar_analysis(jar, framework_path, output, valid)
    	for odex in odex_paths:
        	run_odex_analysis(odex, framework_path, output, valid)
	remove_cmd = "rm ./intermediate/*.*"
	os.system(remove_cmd)
	

# parse the input and start the main() function
if __name__ == "__main__" :
    parser = OptionParser()
    for option in options :
        param = option['name']
        del option['name']
        parser.add_option(*param, **option)

    options, arguments = parser.parse_args()
    sys.argv[:] = arguments
    main(options, arguments)
