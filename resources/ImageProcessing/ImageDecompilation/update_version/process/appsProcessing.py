import sys, os, cmd, threading, code, re
from xml.dom.minidom import parseString
from optparse import OptionParser

option_0 = { 'name' : ('-d', '--directory'),  'help' : 'directory : use this directory', 'nargs' : 1 }
option_1 = { 'name' : ('-f', '--framework'),  'help' : 'framework : use for odex files', 'nargs' : 1 }
option_2 = { 'name' : ('-o', '--output'),  'help' : 'output : output folder name', 'nargs' : 1 }
options = [option_0,option_1,option_2]

# the path for apks inside the root folder
apk_paths = []
odex_paths = []
jar_pure_name = []

system_permissions = [
"android.permission.ACCESS_CHECKIN_PROPERTIES",
"android.permission.ACCOUNT_MANAGER",
"android.permission.BIND_APPWIDGET",
"android.permission.BROADCAST_PACKAGE_REMOVED",
"android.permission.BROADCAST_SMS",
"android.permission.BROADCAST_WAP_PUSH",
"android.permission.CALL_PRIVILEGED",
"android.permission.CAPTURE_AUDIO_OUTPUT",
"android.permission.CAPTURE_SECURE_VIDEO_OUTPUT",
"android.permission.CAPTURE_VIDEO_OUTPUT",
"android.permission.CHANGE_COMPONENT_ENABLED_STATE",
"android.permission.CONTROL_LOCATION_UPDATES",
"android.permission.DELETE_CACHE_FILES",
"android.permission.DELETE_PACKAGES",
"android.permission.DIAGNOSTIC",
"android.permission.DUMP",
"android.permission.FACTORY_TEST",
"android.permission.INSTALL_LOCATION_PROVIDER",
"android.permission.INSTALL_PACKAGES",
"android.permission.LOCATION_HARDWARE",
"android.permission.MASTER_CLEAR",
"android.permission.MEDIA_CONTENT_CONTROL",
"android.permission.MODIFY_PHONE_STATE",
"android.permission.MOUNT_FORMAT_FILESYSTEMS",
"android.permission.MOUNT_UNMOUNT_FILESYSTEMS",
"android.permission.READ_FRAME_BUFFER",
"android.permission.READ_INPUT_STATE",
"android.permission.READ_LOGS",
"android.permission.REBOOT",
"android.permission.SEND_RESPOND_VIA_MESSAGE",
"android.permission.SET_ALWAYS_FINISH",
"android.permission.SET_ANIMATION_SCALE",
"android.permission.SET_DEBUG_APP",
"android.permission.SET_PROCESS_LIMIT",
"android.permission.SET_TIME",
"android.permission.SIGNAL_PERSISTENT_PROCESSES",
"android.permission.STATUS_BAR",
"android.permission.UPDATE_DEVICE_STATS",
"android.permission.WRITE_APN_SETTINGS",
"android.permission.WRITE_GSERVICES",
"android.permission.WRITE_SECURE_SETTINGS"]

bind_permissions = [
"android.permission.BIND_ACCESSIBILITY_SERVICE",
"android.permission.BIND_CHOOSER_TARGET_SERVICE",
"android.permission.BIND_DREAM_SERVICE",
"android.permission.BIND_INCALL_SERVICE",
"android.permission.BIND_INPUT_METHOD",
"android.permission.BIND_MIDI_DEVICE_SERVICE",
"android.permission.BIND_NFC_SERVICE",
"android.permission.BIND_NOTIFICATION_LISTENER_SERVICE",
"android.permission.BIND_REMOTEVIEWS",
"android.permission.BIND_TELECOM_CONNECTION_SERVICE",
"android.permission.BIND_TEXT_SERVICE",
"android.permission.BIND_TV_INPUT",
"android.permission.BIND_VOICE_INTERACTION",
"android.permission.BIND_VPN_SERVICE",
"android.permission.BIND_WALLPAPER"
]


def run_apk_analysis(apk, framework, output):
	print "Preprocessing " + apk
	print "...... decompiling"
	jar = os.path.splitext(os.path.basename(apk))[0]
	odex = apk[:-3] + "odex"
	if odex in odex_paths:
		print apk + " -> " + odex
		baksmali_cmd = "java -jar baksmali-2.1.1.jar -d " + framework + " -c " + framework + "/boot.oat -x " + odex
		os.system(baksmali_cmd)
		smali_cmd = " java -jar smali-2.1.1.jar -o classes.dex out"
		os.system(smali_cmd) 
		decomp_cmd = "sh ./dex2jar-2.0/d2j-dex2jar.sh classes.dex -o ./intermediate/" + jar  + ".jar"
		os.system(decomp_cmd) 
		remove_cmd = "rm -rf ./out"
		os.system(remove_cmd)
		remove_cmd = "rm classes.dex"
		os.system(remove_cmd)
	else:
		decomp_cmd = "sh ./dex2jar-2.0/d2j-dex2jar.sh " + apk + " -o ./intermediate/" + jar  + ".jar"
		os.system(decomp_cmd)
	print "...... extracting manfest"
	apktool_cmd = "java -jar apktool_2.0.3.jar d " + apk + " -o ./" + jar + "/"
	os.system(apktool_cmd)
	print "...... cleaning up"
	mv_cmd = "mv ./" + jar + "/AndroidManifest.xml ./intermediate/" + jar + ".xml"
	os.system(mv_cmd)
	remove_cmd = "rm -rf ./"  + jar
	os.system(remove_cmd)	
	services = parseManifest(jar)
	if len(services)>0:
		print services
		cp_cmd = "cp ./intermediate/" + jar + ".jar ./" + output + "apps/"
		os.system(cp_cmd)
		with open("./" + output + "/input.txt", "a") as inputfile:
    			inputfile.write("Application,Java,jarFile,apps/" + jar + ".jar\n")
		with open("./" + output + "/appservices.txt", "a") as servicefile:
			for service in services:
    				servicefile.write("L" + service.replace(".","/") + "\n")
		

def parseManifest(manifest):
	services = []
	with open('./intermediate/' + manifest + '.xml','r') as f:
		data = f.read()
		dom = parseString(data)
		# application package name
		application = dom.getElementsByTagName('manifest')[0]
		package = application.attributes["package"].value
		# all exported services
		nodes = dom.getElementsByTagName('service')
		for node in nodes:
			#print node.toxml()
			keys = node.attributes.keys()
			permission = []
			name = ""
			enabled = True;
			exported = False;
			Filters = False;
			if "android:name" in keys:
				value = node.attributes["android:name"].value
				if value.startswith('.'):
					name = package + value
				elif "." not in value:
					name = package + "." + value
				else:
					name = value
			if "android:exported" in keys:
				value = node.attributes["android:exported"].value
				if value == "true":
					exported = True
			if "android:enabled" in keys:
				value = node.attributes["android:enabled"].value
				if value == "false":
					enabled = False
			if "android:permission" in keys:
				permission.append(node.attributes["android:permission"].value)
			for filters in node.childNodes:				
				if filters.nodeName == "intent-filter":
					Filters = True
			if Filters:
				exported = True
			print exported
			print enabled
			print Filters
			print permission
			print evaluatePermission(permission)
			if "android.permission.ACCESS_DOWNLOAD_MANAGER" in permission:
				services.append(name)
			elif exported and enabled and evaluatePermission(permission):
				services.append(name)
	return services

def evaluatePermission(permissions):
	for permission in permissions:
		if permission in bind_permissions:
			return False
	return True


def list_framework_paths(root):
	for path, dirs, files in os.walk(root, topdown=False):
    		for name in files:
			if name.endswith(".jar"):
				jar_pure_name.append(name)



# Parse the directory to get all the APKs
def list_apk_paths(root):
	for path, dirs, files in os.walk(root, topdown=False):
    		for name in files:
			if name.endswith(".apk"):
        			apk_paths.append(os.path.join(path, name))
			if name.endswith(".odex"):
        			odex_paths.append(os.path.join(path, name))


# Where everything begins
def main(options, arguments) :
	# set the root folder
	root = options.directory
	framework_path = options.framework
	list_framework_paths(framework_path)
	output = options.output
	mkdir_cmd = "mkdir " + output
	os.system(mkdir_cmd)
	os.system(mkdir_cmd + "/apps")
	list_apk_paths(root)
	for apk in apk_paths:
		run_apk_analysis(apk, framework_path, output)
	remove_cmd = "rm ./intermediate/*.*"
	os.system(remove_cmd)
	

# parse the input and start the main() function
if __name__ == "__main__" :
    parser = OptionParser()
    for option in options :
        param = option['name']
        del option['name']
        parser.add_option(*param, **option)

    options, arguments = parser.parse_args()
    sys.argv[:] = arguments
    main(options, arguments)
