import sys, os, cmd, threading, code, re
from xml.dom.minidom import parseString
from optparse import OptionParser

option_0 = { 'name' : ('-d', '--directory'),  'help' : 'directory : use this directory', 'nargs' : 1 }
options = [option_0]

# the path for images inside the root folder
image_paths = []
done_images = []

def run_image_analysis(image):
	imageName = os.path.splitext(os.path.basename(image))[0]
	#print image
	#print imageName
	jarCmd = "python jarsProcessing.py -d " + image + "/JARs/ -f " + image + "/JARs/ -o " + imageName + "/"
	os.system(jarCmd)
	apkCmd = "python appsProcessing.py -d " + image + "/APKs/ -f " + image + "/JARs/ -o " + imageName + "/"
	os.system(apkCmd)

# Parse the directory to get all the APKs
def list_paths(root):
	for dir in os.walk("../update/result/").next()[1]:
       		done_images.append(dir)
    	for dir in os.walk(root).next()[1]:
		if dir not in done_images and "JARs" not in dir and "APKs" not in dir:
       			image_paths.append(os.path.join(root, dir))
	

# Where everything begins
def main(options, arguments) :
	# set the root folder
	root = options.directory
	list_paths(root)
	print image_paths
	for image in image_paths:
		run_image_analysis(image)
	

# parse the input and start the main() function
if __name__ == "__main__" :
    parser = OptionParser()
    for option in options :
        param = option['name']
        del option['name']
        parser.add_option(*param, **option)

    options, arguments = parser.parse_args()
    sys.argv[:] = arguments
    main(options, arguments)
