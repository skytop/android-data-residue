import sys
import subprocess
import glob
import ntpath
import datetime
import os
import time
import zipfile


default_dir = os.getcwd()			# name dir that script is run from
imgs_dir = os.getcwd() + sys.argv[1] 		# name dir to store all image files


for img_dir in os.listdir(imgs_dir):
	os.chdir(imgs_dir)
	for jar_dir in os.listdir(img_dir):
		if "JARs" in jar_dir:
			print jar_dir
			os.chdir(img_dir)
			for odex in os.listdir(jar_dir):
				if odex.endswith(".odex"):	
					jar = odex[:-4]+"jar"
					if jar not in os.listdir(jar_dir):
						print "mv ./JARs/" + odex + " ./APKs/" + odex
						subprocess.call(["mv", "./JARs/" + odex, "./APKs/" + odex])
	
