import os
import re
import sys
import glob
import ntpath
import xml.dom.minidom

def get_pkg_act_name(eachrom, pkg_out, act_out, svs_out, auth_out):
    xml_list = glob.glob(eachrom+"/*")
    for eachfile in xml_list:
        if os.path.getsize(eachfile) == 0:
            continue
        dom = xml.dom.minidom.parse(eachfile)
        root = dom.documentElement
        pkgName = root.getAttribute('package')
        pkg_out.write(pkgName + '\n')
        
        actions = root.getElementsByTagName('action')
        for action in actions:
            if action.parentNode.parentNode.nodeName == 'activity' or action.parentNode.parentNode.nodeName == 'activity-alias':
                act_out.write(action.getAttribute('android:name') + '\n')
            if action.parentNode.parentNode.nodeName == 'service':
                svs_out.write(action.getAttribute('android:name') + '\n')

        providers = root.getElementsByTagName('provider')
        for provider in providers:
            auth_out.write(provider.getAttribute('android:authorities') + '\n')
    return
  

        
print "Usage: please input Manifest file directory"
if not os.path.exists('defined_sources_roms'):
    cmd = "mkdir defined_sources_roms"
    print cmd
    os.system(cmd)

rom_list = glob.glob(sys.argv[1]+"/*")
for eachrom in rom_list:
    print eachrom
    romname = ntpath.basename(eachrom)
    cmd = "mkdir defined_sources_roms/"+romname
    os.system(cmd)
    pkg_out_name = "defined_sources_roms/"+romname+"/pkgName.txt"
    act_out_name = "defined_sources_roms/"+romname+"/actName_act.txt"
    svs_out_name = "defined_sources_roms/"+romname+"/actName_svs.txt"
    auth_out_name = "defined_sources_roms/"+romname+"/authName.txt"

    pkg_out = open(pkg_out_name,'a')
    act_out = open(act_out_name,'a')
    svs_out = open(svs_out_name,'a')
    auth_out = open(auth_out_name,'a')
    
    get_pkg_act_name(eachrom, pkg_out, act_out, svs_out, auth_out)
