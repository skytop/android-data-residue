import sys, os, cmd, threading, code, re
from xml.dom.minidom import parseString
from optparse import OptionParser

option_0 = { 'name' : ('-d', '--directory'),  'help' : 'directory : use this directory', 'nargs' : 1 }
options = [option_0]

# the path for apks inside the root folder
jar_paths = []
jar_names = []
input_paths = []
app_names = []
framework_names = []

def run_input_analysis(input_file,imageName):
	f = open(input_file, 'r')
	for line in f.readlines():
		if "Application" in line:
			if "apps" in line:
				app_names.append(line.split('/')[1].rstrip())
			if "framework" in line:
				framework_names.append(line.split('/')[1].rstrip())

# Parse the directory to get all the APKs
def list_paths(root):
	for path, dirs, files in os.walk(root, topdown=False):
    		for name in files:
			if name.endswith(".jar"):
        			jar_paths.append(os.path.join(path, name))
				jar_names.append(name)
			if "input.txt" in name:
        			input_paths.append(os.path.join(path, name))

def fixInput(missing_jars):
	for input_file in input_paths:
		f = open(input_file, 'r')
		f1 = open(input_file[:-9] + "newInput.txt",'w')
		for line in f.readlines():
			flag = False
			if "+" in line:
				flag = True
			for jar in missing_jars:
				if jar in line:
					flag = True
			if not flag: 
				f1.write(line)
		f.close()
		f1.close()
		mvcmd = "mv " + input_file[:-9] + "newInput.txt " + input_file
		print mvcmd
		os.system(mvcmd)
		f = open(input_file[:-9] + "frameworkInput.txt", 'r')
		f1 = open(input_file[:-9] + "newInput.txt",'w')
		for line in f.readlines():
			flag = False
			for jar in missing_jars:
				if jar in line:
					flag = True
			if not flag: 
				f1.write(line)
		f.close()
		f1.close()
		mvcmd = "mv " + input_file[:-9] + "newInput.txt " + input_file[:-9] + "frameworkInput.txt"
		print mvcmd
		os.system(mvcmd)
			
			

# Where everything begins
def main(options, arguments) :
	# set the root folder
	root = options.directory
	list_paths(root)
	imageName = os.path.basename(root)
	for input_file in input_paths:
		run_input_analysis(input_file,imageName)
	merge_names = app_names + framework_names 
	missing_jars = [item for item in merge_names if item not in jar_names]
	# there are missing jars, we need to generate them using the processLollipop utility
	print imageName
	print missing_jars
	fixInput(missing_jars)

# parse the input and start the main() function
if __name__ == "__main__" :
    parser = OptionParser()
    for option in options :
        param = option['name']
        del option['name']
        parser.add_option(*param, **option)

    options, arguments = parser.parse_args()
    sys.argv[:] = arguments
    main(options, arguments)
