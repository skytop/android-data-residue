'''
Input argument: the path that containing a directory called "images" that have raw zip files in it

DEPENDENCIES:
	android-tools-fsutils :: sudo apt-get install android-tools-fsutils

Possible TODO:
	make both mac and linux one script, determine which the machine is, then use an
	if statement to chose between 2 different ways to execute

'''

import sys
import subprocess
import glob
import ntpath
import datetime
import os
import time
import zipfile


default_dir = os.getcwd()					# name dir that script is run from
img_dir = os.getcwd() + sys.argv[1] 		# name dir to store all image files
img_out_dir = os.getcwd() + "/image_out"	# name dir to store processed images
unzip_dir = img_out_dir + "/unzipped"	   	# name dir to store all unzipped files (will be listed as the image's name)


# # # # # # # # # # # # # # # # #
# #					          # #
# # Create needed directories # #
# #                           # #
# # # # # # # # # # # # # # # # #
subprocess.call(["mkdir", "image_out"])		# create dir to store all .jar files


# # # # # # # # # # # # # # #
# # #					# # #
# # #  Unzip zip files  # # #
# #	#				    # # #
# # # # # # # # # # # # # # #
for zip_file in os.listdir(img_dir):										# work on each .zip file in the directory of images
	os.chdir(img_dir)														# switch to dir with images
	if zip_file.endswith(".zip"):											# pick out zips in img_dir (rm .zip from filename)
		unzip_dir = img_out_dir + "/" + zip_file[:-4]						#
		print "** found .zip file: " + zip_file								#
		print "** unzipping:  " + zip_file + " -> " + unzip_dir + "...\n"	#
		subprocess.call(["mkdir", unzip_dir])								# create dir to store mounted images to walk through
		sys.stdout.flush()													# flush the buffer
		subprocess.call(["unzip", zip_file, "-d", unzip_dir])				# unzip the image file
		print "** finished unzipping " + zip_file + "...\n"					# unzip finished TODO: if (successful or not)
		subprocess.call(["mkdir", unzip_dir + "/APKs"])						# create dir to store this images APKs
		print "** creating diretory for " + zip_file[:-4] +"'s APKs"		#
		subprocess.call(["mkdir", unzip_dir + "/JARs"])						# create dir to store this images APKs
		print "** creating diretory for " + zip_file[:-4] +"'s JARs"		#


		# # # # # # # # # # # # # # # # # # # #
		# # #					          # # #
		# # # unpack and mount the images # # #
		# # #					          # # #
		# # # # # # # # # # # # # # # # # # # #
		os.chdir(unzip_dir)
		for mountable_file in os.listdir(unzip_dir):							# look for the file to mount
			print "examing: " + mountable_file			
			if "system" not in mountable_file:									# file will be system.img.ext4
				continue														# ignore all other files
			print "** simg2img: " + mountable_file + "...\n"					#
			sys.stdout.flush()							#				
			subprocess.call(["simg2img", mountable_file,  "system.ext4"])		# convert from sparse img to regular img
			print "** done extracting image...\n"								#
			print "** mounting image...\n"										#
			subprocess.call(["mkdir", "mounted"])								# create dir for mounted image
			subprocess.call(["mount", "-o", "loop", "system.ext4", "mounted"])	# mount the image in mounted dir
			sys.stdout.flush()													#
			print "** finished mounting " + mountable_file + "...\n"			#
			break																#


		# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
		# # #																				# # #
		# # #     Walk through directory and place apk,jar,dex,odex files appropriately     # # #
		# # #																			    # # #
		# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
		for directory in os.walk(unzip_dir + "/mounted").next()[1]:
			print directory 
			print os.path.join(unzip_dir + "/mounted", directory)+"/*"
			if "app" in directory:
				subprocess.call(["cp", "-a", os.path.join(unzip_dir + "/mounted", directory)+"/.", unzip_dir + "/APKs/"])
			if "framework" in directory:
				subprocess.call(["cp", "-a", os.path.join(unzip_dir + "/mounted", directory)+"/.", unzip_dir + "/JARs/"])
			else:
				continue

		# # # # # # # # # # # # # # # # # # # #
		# # #					          # # #
		# # #      unmount the images     # # #
		# # #					          # # #
		# # # # # # # # # # # # # # # # # # # #
		print "** unmounting system image: " + zip_file[:-4] + "..."	#
		print "** umounting: " + unzip_dir + "/mounted...\n"			# echo what's happening to the shell
		subprocess.call(["umount", "mounted"])							# unmount the image
		print "** finished unmounting...\n"


		# # # # # # # # # # # # # # # # # # # #
		# # #					          # # #
		# # #   Delete uneccessary files  # # #
		# # #					          # # #
		# # # # # # # # # # # # # # # # # # # #
		os.chdir(unzip_dir)															# switch to dir of where image is held
		print "** cleaning up directory...\n"										#
		for each_file in os.listdir(unzip_dir):										# must switch in order to rm unneccessary file
			if (os.path.isfile(each_file)):											# pick out unimportant files
				print "** removing file " + each_file + "..."						#
				subprocess.call(["rm", each_file])									# remove the file
				print "** finished removing " + each_file + "...\n"					#
			elif (os.path.isdir(each_file)):										# pick out unneccessary directories
				if (each_file == "APKs"):											# DO NOT remove APKs
					continue														#
				elif (each_file == "JARs"):											# DO NOT remove JARs
					continue														#
				else:																#
					print "** removing directory " + each_file + "..."				#
					subprocess.call(["rm", "-r", each_file])						# remove the directory
					print "** finished removing " + each_file + "...\n"				#
