import sys, os, cmd, threading, code, re
from xml.dom.minidom import parseString
from optparse import OptionParser

option_0 = { 'name' : ('-d', '--directory'),  'help' : 'directory : use this directory', 'nargs' : 1 }
options = [option_0]

# Parse the directory to get all the APKs
def list_paths(root):
	for path, dirs, files in os.walk(root, topdown=False):
		for apk in files:
			mvCmd = "mv " + os.path.join(path, apk) + " " + root
			os.system(mvCmd)
			#print mvCmd
	for directory in os.walk(root).next()[1]:
		rmCmd = "rm -rf " + os.path.join(root,directory)
		#print rmCmd
		os.system(rmCmd)       		

# Where everything begins
def main(options, arguments) :
	# set the root folder
	root = options.directory
	index = 1
	for img in os.walk(root).next()[1]:
		img_path = os.path.join(root,img)
		print index
		index = index + 1
		print img_path
		for folder in os.walk(img_path).next()[1]:
			apk_jar = os.path.join(img_path,folder)
			print apk_jar
			list_paths(apk_jar)

# parse the input and start the main() function
if __name__ == "__main__" :
    parser = OptionParser()
    for option in options :
        param = option['name']
        del option['name']
        parser.add_option(*param, **option)

    options, arguments = parser.parse_args()
    sys.argv[:] = arguments
    main(options, arguments)
