import sys, os, cmd, threading, code, re
from xml.dom.minidom import parseString
from optparse import OptionParser

option_0 = { 'name' : ('-d', '--directory'),  'help' : 'directory : use this directory', 'nargs' : 1 }
options = [option_0]

# Parse the directory to get all the APKs
def list_paths(root):
	for path, dirs, files in os.walk(root, topdown=False):
		for tgz in files:
			if tgz.endswith(".tgz"):
				tarCmd = "tar zxvf " + os.path.join(path, tgz) + " -C " + os.path.join(root, "out")
				os.system(tarCmd)
	output = os.path.join(root, "out")
	for path, dirs, files in os.walk(output, topdown=False):
		for zipfile in files:
			if zipfile.endswith(".zip"):
				mvCmd = "mv " + os.path.join(path, zipfile) + " " + os.path.join(root, "out")
				os.system(mvCmd)
	for directory in os.walk(output).next()[1]:
		rmCmd = "rm -rf " + os.path.join(output,directory)
		#print rmCmd
		os.system(rmCmd)       		

# Where everything begins
def main(options, arguments) :
	# set the root folder
	root = options.directory
	mkdircmd = "mkdir " + os.path.join(root, "out")
	os.system(mkdircmd)
	list_paths(root)

	

# parse the input and start the main() function
if __name__ == "__main__" :
    parser = OptionParser()
    for option in options :
        param = option['name']
        del option['name']
        parser.add_option(*param, **option)

    options, arguments = parser.parse_args()
    sys.argv[:] = arguments
    main(options, arguments)
