import sys, os, cmd, threading, code, re
from xml.dom.minidom import parseString
from optparse import OptionParser

option_0 = { 'name' : ('-d', '--directory'),  'help' : 'directory : use this directory', 'nargs' : 1 }
options = [option_0]

# Parse the directory to get all the APKs
def list_paths(root):
	for directory in os.walk(root).next()[1]:
		mvCmd = "mv ../nexus_images/" + directory + " ../badImages/"
		os.system(mvCmd)

# Where everything begins
def main(options, arguments) :
	# set the root folder
	root = options.directory
	list_paths(root)

	

# parse the input and start the main() function
if __name__ == "__main__" :
    parser = OptionParser()
    for option in options :
        param = option['name']
        del option['name']
        parser.add_option(*param, **option)

    options, arguments = parser.parse_args()
    sys.argv[:] = arguments
    main(options, arguments)
