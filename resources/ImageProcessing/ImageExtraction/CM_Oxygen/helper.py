import sys, os, cmd, threading, code, re
from xml.dom.minidom import parseString
from optparse import OptionParser

option_0 = { 'name' : ('-d', '--directory'),  'help' : 'directory : use this directory', 'nargs' : 1 }
options = [option_0]

# Parse the directory to get all the APKs
def list_paths(root):
	for directory in os.walk(root).next()[1]:
		image_folder = os.path.join(root, directory)
		moveImage(directory, image_folder)	

def moveImage(name, folder):
	prop_folder = "./buildprops/" + name
	manifest_folder = "./maniefst_file/" + name
	platform_folder = "./platforms/" + name
	mkCMD = "mkdir " + prop_folder
	os.system(mkCMD)
	mkCMD = "mkdir " + manifest_folder
	os.system(mkCMD)
	mkCMD = "mkdir " + platform_folder
	os.system(mkCMD)
	cp_prop = "cp -a " + folder + "/buildprops/. " + prop_folder
	os.system(cp_prop)
	cp_manifest = "cp -a " + folder + "/maniefst_file/. " + manifest_folder
	os.system(cp_manifest)
	cp_platform = "cp -a " + folder + "/platforms/. " + platform_folder
	os.system(cp_platform)

# Where everything begins
def main(options, arguments) :
	# set the root folder
	root = options.directory
	mkdir = "mkdir ./buildprops"
	os.system(mkdir)
	mkdir = "mkdir ./maniefst_file"
	os.system(mkdir)
	mkdir = "mkdir ./platforms"
	os.system(mkdir)
	list_paths(root)

# parse the input and start the main() function
if __name__ == "__main__" :
    parser = OptionParser()
    for option in options :
        param = option['name']
        del option['name']
        parser.add_option(*param, **option)

    options, arguments = parser.parse_args()
    sys.argv[:] = arguments
    main(options, arguments)
