import sys, os, cmd, threading, code, re
from xml.dom.minidom import parseString
from optparse import OptionParser

option_0 = { 'name' : ('-d', '--directory'),  'help' : 'directory : use this directory', 'nargs' : 1 }
options = [option_0]

# Parse the directory to get all the APKs
def list_paths(root):
	for path, dirs, files in os.walk(root, topdown=False):
		for img in files:
			processImage(img, path)

def processImage(img, path):
	imgFile = os.path.join(path, img)
	unzipFolder = "./unzip/" + img
	unzipCMD = "unzip " + imgFile + " -d " + unzipFolder
	print unzipCMD
	os.system(unzipCMD)
	allFiles = []
	for path, dirs, files in os.walk(unzipFolder, topdown=False):
		for f in files:
			allFiles.append(f)
	if "system.img" in allFiles:
		mount(unzipFolder, img)
	else:
		extract(unzipFolder+"/system",img)
	rmCMD = "rm -rf " + unzipFolder
	print rmCMD
	os.system(rmCMD)
	
def mount(folder,img):
	systemimg = folder + "/system.img"
	simg2img = "simg2img " + systemimg + " " + folder + "/system.ext4"
	os.system(simg2img)
	raw_image = folder + "/system.ext4"
	mount = folder + "/mount"
	mkdir = "mkdir " + mount
	print mkdir
	os.system(mkdir)
	mountCMD = "mount -t ext4 -o loop " + raw_image + " " + mount
	print mountCMD
	os.system(mountCMD)
	extract(mount, img)
	umountCMD = "umount " + mount
	print umountCMD
	os.system(umountCMD)

def extract(folder,img):
	imgFolder2 = "./YOUSRA/" + img
	mkdir = "mkdir " + imgFolder2
	print mkdir
	os.system(mkdir)
	mkdir = "mkdir " + imgFolder2 + "/buildprops"
	print mkdir
	os.system(mkdir)
	mkdir = "mkdir " + imgFolder2 + "/maniefst_file"
	print mkdir	
	os.system(mkdir)
	mkdir = "mkdir " + imgFolder2 + "/platforms"
	print mkdir
	os.system(mkdir)
	for directory in os.walk(folder).next()[1]:
		if "etc" in directory:
			cmd = "cp -R " + os.path.join(folder, directory)+ " " + imgFolder2 + "/platforms/"
			print cmd
			os.system(cmd)
	apkFiles = []
	for path, dirs, files in os.walk(folder, topdown=False):
		for f in files:
			if ".apk" in f:
				apkFiles.append(os.path.join(path, f))
			elif "build.prop" in f:
				cmd = "cp " + os.path.join(path, f) + " " + imgFolder2 + "/buildprops/"
				print cmd
				os.system(cmd)
	for apk in apkFiles:
		apktool = "java -jar tools/apktool_2.0.3.jar d " + apk + " -o output"
		os.system(apktool)
		mvCMD = "mv output/AndroidManifest.xml " +  imgFolder2 + "/maniefst_file/" + os.path.splitext(os.path.basename(apk))[0] + "_AndroidManifest.xml"
		os.system(mvCMD)
		rmCMD = "rm -rf output"
		os.system(rmCMD)

# Where everything begins
def main(options, arguments) :
	# set the root folder
	root = options.directory
	mkdir = "mkdir ./unzip"
	os.system(mkdir)
	mkdir = "mkdir ./YOUSRA"
	os.system(mkdir)
	list_paths(root)

# parse the input and start the main() function
if __name__ == "__main__" :
    parser = OptionParser()
    for option in options :
        param = option['name']
        del option['name']
        parser.add_option(*param, **option)

    options, arguments = parser.parse_args()
    sys.argv[:] = arguments
    main(options, arguments)
