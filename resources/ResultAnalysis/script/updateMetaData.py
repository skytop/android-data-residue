import os
import csv
import xml.etree.ElementTree as ET

#read xml file and change attributes value for image tag 
def rewriteXML(filename,name,vendor,real_vendor,version,model,manufacturer,region,carrier):
	directory = "./sample/"+filename
	tree = ET.parse(directory)
	root = tree.getroot()
	for item in root.iter('image'): 	
		item.set('name',name[0:len(name)-4])
		item.set('vendor',vendor)
		item.set('real_vendor',real_vendor)
		item.set('version',version)
		item.set('model',model)
		item.set('manufacturer',manufacturer)
		item.set('region',region)
		item.set('carrier',carrier)
	directory = './output/'+filename
	tree.write(directory)

#read row from attributes 
def readCSV(filename): 
	rownum = 0
	score = 0
	result = ""
	with open('all_images.csv', 'rb') as f: 
		reader = csv.reader(f)
		for row in reader: 	
			if rownum == 0:
				header = row
			else: 
				colnum = 0
				for col in row: 
					if(colnum==0):
						tmp =[col,filename]
						tmpscore = long_substr(tmp)
						if(score < tmpscore): 
							score = tmpscore
							result = row
					colnum += 1
			rownum += 1
 	name = result[0]
	vendor = result[1]
	real_vendor = result[2]	
	version = result[3]
	model = result[4]
	manufacturer = result[5]	
	region = result[6]
	carrier = result[7]
 	rewriteXML(filename,name,vendor,real_vendor,version,model,manufacturer,region,carrier)		
	return result 

# read file name from certain directory
def readFilename():
	filenamelist=[]
	for filename in os.listdir('./sample'):	
		filename = filename
		readCSV(filename)
		print filename
	return filenamelist

# find the longest match between two strings
def long_substr(data):
    substr = ''
    if len(data) > 1 and len(data[0]) > 0:
        for i in range(len(data[0])):
            for j in range(len(data[0])-i+1):
                if j > len(substr) and is_substr(data[0][i:i+j], data):
                    substr = data[0][i:i+j]
    return len(substr)

def is_substr(find, data):
    if len(data) < 1 and len(find) < 1:
        return False
    for i in range(len(data)):
        if find not in data[i]:
            return False
    return True



####################### main entry ###########################
readFilename()
