#######################################
# Dump data residue detail information 
# Put xml files in folder called 'sample'
# Author: Kailiang
######################################
import os
import csv
import xml.etree.ElementTree as ET

header =[]
aosp_2_3_x = set()
aosp_4_0_x = set()
aosp_4_1_x = set()
aosp_4_2_x = set()
aosp_4_3_x = set()
aosp_4_4_x = set()
aosp_5_0_x = set()
aosp_5_1_x = set()
aosp_6_0_x = set()

result = {'2.3_aosp':0, '2.3_other':0, '2.3_image':0,
	'4.0_aosp':0, '4.0_other':0, '4.0_image':0,
	'4.1_aosp':0, '4.1_other':0, '4.1_image':0,
	'4.2_aosp':0, '4.2_other':0, '4.2_image':0,
	'4.3_aosp':0, '4.3_other':0, '4.3_image':0,
	'4.4_aosp':0, '4.4_other':0, '4.4_image':0,
	'5.0_aosp':0, '5.0_other':0, '5.0_image':0,
	'5.1_aosp':0, '5.1_other':0, '5.1_image':0,
	'6.0_aosp':0, '6.0_other':0, '6.0_image':0}

def harvestAOSP(filename):
	image_name = ''
	directory = "./sample/"+filename
	tree = ET.parse(directory)
        root = tree.getroot()
	for image in root.iter('image'): 	
		image_name = image.get('name')
		version = image.get('version')
		vendor = image.get('real_vendor')
		if 'google' not in vendor:
			break;
		for service in image.iter('service'):
			service_name = str(service.get('name'))
			service_category = str(service.get('category'))
			for residue in service.iter('residue'):
				residue_name = str(residue.get('name'))	
				complexity = str(residue.get('complexity'))
				if '1000' not in complexity:
					continue
				#create record for csv 
				key = service_name+'_'+residue_name+'_'+service_category	
				if '2.3.' in version:
					aosp_2_3_x.add(key)
				if '4.0.' in version:
					aosp_4_0_x.add(key)
				if '4.1.' in version:
					aosp_4_1_x.add(key)
				if '4.2.' in version:
					aosp_4_2_x.add(key)
				if '4.3.' in version or version == '4.3':
					aosp_4_3_x.add(key)
				if '4.4.' in version or version == '4.4':
					aosp_4_4_x.add(key)
				if '5.0.' in version or version == '5.0':
					aosp_5_0_x.add(key)
				if '5.1.' in version or version == '5.1':
					aosp_5_1_x.add(key)
				if '6.0.' in version:
					aosp_6_0_x.add(key)

				


def collect(filename):
	image_name = ''
	directory = "./sample/"+filename
	tree = ET.parse(directory)
        root = tree.getroot()
	for image in root.iter('image'): 	
		image_name = image.get('name')
		version = image.get('version')
		vendor = image.get('real_vendor')
		if 'Cynogen' not in vendor:
			break;
		if '2.3.' in version:
			result['2.3_image'] = result['2.3_image'] + 1
		elif '4.0.' in version:
			result['4.0_image'] = result['4.0_image'] + 1		
		elif '4.1.' in version:
			result['4.1_image'] = result['4.1_image'] + 1
		elif '4.2.' in version:
			result['4.2_image'] = result['4.2_image'] + 1
		elif '4.3.' in version or version == '4.3':
			result['4.3_image'] = result['4.3_image'] + 1
		elif '4.4.' in version or version == '4.4':
			result['4.4_image'] = result['4.4_image'] + 1
		elif '5.0.' in version or version == '5.0':
			result['5.0_image'] = result['5.0_image'] + 1
		elif '5.1.' in version or version == '5.1':
			result['5.1_image'] = result['5.1_image'] + 1		
		elif '6.0.' in version:
			result['6.0_image'] = result['6.0_image'] + 1
		else:
			print version
		for service in image.iter('service'):
			service_name = str(service.get('name'))
			service_category = str(service.get('category'))
			for residue in service.iter('residue'):
				residue_name = str(residue.get('name'))	
				complexity = str(residue.get('complexity'))
				if '1000' not in complexity:
					continue				
				#create record for csv 
				key = service_name+'_'+residue_name+'_'+service_category	
				if '2.3.' in version:
					if key in aosp_2_3_x:
						result['2.3_aosp'] = result['2.3_aosp'] + 1
					else:
						result['2.3_other'] = result['2.3_other'] + 1
				if '4.0.' in version:
					if key in aosp_4_0_x:
						result['4.0_aosp'] = result['4.0_aosp'] + 1
					else:
						result['4.0_other'] = result['4.0_other'] + 1
				if '4.1.' in version:
					if key in aosp_4_1_x:
						result['4.1_aosp'] = result['4.1_aosp'] + 1
					else:
						result['4.1_other'] = result['4.1_other'] + 1
				if '4.2.' in version:
					if key in aosp_4_2_x:
						result['4.2_aosp'] = result['4.2_aosp'] + 1
					else:
						result['4.2_other'] = result['4.2_other'] + 1
				if '4.3.' in version or version == '4.3':
					if key in aosp_4_3_x:
						result['4.3_aosp'] = result['4.3_aosp'] + 1
					else:
						result['4.3_other'] = result['4.3_other'] + 1
				if '4.4.' in version or version == '4.4':
					if key in aosp_4_4_x:
						result['4.4_aosp'] = result['4.4_aosp'] + 1
					else:
						result['4.4_other'] = result['4.4_other'] + 1
				if '5.0.' in version or version == '5.0':
					if key in aosp_5_0_x:
						result['5.0_aosp'] = result['5.0_aosp'] + 1
					else:
						result['5.0_other'] = result['5.0_other'] + 1
				if '5.1.' in version or version == '5.1':
					if key in aosp_5_1_x:
						result['5.1_aosp'] = result['5.1_aosp'] + 1
					else:
						result['5.1_other'] = result['5.1_other'] + 1
				if '6.0.' in version:
					if key in aosp_6_0_x:
						result['6.0_aosp'] = result['6.0_aosp'] + 1
					else:
						result['6.0_other'] = result['6.0_other'] + 1
def initCVSHeader():
	global header
	header = [
	'Image_Service_DR_N',
	'Service_DR_N_Category',
	'Service_Type',
	'Service Code Size',
 	'Category',
	'Complexity'
	] 

def readFilename():
	for filename in os.listdir('./sample'):	
		filename = filename
		harvestAOSP(filename)
	#for filename in os.listdir('./sample'):	
	#	filename = filename
	#	collect(filename)
	#	print filename
	allset = aosp_2_3_x | aosp_4_0_x | aosp_4_1_x | aosp_4_2_x | aosp_4_3_x | aosp_4_4_x | aosp_5_0_x | aosp_5_1_x | aosp_6_0_x
	unique_ones = open('unique_ones.csv')
	results = csv.reader(unique_ones)
	i=0
	j=0
	for row in results:
		i = i+1
		if row[0] in allset:
			j = j+1

	print i
	print j

####################### main entry ###########################
#initCVSHeader()
#with open('detail.csv','a') as csvfile:
#	file_writer = csv.writer(csvfile)	
#	file_writer.writerow(header)

readFilename()


print sorted(result.iteritems())
