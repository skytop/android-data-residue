#######################################
# Dump data residue detail information 
# Put xml files in folder called 'sample'
# Author: Kailiang
######################################
import os
import csv
import xml.etree.ElementTree as ET

header =[]

#extract residue instance
def countResidue(filename):
	image_name = ''
	directory = "./sample/"+filename
	tree = ET.parse(directory)
        root = tree.getroot()
	for image in root.iter('image'): 	
		image_name = image.get('name')
		for service in image.iter('service'):
			service_name = str(service.get('name'));
			service_category = str(service.get('category'));
			service_type = str(service.get('type'));
			service_size = str(service.get('size'));
			for residue in service.iter('residue'):
				residue_name = str(residue.get('name'));	
				complexity = str(residue.get('complexity'));
				#create record for csv 
				col1= image_name+'_'+service_name+'_'+residue_name;
				col2= service_name+'_'+residue_name+'_'+service_category;	
				col3= service_type; 
				col4= service_size;
				col5= service_category;
				col6= complexity;
				record = [col1,col2,col3,col4,col5,col6];
				with open('detail.csv','a') as csvfile:
					file_writer = csv.writer(csvfile)	
					file_writer.writerow(record)

def initCVSHeader():
	global header
	header = [
	'Image_Service_DR_N',
	'Service_DR_N_Category',
	'Service_Type',
	'Service Code Size',
 	'Category',
	'Complexity'
	] 

def readFilename():
	for filename in os.listdir('./sample'):	
		filename = filename
		countResidue(filename)
		print filename


####################### main entry ###########################
initCVSHeader()
with open('detail.csv','a') as csvfile:
	file_writer = csv.writer(csvfile)	
	file_writer.writerow(header)

readFilename()
