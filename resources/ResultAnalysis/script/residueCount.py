#######################################
# Count residue instance in xml file. 
# Put xml files in folder called 'sample'
# Author: Kailiang
######################################
import os
import csv
import xml.etree.ElementTree as ET

total = 0
header =[]

#extract residue instance
def countResidue(filename):
	residueNum = 0
	name = ''
	version = ''
	vendor= ''
	real_vendor= ''
	record =[]
	directory = "./sample/"+filename
	#init record	
	for i in range(0,4):
		record.append('')
	for i in range(0,1002):
		record.append('0')	
	tree = ET.parse(directory)
        root = tree.getroot()
	for image in root.iter('image'): 	
		name = image.get('name')
		version = image.get('version')
		vendor = image.get('vendor')
		real_vendor = image.get('real_vendor')
		for service in image.iter('service'):
			for residue in service.iter('residue'):
				residueNum = residueNum + 1 
				tmp = record[int(residue.get('complexity'))+4]
				record[int(residue.get('complexity'))+4] = str(int(tmp)+1)
	record[0]=name
	record[1]=version
	record[2]=vendor
	record[3]=real_vendor
	record[1005]=str(residueNum)
	with open('summary.csv','a') as csvfile:
		file_writer = csv.writer(csvfile)	
		file_writer.writerow(record)
	global total
	total = total + residueNum

def initCVSHeader():
	global header
	header = [
	'Name',
	'Version',
	'Vendor',
	'Real Vendor'	
	] 
	for i in range(0,1001):		
		header.append('C'+str(i))
	header.append('Total Residue')

def readFilename():
	for filename in os.listdir('./sample'):	
		filename = filename
		countResidue(filename)
		print filename


####################### main entry ###########################
initCVSHeader()
with open('summary.csv','a') as csvfile:
	file_writer = csv.writer(csvfile)	
	file_writer.writerow(header)

readFilename()
