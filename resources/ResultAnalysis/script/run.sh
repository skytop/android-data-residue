#!/bin/bash
# My first script

echo '\n--------> update meta\n'
python updateMetaData.py
echo '\n--------> remove sample\n'
rm -rf sample/
echo '\n--------> rename output\n'
mv output/ sample
echo '\n--------> new residue\n'
python newResidue.py
echo '\n--------> evaluation google\n'
python performEvaluation_google.py
echo '\n--------> evaluation\n'
python performEvaluation.py
echo '\n--------> residue count\n'
python residueCount.py
echo '\n--------> residue detail\n'
python residueDetail.py
echo '\n--------> clean up\n'
rm sample/*
rm patching/*
echo '\n--------> output\n'
mkdir result
mv *.csv result


