#######################################
# Detection Performance Evaluation
# Put xml and txt files in 'sample' and 'patching'
# Author: Kailiang
######################################
import os
import csv
import xml.etree.ElementTree as ET

header =[]

#extract residue instance
def evaluateImage(xmlfilename,txtfilename):
	KEYWORD_THREAD ='Thread rewriting stats: ';
	len_thread = len(KEYWORD_THREAD);
	thread_finish = 0;
	thread_total= 0;
	KEYWORD_SERVICE ='Service rewriting stats: ';
	len_service = len(KEYWORD_SERVICE);
	service_finish = 0;
	service_total= 0;
	KEYWORD_HANDLER ='Handler rewriting stats: ';
	len_handler = len(KEYWORD_HANDLER);
	handler_finish = 0;
	handler_total= 0;
	KEYWORD_ASYNTASK ='AsyncTask rewriting stats: ';
	len_asyntask = len(KEYWORD_ASYNTASK);
	asyntask_finish = 0;
	asyntask_total= 0;
	txtdirectory = './patching/'+ txtfilename;
	with open(txtdirectory) as f:
	    content = f.readlines()
	    for line in content: 
		if KEYWORD_HANDLER in line:
			tmp = line[len_handler-1:].split('/');
			handler_finish = handler_finish + int(tmp[0]);
			handler_total = handler_total + int(tmp[1]);
		if KEYWORD_ASYNTASK in line:
			tmp = line[len_asyntask-1:].split('/');
			asyntask_finish = asyntask_finish + int(tmp[0]);
			asyntask_total = asyntask_total + int(tmp[1]);
		if KEYWORD_SERVICE in line:
			tmp = line[len_service-1:].split('/');
			service_finish = service_finish + int(tmp[0]);
			service_total = service_total + int(tmp[1]);
		if KEYWORD_THREAD in line:
			tmp = line[len_thread-1:].split('/');
			thread_finish = thread_finish + int(tmp[0]);
			thread_total = thread_total + int(tmp[1]);
	col20=str(thread_finish);	
	col21=str(thread_total);	
	col22=str(service_finish);	
	col23=str(service_total);	
	col24=str(handler_finish);	
	col25=str(handler_total);	
	col26=str(asyntask_finish);	
	col27=str(asyntask_total);	

	image_name = '';
	image_vendor = '';
	image_real_vendor = ''; 
	image_model = '';
	image_version = '';
	image_manufacturer = '';
	image_region= '';
	image_carrier= '';	
	serviceDetectionCost = '0';
	rewritingCost = '0';
	residueDetectionCost = '0';	
	finish_service_number =0;
	service_number = 0;
	finish_framework_service_number = 0;
	framework_service_number = 0;
	application_service_number = 0;
	finish_application_service_number = 0;
	directory = "./sample/"+xmlfilename;
	tree = ET.parse(directory);
        root = tree.getroot();
	for image in root.iter('image'): 	
		image_name = image.get('name');
		image_vendor = image.get('vendor');
		image_real_vendor = image.get('real_vendor');	
		image_model = image.get('model');
		image_version = image.get('version');
		image_manufacturer = image.get('manufacturer');
		image_region = image.get('region');
		image_carrier = image.get('carrier');
		for serviceDetectionCost in image.iter('serviceDetectionCost'):
			serviceDetectionCost = serviceDetectionCost.text; 
		for rewritingCost in image.iter('rewritingCost'):
			rewritingCost = rewritingCost.text; 
		for residueDetectionCost in image.iter('residueDetectionCost'):
			residueDetectionCost = residueDetectionCost.text;
		for service in image.iter('service'):
			service_number = service_number + 1;
			finish_flag = service.get('finished');
			service_type = service.get('type');
			if(service_type=='appservice'):
				application_service_number = application_service_number +1;
				if(finish_flag == 'true'):
					finish_application_service_number = finish_application_service_number +1;
			if(service_type=='frameworkservice'):
				framework_service_number = framework_service_number +1;	
				if(finish_flag =='true'):
					finish_framework_service_number = finish_framework_service_number +1;
			if(finish_flag == 'true'):
				finish_service_number =finish_service_number +1;
		#create record for csv 
		col1= image_name;
		col2= image_vendor;	
		col3= image_real_vendor;	
		col4= image_version;	
		col5= image_model;	
		col6= image_manufacturer;	
		col7= image_region;	
		col8= image_carrier;	
		col9= serviceDetectionCost;
		col10= rewritingCost;
		col11= residueDetectionCost;
		tmp_total = int(serviceDetectionCost)+int(rewritingCost)+int(residueDetectionCost); #total cost 	
		col12= str(tmp_total); #total cost 	
		col13= str(int(service_number)/7);
		col14= str(int(finish_service_number)/7);
		col15= str((int(service_number)-int(finish_service_number))/7);  #unfinished service number 
		col16= str(int(finish_framework_service_number)/7);
		col17= str((int(framework_service_number)-int(finish_framework_service_number))/7);
		col18= str(int(finish_application_service_number)/7);
		col19= str((int(application_service_number)-int(finish_application_service_number))/7);
		record = [col1,col2,col3,col4,col5,col6,col7,col8,col9,col10,col11,col12,col13,col14,col15,col16,col17,col18,col19,col20,col21,col22,col23,col24,col25,col26,col27];
		with open('performance.csv','a') as csvfile:
			file_writer = csv.writer(csvfile);	
			file_writer.writerow(record);

def initCVSHeader():
	global header
	header = [
	'Name',
	'Vendor',
	'Real_vendor',
	'Image Version',
 	'Model',
	'Manufacturer',
	'Region',
	'Carrier',
	'ServiceDetectionCost',
	'RewritingCost',
	'Refection Cost',
	'ToTal Cost',
	'Number of Service',
	'Finished Service',
	'Unfinished Services',
	'Finished Framework Services',
	'Unfinished Framework Services',
	'Finished App Services',
	'Unfinished App Services',
	'Thread Patching Status Finish',
	'Thread Patching Status Total',
	'Service Patching Status Finish',
	'Service Patching Status Total',
	'Handler Patching Status Finish',
	'Handler Patching Status Total',
	'AsynTask Patching Status Finish',
	'AsynTask Patching Status Total'
	] 

def readFilename():
	xmlfilename='';
	txtfilename='';
	for xfilename in os.listdir('./sample'):	
		xmlfilename = xfilename;
		for tfilename in os.listdir('./patching'):	
			txtfilename = tfilename;
			if(xmlfilename[:-4] == txtfilename[:-4]):
				print xmlfilename[:-4];
				evaluateImage(xmlfilename,txtfilename);
	


####################### main entry ###########################
initCVSHeader();
with open('performance.csv','a') as csvfile:
	file_writer = csv.writer(csvfile);	
	file_writer.writerow(header);

readFilename();
