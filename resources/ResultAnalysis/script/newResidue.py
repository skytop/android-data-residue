#######################################
# Dump new data residue detail information 
# Put xml files in folder called 'sample'
# Author: Kailiang
######################################
import os
import csv
import xml.etree.ElementTree as ET

header =[];
transport_vulnerability=[];
restrictions_vulnerability=[];
notification_policy_vulnerability=[];
#extract residue instance
def countResidue(filename):
	global transport_vulnerability;
	global restrictions_vulnerability;
	global notification_policy_vulnerability;
	image_name = ''
	directory = "./sample/"+filename
	tree = ET.parse(directory)
        root = tree.getroot()
	for image in root.iter('image'): 	
		image_name = image.get('name')
		for service in image.iter('service'):
			service_category = str(service.get('category'));
			for residue in service.iter('residue'):
				residue_name = str(residue.get('name'));	
				if(service_category == 'settings' and residue_name=='backup_transport'):
			 		transport_vulnerability.append(image_name);
				if(service_category == 'xml' and residue_name=='restrictions'):
			 		restrictions_vulnerability.append(image_name);
				if(service_category == 'xml' and residue_name=='notification-policy'):
			 		notification_policy_vulnerability.append(image_name);
def writeCSV():
	vulnerable_images=[];
	for instance in set(transport_vulnerability):	
		vulnerable_images.append(instance);
		record=[instance];
		with open('transport_vulnerability.csv','a') as csvfile:
			file_writer = csv.writer(csvfile)	
			file_writer.writerow(record)
	for instance in set(restrictions_vulnerability):	
		vulnerable_images.append(instance);
		record=[instance];
		with open('restrictions_vulnerability.csv','a') as csvfile:
			file_writer = csv.writer(csvfile)	
			file_writer.writerow(record)
	for instance in set(notification_policy_vulnerability):	
		vulnerable_images.append(instance);
		record=[instance];
		with open('notification_policy_vulnerability.csv','a') as csvfile:
			file_writer = csv.writer(csvfile)	
			file_writer.writerow(record)
	for instance in set(vulnerable_images):
		record=[instance];
		with open('vulnerable_images.csv','a') as csvfile:
			file_writer = csv.writer(csvfile)	
			file_writer.writerow(record)
	

def initCVSHeader():
	global header;
	header = [
	'Image_Name'
	] 

def readFilename():
	for filename in os.listdir('./sample'):	
		filename = filename
		countResidue(filename)
		print filename
	writeCSV();


####################### main entry ###########################
initCVSHeader()
with open('transport_vulnerability.csv','a') as csvfile:
	file_writer = csv.writer(csvfile)	
	file_writer.writerow(header)

with open('restrictions_vulnerability.csv','a') as csvfile:
	file_writer = csv.writer(csvfile)	
	file_writer.writerow(header)

with open('notification_policy_vulnerability.csv','a') as csvfile:
	file_writer = csv.writer(csvfile)	
	file_writer.writerow(header)

with open('vulnerable_images.csv','a') as csvfile:
	file_writer = csv.writer(csvfile)	
	file_writer.writerow(header)

readFilename()

