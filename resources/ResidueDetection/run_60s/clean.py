import sys, os, cmd, threading, code, re
from xml.dom.minidom import parseString
from optparse import OptionParser

option_0 = { 'name' : ('-d', '--directory'),  'help' : 'directory : use this directory', 'nargs' : 1 }
options = [option_0]
done_xml = []
done_txt = []

# Parse the directory to get all the APKs
def list_paths():
	for path, dirs, files in os.walk("./", topdown=False):
                  for name in files:
                          if name.endswith(".xml"):
                                  done_xml.append(name)
                          if name.endswith(".txt"):
                                  done_txt.append(name)


# Where everything begins
def main(options, arguments) :
	# set the root folder
	list_paths()
	for txt in done_txt:
		if (txt[:-4] + ".xml") not in done_xml:
			rmCMD = "rm " + txt
			os.system(rmCMD)


# parse the input and start the main() function
if __name__ == "__main__" :
    parser = OptionParser()
    for option in options :
        param = option['name']
        del option['name']
        parser.add_option(*param, **option)

    options, arguments = parser.parse_args()
    sys.argv[:] = arguments
    main(options, arguments)
