import sys, os, cmd, threading, code, re
from xml.dom.minidom import parseString
from optparse import OptionParser

option_0 = { 'name' : ('-d', '--directory'),  'help' : 'directory : use this directory', 'nargs' : 1 }
options = [option_0]
images = {}
done = []

# Parse the directory to get all the APKs
def list_paths(root):
	for directory in os.walk(root).next()[1]:
		image_folder = os.path.join(root,directory)
		for image in os.walk(image_folder).next()[1]:
			images[os.path.join(image_folder, image)] = image
	for path, dirs, files in os.walk("./", topdown=False):
                  for name in files:
                          if name.endswith(".xml"):
                                  done.append(name)


# Where everything begins
def main(options, arguments) :
	# set the root folder
	root = options.directory
	list_paths(root)
	print "Totally " + str(len(images)) + " images to process ..."
	index = 1
	for image, imageName in images.iteritems():
		if (imageName + ".xml") in done:
			continue
		print "--- processing " + str(index) + "th image ..."
		cpCMD = "cp -a " + image + "/. ./"
		runCMD = "python runOnce.py -n " + imageName
		print cpCMD
		os.system(cpCMD)
		print runCMD	
		os.system(runCMD)
		index = index + 1	


# parse the input and start the main() function
if __name__ == "__main__" :
    parser = OptionParser()
    for option in options :
        param = option['name']
        del option['name']
        parser.add_option(*param, **option)

    options, arguments = parser.parse_args()
    sys.argv[:] = arguments
    main(options, arguments)
