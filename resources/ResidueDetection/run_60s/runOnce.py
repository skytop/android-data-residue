import sys, os, cmd, threading, code, re
from xml.dom.minidom import parseString
from optparse import OptionParser

option_0 = { 'name' : ('-n', '--name'),  'help' : 'name : output xml name', 'nargs' : 1 }
options = [option_0]

# Where everything begins
def main(options, arguments) :
	# set the root folder
	image = options.name
	runCMD = "java -Xmx16g -jar residueDetection.jar > " + image + ".txt" 
	mvCMD = "mv result.xml " + image + ".xml"
	cleanCMD = "rm -rf apps/ appservices.txt framework/ frameworkInput.txt input.txt"
	print runCMD
	os.system(runCMD)
	print mvCMD
	os.system(mvCMD)
	print cleanCMD
	os.system(cleanCMD)


# parse the input and start the main() function
if __name__ == "__main__" :
    parser = OptionParser()
    for option in options :
        param = option['name']
        del option['name']
        parser.add_option(*param, **option)

    options, arguments = parser.parse_args()
    sys.argv[:] = arguments
    main(options, arguments)
