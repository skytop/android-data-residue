# README #

This repository contains the source code for **ANRED**, which is an **AN**droid **RE**sidue Detector that takes an Android device image as input and quantifies the risk for each identified data residue instance within collected system services. 


### Major Code Components ###

* **/src/com/residue/driver/**: launch and guide the analysis;
* **/src/com/residue/analysis/**: analysis logic based on different residue types;
* **/src/com/residue/api/**: generate saving API and deleting API paris;
* **/src/com/residue/rewriting/**: rewrite application jars to reconnect broken links;
* **/src/com/residue/apk/**: process application apks;
* **/src/com/residue/slice/**: data flow analysis APIs;
* **/src/com/residue/utility/**: commonly used APIs;

### Resources ###

* **/ImageCollections/**: provide a few processed images from different vendors to try our ANRED (the entire image set is available upon request);
* **/ImageProcessing/**: python scripts to process each given device image;
* **/ResidueDetection/run_60s/**: python scripts to start the ANRED analysis on all images within the given timeout (60s);
* **/ResidueResult.zip**: contains all results after applying ANRED on 606 images;
* **/ResultAnalysis/**: python scripts to analyze the generated results and present them in a meaningful manner (refer to our ESORICS 16' paper for more details); 

### Run from nightly build ###

* Download residueDetection.jar from the /ResidueDetection/run_60s/ folder;
* Download and copy one processed image to the same folder:
```
#!command

    cp /resources/ImageCollections/HTC/HTC_M8_M/* ./
```
* Run as java application (adjust parameters as needed):
```
#!command

    java -Xmx16g -jar residueDetection.jar > log.txt
```
* The residue report will be saved in result.xml;

### Run from source code ###

* Check out the repository to your computer;
* Import the repository into Eclipse;
* Copy one processed image to the root folder:
```
#!command

    cp /resources/ImageCollections/HTC/HTC_M8_M/* ./
```
* Run the code as a Java application. The entry point is: /src/com/residue/driver/Test.java/main()

### Publications ###
* [Xiao Zhang](https://xzhang35.expressions.syr.edu/), Yousra Aafer, Kailiang Ying and [Wenliang Du](http://www.cis.syr.edu/~wedu/). Hey, You, Get Off of My Image: Detecting Data Residue in Android Images. To appear in Proceedings of the 21st European Symposium on Research in Computer Security (ESORICS’16). Heraklion, Crete, Greece. September 26-30, 2016. [acceptance ratio: 60/285 ≈ 21%]
* [Xiao Zhang](https://xzhang35.expressions.syr.edu/), Kailiang Ying, Yousra Aafer, Zhenshen Qiu, and [Wenliang Du](http://www.cis.syr.edu/~wedu/). [Life after App Uninstallation: Are the Data Still Alive? Data Residue Attacks on Android](https://xzhang35.expressions.syr.edu/wp-content/uploads/2015/10/android_data_residue.pdf). In Proceedings of the Network and Distributed System Security Symposium (NDSS), San Diego, California, USA. February 21-24, 2016. ([Bib](https://xzhang35.expressions.syr.edu/wp-content/uploads/2015/10/dataresidue.txt)) ([Data Residue Vulnerability Website](https://sites.google.com/site/droidnotsecure/)) [acceptance ratio: 60/389 ≈ 15.4%]

### Contact ###
Please send your questions, suggestions and requests to [zhxzhxustc@gmail.com](mailto:zhxzhxustc@gmail.com).

### License ###
Copyright (C) 2016 ANRED
Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at:
http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.

All libraries referred in ANRED's code base belong to their original authors and follow their own licenses. 