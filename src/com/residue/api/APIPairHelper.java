package com.residue.api;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import com.ibm.wala.classLoader.IBytecodeMethod;
import com.ibm.wala.classLoader.IClass;
import com.ibm.wala.classLoader.IMethod;
import com.ibm.wala.ipa.cha.IClassHierarchy;
import com.ibm.wala.shrikeBT.IInstruction;
import com.ibm.wala.shrikeCT.InvalidClassFileException;

public class APIPairHelper {

	IClassHierarchy cha = null;
	
	boolean USE_BASE = true;
	
	// java.util package criteria
	private String JAVA_UTIL_PATTERN = "java/util";
	private String[] JAVA_UTIL_EXCLUSIONS = {
			"java/util/concurrent",
			"java/util/logging",
			"java/util/SimpleTimeZone",
			"java/util/TimeZone",
			"java/util/regex",
			"java/util/EventObject",
			"java/util/ServiceConfigurationError",
			"java/util/zip",
			"java/util/ResourceBundle",
			"java/util/jar",
			"java.util/Formatter",
			"java/util/MissingFormatArgumentException",
			"java/util/prefs",
			"java/util/IllegalFormatPrecisionException",
			"java/util/MissingResourceException",
			"java/util/Locale",
			"java/util/Timer",
			"java/util/BitSet",
			"java/util/Scanner",
			"java/util/Random",
			"java/util/JapaneseImperialCalendar",
			"java/util/Calendar",
			"java/util/Currency",
			"java/util/IllegalFormatWidthException",
			"java/util/ServiceLoader",
			"java/util/IllegalFormatCodePointException",
			"java/util/TimerThread",
			"java/util/UnknownFormatFlagsException",
			"java/util/XMLUtils",
			"java/util/Observable",
			"java/util/Date",
			"java/util/Objects",
			"java/util/UUID",
			"java/util/StringTokenizer",
			"java/util/DualPivotQuicksort",
			"java/util/IllformedLocaleException",
			"java/util/GregorianCalendar",
			"java/util/PropertyPermission",
			"java/util/DuplicateFormatFlagsException",
			"$"
			
	};
	private String[] JAVA_UTIL_SAVING_PATTERN
		= {"put", "set", "add", "push", "offer", "copy"};
	private String[] JAVA_UTIL_REMOVING_PATTERN
	= {"remove", "poll", "clear", "retain"};
	
	
	// Settings package criteria
	private String SETTINGS_PATTERN = "android/provider/Settings";
	private String[] SETTINGS_EXCLUSIONS={};
	private String[] SETTINGS_SAVING_PATTERN = {"set", "add", "put"};
	private String[] SETTINGS_DELETING_PATTERN = {"set", "add", "put"};
	
	// android database package criteria
	private String ANDROID_DATABASE_PATTERN = "SQLiteDatabase";
	private String[] ANDROID_DATABASE_EXCLUSIONS={"Exception", "Bookmarks", "NameValueTable", "NameValueCache","$"};
	private String[] ANDROID_DATABASE_SAVING_PATTERN = {"execSQL", "update", "insert", "create", "replace"};
	private String[] ANDROID_DATABASE_DELETING_PATTERN = {"update", "execSQL", "delete", "replace", "insert"};

	// android xml parser criteria
	private String ANDROID_XML_PATTERN = "XmlSerializer";
	private String[] ANDROID_XML_EXCLUSIONS={"$"};
	private String[] ANDROID_XML_SAVING_PATTERN = {"setOutput"};
	private String[] ANDROID_XML_DELETING_PATTERN = {"setOutput"};
	
	// android sharedPreference  criteria
	private String ANDROID_SHAREDPREFERENCE_PATTERN = "SharedPreferences";
	private String[] ANDROID_SHAREDPREFERENCE_EXCLUSIONS={};
	private String[] ANDROID_SHAREDPREFERENCE_SAVING_PATTERN = {"commit", "apply"};
	private String[] ANDROID_SHAREDPREFERENCE_DELETING_PATTERN = {"commit", "apply"};

	
	// android file  criteria
	private String ANDROID_FILE_PATTERN = "java/io";
	private String[] ANDROID_FILE_EXCLUSIONS={
			"Exception",
			"java/io/Console",
			"$"
	};
	private String[] ANDROID_FILE_SAVING_PATTERN = {"write", "append", "print", "put"};
	private String[] ANDROID_FILE_DELETING_PATTERN = {"delete"};

	
	// android content provider  criteria
		private String CONTENT_PROVIDER_PATTERN = "android/content/ContentResolver";
		private String[] CONTENT_PROVIDER_EXCLUSIONS={
				"Exception",
				"$"
		};
		private String[] CONTENT_PROVIDER_SAVING_PATTERN = {"Insert", "insert", "update"};
		private String[] CONTENT_PROVIDER_DELETING_PATTERN = {"delete", "update"};
	
	
	public APIPairHelper(IClassHierarchy cls){
		cha = cls;
	}
	
	/*
	 * Detect the saving/deleting API pair
	 */
	public void detectAPIPairs() throws IOException{
		/*traverseForlAPIPairs(JAVA_UTIL_PATTERN, JAVA_UTIL_EXCLUSIONS,
				JAVA_UTIL_SAVING_PATTERN, JAVA_UTIL_REMOVING_PATTERN);
		traverseForlAPIPairs(SETTINGS_PATTERN, SETTINGS_EXCLUSIONS,
				SETTINGS_SAVING_PATTERN,SETTINGS_DELETING_PATTERN);
		traverseForlAPIPairs(ANDROID_DATABASE_PATTERN, ANDROID_DATABASE_EXCLUSIONS,
				ANDROID_DATABASE_SAVING_PATTERN, ANDROID_DATABASE_DELETING_PATTERN);
		traverseForlAPIPairs(ANDROID_XML_PATTERN, ANDROID_XML_EXCLUSIONS,
				ANDROID_XML_SAVING_PATTERN,ANDROID_XML_DELETING_PATTERN);
		traverseForlAPIPairs(ANDROID_SHAREDPREFERENCE_PATTERN, ANDROID_SHAREDPREFERENCE_EXCLUSIONS,
				ANDROID_SHAREDPREFERENCE_SAVING_PATTERN, ANDROID_SHAREDPREFERENCE_DELETING_PATTERN);
		traverseForlAPIPairs(ANDROID_FILE_PATTERN, ANDROID_FILE_EXCLUSIONS, 
				ANDROID_FILE_SAVING_PATTERN, ANDROID_FILE_DELETING_PATTERN);*/
		traverseForlAPIPairs(CONTENT_PROVIDER_PATTERN, CONTENT_PROVIDER_EXCLUSIONS, 
				CONTENT_PROVIDER_SAVING_PATTERN, CONTENT_PROVIDER_DELETING_PATTERN);
		//checkClassUsage("android/provider/Settings");
	}
	
	/*
	 * Check usage of classes in one particular package.
	 */
	private Set<String> checkClassUsage(String pkg){
		Set<String> classSet = new HashSet<String>();
		for (IClass c : cha) {
			String cname = c.getName().toString();
			//System.out.println("cname: " + cname);
			if(!cname.contains("android"))continue;
			
			for (IMethod m : c.getAllMethods()) {
				String mname = m.getName().toString();			
				try {
					IBytecodeMethod method = (IBytecodeMethod)m;
					if(method!=null && method.getInstructions()!=null){						
						for (IInstruction instruct : method.getInstructions()){
							if(instruct.toString().contains(pkg)){
								//System.err.println("	instruction: " + instruct.toString());
								String[] sub = instruct.toString().split(";");
								for(String str:sub){
									int position = str.indexOf(pkg);
									if(position!=-1){
										classSet.add(str.substring(position));
									}
									
								}
							}								
						}
					}
				} catch (InvalidClassFileException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}	
		
		System.err.println(classSet.size() + " uniques classes in " + pkg);
		/*for(String str : classSet)
			System.err.println(str);*/
		return classSet;
	}
	
	/*
	 * Save all methods from PATTERN package with EXCLUSIONS. Then we further 
	 * inspect those methods for saving/deleting pairs.
	 */
	private void traverseForlAPIPairs(String pattern, String[] exclusions, 
			String[] methods_saving_pattern, String[] methods_deleting_pattern) throws IOException {
		FileWriter fw = null;
		String filename= "./out/API/" + pattern.replace('/', '_');
		if(USE_BASE)
			filename = filename+"_base";
		
		fw = new FileWriter(filename,false); 
		
		int total_class = 0;
		int exclude_class = 0;
		int unused_class = 0;
		int total_methods = 0;
		
		Set<String> methodSet = new HashSet<String>();
		Set<String> savingMethodSet = new HashSet<String>();
		Set<String> deletingMethodSet = new HashSet<String>();

		// classes matching with the pattern, and also used in AOSP;
		Set<String> usedClasses = null;
		if(USE_BASE)
			usedClasses = checkClassUsage(pattern);
				
		Iterator<IClass> classIt = cha.iterator();
		while (classIt.hasNext()) {
			IClass clazz = classIt.next();	
			
			// Match pattern?
			if (!clazz.getName().toString().contains(pattern)) {
				continue;
			}
			total_class++;
			total_methods = total_methods + clazz.getAllMethods().size();
			
			// Exclude?			
			if(isPresent(clazz.getName().toString(), exclusions)) {
				exclude_class++;
				continue;
			}
			if(USE_BASE && 
					!isPresent(clazz.getName().toString(), usedClasses.toArray(new String[usedClasses.size()]))) {
				unused_class++;
				continue;
			}
			
			//System.err.println(clazz.getName().toString());
			
			// Output
			for (IMethod method : clazz.getAllMethods()) {
				String junitDesc = method.getSignature();
				methodSet.add(junitDesc);
				IClass declClass = method.getDeclaringClass();
				
				if (method.isInit() || !method.isPublic()) {
					continue;
				}
				//if(junitDesc.contains(pattern.replace('/', '.'))){
				if(isPresent(method.getName().toString(), methods_saving_pattern))
					savingMethodSet.add(junitDesc);
				if(isPresent(method.getName().toString(), methods_deleting_pattern))
					deletingMethodSet.add(junitDesc);
				//}
					
			}
		}
		
		// Summary
		fw.write("\nSUMMARY: " + total_class + " classes are matching the pattern " + pattern + "\n");
		fw.write("         " + total_methods + " APIs totally\n\n");
		fw.write("         " + exclude_class + " classes are excluded because of non-related to read or write\n");
		fw.write("         " + unused_class + " classes are excluded because of unused in AOSP\n\n");
		fw.write("         " + (total_class-exclude_class-unused_class) + " classes are left\n");
		fw.write("         " + methodSet.size() + " APIs require examination\n");
		
		Set<String> combinedSet = new HashSet<String>(savingMethodSet);
		combinedSet.addAll(deletingMethodSet);
		
		fw.write("         " + combinedSet.size() + " unique candidate APIs\n");
		fw.write("         " + savingMethodSet.size() + " saving APIs\n");
		fw.write("         " + deletingMethodSet.size() + " deleting APIs\n\n\n");
		
		fw.write("=============> Saving APIs <==============\n");		
		for(String method : savingMethodSet){
			fw.write("\"" + method + "\"," +"\n");
		}
		fw.write("\n\n\n");
		
		fw.write("=============> Deleting APIs <==============\n");
		for(String method : deletingMethodSet){
			fw.write("\"" + method + "\"," +"\n");
		}

		if(fw!=null)fw.close();
	}
	
	/*
	 * Determine whether candidate string contains one key word in the string set.
	 */
	private boolean isPresent(String candidate, String[] set){
		if(set==null)return false;
		for(String str : set){
			if(candidate.contains(str))return true;
		}
		return false;
	}
}
