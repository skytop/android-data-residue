package com.residue.slice;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;

import com.ibm.wala.ipa.callgraph.CallGraph;
import com.ibm.wala.ipa.callgraph.CallGraphBuilder;
import com.ibm.wala.ipa.callgraph.propagation.PointerAnalysis;
import com.ibm.wala.ipa.slicer.NormalStatement;
import com.ibm.wala.ipa.slicer.Slicer;
import com.ibm.wala.ipa.slicer.Statement;
import com.ibm.wala.ipa.slicer.Slicer.ControlDependenceOptions;
import com.ibm.wala.ipa.slicer.Slicer.DataDependenceOptions;
import com.ibm.wala.ssa.SymbolTable;
import com.residue.driver.Configuration;

public class SliceUtility {

	/*
	 *  Current is backward slice, can also do forward slice.
	 */
	public Collection<Statement> sliceCGNode(Statement s,CallGraph cg,CallGraphBuilder builder){
		if(Configuration.DEBUG_SAVING){
			System.out.println("Backslicing using seed: ");
			System.err.println("	---" + s.toString() + "| " + s.getNode().getMethod().getDeclaringClass());
		}
		return slice_using_seed(s, cg, builder.getPointerAnalysis(), true);
	}

	/*
	 * Compute slice 
	 */
	private Collection<Statement> slice_using_seed(Statement seed, CallGraph cg, PointerAnalysis pa, boolean backward) 
	{
		Collection<Statement> slice ;
		try{
			if(backward)
				slice = Slicer.computeBackwardSlice(seed, cg, pa,DataDependenceOptions.NO_BASE_NO_HEAP_NO_EXCEPTIONS, ControlDependenceOptions.FULL);
			else
				slice = Slicer.computeForwardSlice(seed, cg, pa, DataDependenceOptions.NO_BASE_NO_EXCEPTIONS, ControlDependenceOptions.FULL);
			
			//dumpSlice(slice,cg);

			return slice;
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	/*
	 * Dump slice information
	 */
	public void dumpSlice(Collection<Statement> slice, String filename) throws IOException {		
		FileWriter fw = new FileWriter(filename,false); //the true will append the new data	
		for (Statement s : slice){
			fw.write(s.toString()+"\n\n");
		}
		fw.close();
	}
}
