package com.residue.apk;


/*

commands ran in ubuntu command line :
=====================================

./apktool decode ../Sample\ app/app-release.apk ../Sample\ app/app1/

unzip -o ../Sample\ app/app-release.apk -d ../Sample\ app/app1/

sh ./dex2jar.sh ../Sample\ app/app1/classes.dex 

*/

import java.io.File;
import java.util.regex.Pattern;
import java.io.*;

import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

class Apkpreprocess {

	public static void main(String[] args)
	{
		System.out.println("Extracting APK Set .. \n");

		File apksDir = new File("../apks");

		if(apksDir.isDirectory()){

			String[] apks = apksDir.list();

			for(String filename : apks){

				// print apk name
				System.out.println(filename);
	
				// make new directory
				String[] words = filename.split(Pattern.quote("."));

				String name = "";
				int i;

				for(i=0; i<words.length-2; i++) {
					
					name += words[i] + "_";
				}
				name += words[i];

				// String appdir = "../extracted/" + words[0];
				String appdir = "../extracted/" + name;

				File dir = new File(appdir);
				dir.mkdir();

				try {

					System.out.println("./apktool/apktool decode -f ../apks/" + filename + " " + appdir + "/");

					
					/* run apktool */

					try {
						Process p = Runtime.getRuntime().exec("./apktool/apktool decode -f ../apks/" + filename + " " + appdir + "/");
						p.waitFor();
					
					} catch (Exception e) {
						e.printStackTrace();
					}

					System.out.println("unzipping apk .. ");

					/* unzip apk */

					unZipIt("../apks/" + filename, dir.getCanonicalPath());

					/* convert dex to jar using dex2jar */

					// sh ./dex2jar.sh ../Sample\ app/app1/classes.dex

					try {
						Process p = Runtime.getRuntime().exec("sh ./dex2jar.sh " + appdir + "/classes.dex");
						p.waitFor();
					
					} catch (Exception e) {
						e.printStackTrace();
					}


				}
				catch(IOException e) {

					System.out.println(e.getMessage());
				}

				System.out.println("\n\n");
			}
		}
	}

	public static void unZipIt(String zipFile, String outputFolder){
 
	     byte[] buffer = new byte[1024];
	 
	     try{
	 
		/*
	    	//create output directory is not exists
	    	File folder = new File(outputFolder);
	    	if(!folder.exists()){
	    		folder.mkdir();
	    	}
		*/
	 
	    	//get the zip file content
	    	ZipInputStream zis = new ZipInputStream(new FileInputStream(zipFile));

	    	//get the zipped file list entry
	    	ZipEntry ze = zis.getNextEntry();
	 
	    	while(ze!=null){
	 
	    	   String fileName = ze.getName();

		   if(fileName.toLowerCase().contains("dex")) {

	                   System.out.println("filename : " + fileName);

			   File newFile = new File(outputFolder + File.separator + fileName);
		 
			   System.out.println("file unzip : "+ newFile.getAbsoluteFile());
		 
			    //create all non exists folders
			    //else you will hit FileNotFoundException for compressed folder
			    //new File(newFile.getParent()).mkdirs();
		 
			    FileOutputStream fos = new FileOutputStream(newFile);             
		 
			    int len;
			    while ((len = zis.read(buffer)) > 0) {
		       		fos.write(buffer, 0, len);
			    }
		 
			    fos.close();   
			    
		    }

		    ze = zis.getNextEntry();
	    	}
	 
		zis.closeEntry();
	    	zis.close();
	 
	    	System.out.println("Done");
	 
	    }catch(IOException ex){
	       ex.printStackTrace(); 
	    }
       }    
}
