package com.residue.analysis;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map.Entry;

import com.ibm.wala.classLoader.IBytecodeMethod;
import com.ibm.wala.classLoader.IClass;
import com.ibm.wala.classLoader.IMethod;
import com.ibm.wala.core.tests.callGraph.CallGraphTestUtil;
import com.ibm.wala.ipa.callgraph.AnalysisCache;
import com.ibm.wala.ipa.callgraph.AnalysisOptions;
import com.ibm.wala.ipa.callgraph.AnalysisScope;
import com.ibm.wala.ipa.callgraph.CallGraph;
import com.ibm.wala.ipa.callgraph.CallGraphBuilderCancelException;
import com.ibm.wala.ipa.callgraph.Entrypoint;
import com.ibm.wala.ipa.callgraph.impl.DefaultEntrypoint;
import com.ibm.wala.ipa.callgraph.impl.Util;
import com.ibm.wala.ipa.cha.IClassHierarchy;
import com.ibm.wala.shrikeBT.Constants;
import com.ibm.wala.shrikeBT.IInstruction;
import com.ibm.wala.shrikeCT.InvalidClassFileException;
import com.residue.driver.Configuration;

public class CallGraphHelper {
	
	private com.ibm.wala.ipa.callgraph.CallGraphBuilder builder = null;
	
	/*
	 * Collect all entry points within the service classes, including the lifetcycle functions
	 * and broken methods inside internal interfaces/abstract classes.
	 */
	public ArrayList<Entrypoint> collectEntryPoints(Entry<String,String> serviceEntry, IClassHierarchy cha, 
			AnalysisScope scope) throws IOException, IllegalArgumentException, CallGraphBuilderCancelException{
		LifeCycleEntryPoints life = new LifeCycleEntryPoints();
		final ArrayList<Entrypoint> entryPoints = life.getLifeCycleEntryPointsForService(serviceEntry, cha, scope);
		
		// Those broken points are not patched, instead, we want to consider them as entry points.
		if(!Configuration.ENABLE_GENERIC_REWRITER){
			BrokenEntryPoints broken = new BrokenEntryPoints(cha, serviceEntry.getKey());
			entryPoints.addAll(broken.getBrokenEntryPointsForService());
		}
		
		//dumpEntryPoints(entryPoints, serviceEntry.getKey());
		return entryPoints;
	}
	
	public com.ibm.wala.ipa.callgraph.CallGraphBuilder getCGBuilder(){
		return builder;
	}
	
	/*
	 * Split the entry points set into two subset: saving set and deleting set.
	 */
	public ArrayList<Entrypoint> splitEntryPoints(ArrayList<Entrypoint> entryPoints){
		ArrayList<Entrypoint> deletingSet = new ArrayList<Entrypoint>();
		ArrayList<Entrypoint> savingSet = new ArrayList<Entrypoint>(entryPoints);
		boolean isReceivingPackageRemoval = isReceivingPackageRemoval(entryPoints);
		for(Entrypoint entry : savingSet){
			IMethod method = entry.getMethod();			
			if(method.getName().toString().contains("init>")){				
				deletingSet.add(entry);
				continue;
			}
			
			IClass cls = method.getDeclaringClass();
			IClass superCls = cls.getSuperclass();
			Collection<IClass> interfaces = cls.getAllImplementedInterfaces();
			
			/*if(superCls!=null){
				System.err.println("Method: " + method.getName().toString());
				System.err.println("	class: " + cls.getName().toString());
				System.err.println("	interfaces: " + cls.getAllImplementedInterfaces());
				System.err.println("	superClass: " + superCls.getName().toString());
			}*/
			if(superCls!=null && superCls.getName().toString().equals(Configuration.PackageMonitorClass)){
				for(String str : Configuration.PackageMonitorAPIs){
					if(str.equals(method.getName().toString() + method.getDescriptor())){
						deletingSet.add(entry);
						entryPoints.remove(entry);
						//System.err.println("	===> " + method.getSignature().toString());
						continue;
					}
				}			
			}
			
			for(IClass interf : interfaces){
				if(interf.getName().toString().equals(Configuration.BinderDeathClass)){
					if((method.getName().toString()+method.getDescriptor()).equals(Configuration.BinderDeathAPIs)){
						deletingSet.add(entry);
						entryPoints.remove(entry);
						//System.err.println("	===> " + method.getSignature().toString());
						continue;
					}
				}
				
				if(interf.getName().toString().equals(Configuration.RegisteredServicesCacheClass)){
					if((method.getName().toString()+method.getDescriptor()).equals(Configuration.RegisteredServicesCacheAPIs)){
						deletingSet.add(entry);
						entryPoints.remove(entry);
						//System.err.println("	===> " + method.getSignature().toString());
						continue;
					}
				}
			}
			
			if(superCls!=null && superCls.getName().toString().equals(Configuration.BroadcastReceiverClass)){
				if((method.getName().toString()+method.getDescriptor()).equals(Configuration.BroadcastReceiverAPIs)){							
					if(isReceivingPackageRemoval){								
						deletingSet.add(entry);
					}
				}			
			}
			
		}
		
		return deletingSet;
	}
	
	/*
	 * Given a class name, find the outer class name.
	 * Used to detect all internal classes.
	 */
	private String findOuterClassName(String cname){
		String target = cname;
		int index = target.indexOf("$");
		if(index==-1)index=target.length();
		target = target.substring(0, index);
		return target;
	}
	
	/*
	 * Return whether this class and its outer class contains any logic in handling package removal.
	 */
	private boolean isReceivingPackageRemoval(ArrayList<Entrypoint> entryPoints){
		for(Entrypoint entry : entryPoints){
			IClass cls = entry.getMethod().getDeclaringClass();
			for(IMethod method : cls.getDeclaredMethods()){
				IBytecodeMethod method2 = (IBytecodeMethod)method;
				try {
					if(method2!=null && method2.getInstructions()!=null){						
						for (IInstruction instruct : method2.getInstructions()){
							if(instruct.toString().contains("android.intent.action.PACKAGE_REMOVED")
									|| instruct.toString().contains("android.intent.action.PACKAGE_CHANGED")
									|| instruct.toString().contains("android.intent.extra.REPLACING")
									|| instruct.toString().contains("android.intent.extra.REMOVED_FOR_ALL_USERS")
									|| instruct.toString().contains("android.intent.extra.DATA_REMOVED")
									|| instruct.toString().contains("android.intent.extra.DONT_KILL_APP")
									|| instruct.toString().contains("android.intent.extra.changed_component_name_list")
									|| instruct.toString().contains("android.intent.extra.UID")
									|| instruct.toString().contains("android.intent.action.UID_REMOVED")){								
								return true;
							}
							//System.err.println("	instruction: " + instruct.toString());	
						}
					}
				} catch (InvalidClassFileException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}		
		return false;
	}
	
	/*
	 * Build a callgraph for the given set of entry point
	 */
	public CallGraph build(final ArrayList<Entrypoint> entryPoints, IClassHierarchy cha, 
			AnalysisScope scope) throws IOException, IllegalArgumentException, CallGraphBuilderCancelException{
		
		Iterable<Entrypoint> mEntryPoints = new Iterable<Entrypoint>() {
			@Override
			public Iterator<Entrypoint> iterator() {
				return entryPoints.iterator();
			}
		};
		
		CallGraph cg = buildCallGraph(mEntryPoints, cha, scope);
		return cg;
	}

	private CallGraph buildCallGraph(Iterable<Entrypoint> entryPoints, IClassHierarchy cha, AnalysisScope scope) throws IllegalArgumentException, CallGraphBuilderCancelException{
		AnalysisOptions options = CallGraphTestUtil.makeAnalysisOptions(scope, entryPoints);
		builder =  Util.makeZeroCFABuilder(options, new AnalysisCache(), cha, scope, null,null);
		CallGraph cg = builder.makeCallGraph(options,null);
		return cg;
	}
	
	/*
	 * Dump all system service class entry points to disk
	 */
	private void dumpEntryPoints(ArrayList<Entrypoint> entryPoints, String binderClass){
		try
		{
			String filename= Configuration.ENTRY_POINT + binderClass.replace("/", "_");
			FileWriter fw = new FileWriter(filename,false); 
			
			//Only save the entry point methods.
			for (Entrypoint entry : entryPoints){
				fw.write(entry.getMethod().getSignature() +  "\n");
			}
			
			fw.close();
		} catch(IOException ioe){
			System.err.println("IOException: " + ioe.getMessage());
		}
	}
}
