package com.residue.analysis;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import com.ibm.wala.classLoader.IBytecodeMethod;
import com.ibm.wala.classLoader.IClass;
import com.ibm.wala.classLoader.IClassLoader;
import com.ibm.wala.classLoader.IMethod;
import com.ibm.wala.core.tests.callGraph.CallGraphTestUtil;
import com.ibm.wala.ipa.callgraph.AnalysisCache;
import com.ibm.wala.ipa.callgraph.AnalysisOptions;
import com.ibm.wala.ipa.callgraph.AnalysisScope;
import com.ibm.wala.ipa.callgraph.CGNode;
import com.ibm.wala.ipa.callgraph.CallGraph;
import com.ibm.wala.ipa.callgraph.CallGraphBuilderCancelException;
import com.ibm.wala.ipa.callgraph.Entrypoint;
import com.ibm.wala.ipa.callgraph.impl.DefaultEntrypoint;
import com.ibm.wala.ipa.callgraph.impl.Util;
import com.ibm.wala.ipa.cha.IClassHierarchy;
import com.ibm.wala.shrikeBT.IInstruction;
import com.ibm.wala.shrikeCT.InvalidClassFileException;
import com.ibm.wala.ssa.DefUse;
import com.ibm.wala.ssa.IR;
import com.ibm.wala.ssa.SSAInstruction;
import com.residue.driver.Configuration;


public class SystemServiceDetection {


	public SystemServiceDetection(){
		
	}
	
	
	/*
	 * From the class hierarchy, find all system service classes.
	 * We currently use one heuristics:
	 * To register as a system service, it needs to publish itself via
	 * the addService() APIs in android.os.ServiceManager class.
	 * 	addService(String name, IBinder service)
	 * 	addService(String name, IBinder service, boolean allowIsolated)
	 * 
	 * We iterate all instructions to find appearance of such APIs, and resolve
	 * the arguments inside.
	 * 
	 * TODO: We use the AOSP as basis and add new system services if it can be decompiled successfully.
	 * TODO: Save a pair of classes: key is the system service class, value is the exported IBinder class
	 * 
	 * Input: class hierarchy
	 * Output: a list of system service classes
	 */
	public HashMap<String, String> getSystemServiceClassesAccurate(IClassHierarchy cha, AnalysisScope scope) throws IOException, IllegalArgumentException, CallGraphBuilderCancelException, InvalidClassFileException{

		HashMap<String, String> SSClasses = new HashMap<String, String>();

		IClassLoader loader = cha.getFactory().getLoader(
				scope.getApplicationLoader(), cha, scope);

		// Add all public functions as entry points
		final ArrayList<Entrypoint> entrypoints = new ArrayList<Entrypoint>();		
		for (IClass c : cha) {
			
			String cname = c.getName().toString();
			//System.err.println(cname);
			
			// comment out the following line for vendor image processing
			//if(!cname.contains("android"))continue;

			// Build a small call graph, starting from methods that invoke the addService() API
			// or publishBinderService() API. The goal is to find all appearance of such APIs, and 
			// more importantly, to resolve the IBinder type.
			for (IMethod m : c.getAllMethods()) {
				String mname = m.getName().toString();			
				try {
					IBytecodeMethod method = (IBytecodeMethod)m;
					if(method!=null && method.getInstructions()!=null){						
						for (IInstruction instruct : method.getInstructions()){
							if(instruct.toString().contains("addService") || instruct.toString().contains("publishBinderService")){
								if(Configuration.DEBUG_SERVICE){
									System.out.println("    method: " + m.getName().toString());
									System.err.println("	instruction: " + instruct.toString());
								}

								DefaultEntrypoint de = new DefaultEntrypoint(m,cha);
								entrypoints.add(de);
							}								
						}
					}
				} catch (InvalidClassFileException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				/*if(m.getSignature().contains("android") && m.isPublic()){
					//System.out.println("Method signature: " + m.getSignature().toString());
					AIDLEntryPoints aep = new AIDLEntryPoints(mname,cname,m.getSignature());
					allPublicFunctions.add(aep);
				}*/
			}
			
		}		

		System.err.println("Build call graph for extracting system services");
		// Build call graph
		AnalysisOptions options = CallGraphTestUtil.makeAnalysisOptions(scope, new Iterable<Entrypoint>() {
			@Override
			public Iterator<Entrypoint> iterator() {
				return entrypoints.iterator();
			}
		});
		//com.ibm.wala.ipa.callgraph.CallGraphBuilder builder = Util.makeZeroCFABuilder(options, new AnalysisCache(), cha, scope, null,null);		
		// We want a fast algorithm here.
		com.ibm.wala.ipa.callgraph.CallGraphBuilder builder = Util.makeRTABuilder(options, new AnalysisCache(), cha, scope);		

		CallGraph cg = builder.makeCallGraph(options,null);

		for (Iterator<CGNode> nodes = cg.iterator(); nodes.hasNext();) {
			CGNode nd = nodes.next();
			if(nd.getIR()==null)continue;
			IR ir = nd.getIR();
			
			//if(ir.toString().contains("enqueueToast"))
			//	System.err.println("enqueueToast found.");

			// Exclude invocation from the SystemService or ServiceManager class
			if(nd.getMethod().getDeclaringClass().getName().toString().equals("Lcom/android/server/SystemService") 
					|| nd.getMethod().getDeclaringClass().getName().toString().equals("Landroid/os/ServiceManager"))
				continue;
			for (Iterator<SSAInstruction> it = ir.iterateAllInstructions(); it.hasNext();) {
				SSAInstruction s = it.next();

				if (s instanceof com.ibm.wala.ssa.SSAAbstractInvokeInstruction) {
					com.ibm.wala.ssa.SSAAbstractInvokeInstruction call = (com.ibm.wala.ssa.SSAAbstractInvokeInstruction) s;

					if ((call.getCallSite().getDeclaredTarget().getName().toString().equals("addService") &&
							call.getCallSite().getDeclaredTarget().getDeclaringClass().getName().toString().equals("Landroid/os/ServiceManager"))
							|| (call.getCallSite().getDeclaredTarget().getName().toString().equals("publishBinderService"))){


						if(Configuration.DEBUG_SERVICE)
							System.err.println("call: " + call.toString());

						int paraIndex = 1;
						if(call.toString().contains("invokevirtual"))paraIndex++;

						DefUse du = nd.getDU();
						if(du == null)continue;
						//System.err.println("du: " + du);

						SSAInstruction second = du.getDef(call.getUse(paraIndex));					    
						if(second==null)continue;
						//System.err.println("second: " + second);

						if (second instanceof com.ibm.wala.ssa.SSAAbstractInvokeInstruction) {
							com.ibm.wala.ssa.SSAAbstractInvokeInstruction secondcall = (com.ibm.wala.ssa.SSAAbstractInvokeInstruction) second;
							
							if(nd.getMethod().getDeclaringClass().getName().toString().contains("wala"))
								continue;
							
							if(Configuration.DEBUG_SERVICE){
								System.err.println("calling class: " + nd.getMethod().getDeclaringClass().getName().toString());
								System.err.println("Argument type-3: " + secondcall.getCallSite().getDeclaredTarget().getDeclaringClass().getName().toString());
							}
							
							SSClasses.put(secondcall.getCallSite().getDeclaredTarget().getDeclaringClass().getName().toString(), 
									nd.getMethod().getDeclaringClass().getName().toString());

						} else if (second instanceof com.ibm.wala.ssa.SSANewInstruction) {
							com.ibm.wala.ssa.SSANewInstruction secondcall = (com.ibm.wala.ssa.SSANewInstruction) second;
							if(nd.getMethod().getDeclaringClass().getName().toString().contains("wala"))
								continue;
							if(Configuration.DEBUG_SERVICE){
								System.err.println("calling class: " + nd.getMethod().getDeclaringClass().getName().toString());
								System.err.println("Argument type-4: " + secondcall.getConcreteType().getName());
								System.err.println("Argument type-5: " + secondcall.getConcreteType().getName().getClassName());
							}
							SSClasses.put(secondcall.getConcreteType().getName().toString(),
									nd.getMethod().getDeclaringClass().getName().toString());
						} else if  (second instanceof com.ibm.wala.ssa.SSAGetInstruction) {
							com.ibm.wala.ssa.SSAGetInstruction secondcall = (com.ibm.wala.ssa.SSAGetInstruction) second;
							if(secondcall.getDeclaredFieldType().getName().toString().equals("Landroid/os/IBinder")){	
								
								IClass serviceClass = nd.getMethod().getDeclaringClass();								
								String serviceName = serviceClass.getName().toString();								
								
								IClassHierarchy iCH = serviceClass.getClassHierarchy();
								Iterator<IClass> iterator = iCH.iterator();
								while(iterator.hasNext()){
									IClass cls = iterator.next();
									if(cls.getName().toString().contains(serviceName) && 
											cls.getSuperclass().getName().toString().contains("Stub")){
										String binderName = cls.getName().toString();
										
										SSClasses.put(binderName, serviceName);
										if(Configuration.DEBUG_SERVICE){
											System.err.println("calling class: " + serviceName);
											System.err.println("Argument type-1: " + binderName);
										}
									}
								}							
								
							} else {
								SSClasses.put(secondcall.getDeclaredFieldType().getName().toString(),
										nd.getMethod().getDeclaringClass().getName().toString());																
								if(Configuration.DEBUG_SERVICE){
									System.err.println("calling class: " + nd.getMethod().getDeclaringClass().getName().toString());
									System.err.println("Argument type-2: " + secondcall.getDeclaredFieldType());							
								}
								
								//SSClasses.add(secondcall.getDeclaredField().getDeclaringClass().getName().toString());
							}
						}		
					}
				}
			}

		}

		// Find all appearance of addServices() API
		if(Configuration.DEBUG_SERVICE){
			System.out.println("\nSummary: found " + SSClasses.size() + " system services.");
			dumpSystemServiceList(SSClasses);
		}
		
		return SSClasses;
	}
	
	/*
	 * Dump all system service class names to disk
	 */
	public void dumpSystemServiceList(HashMap<String, String> SSClasses){
		try
		{
			String filename= Configuration.SYSTEM_SERVICES;
			FileWriter fw = new FileWriter(filename,false); 

			for (Entry<String, String> entry : SSClasses.entrySet()){
				fw.write(entry.getKey() + "|" + entry.getValue() +  "\n");
			}
			fw.close();
		} catch(IOException ioe){
			System.err.println("IOException: " + ioe.getMessage());
		}
	}
	
	
	/*
	 * From the class hierarchy, find all system service classes.
	 * We currently use two heuristics:
	 * 1> system service class extends a stub class
	 * 2> system service class extends com.android.server.SystemService class
	 * False Positives: not all Stub classes are registered as services 
	 * False Negatives: not all system services are added as a Stub, IBinder object is all required.
	 * In AMS, ServiceManager.addService("dbinfo", new DbBinder(this));
	 * New heuristics:
	 * 3> system service class extends com.android.server.SystemService class
	 * 
	 * 
	 * Input: class hierarchy
	 * Output: a list of system service classes
	 */
	
	/*private ArrayList<String> getSystemServiceClasses(IClassHierarchy cha){
		ArrayList<String> SSClasses = new ArrayList<String>();
		int i=0, j=0;
		for (IClass c : cha) {
			String cname = c.getName().toString();
			IClass superclass = c.getSuperclass();
			if(superclass==null)continue;
			String supername = superclass.getName().toString();
			if(supername.contains("$Stub") || supername.contains("Binder")){				
				if(DEBUG_SERVICE)
					System.out.println("Class: "+cname+"   ,Super Class: " + supername);
				SSClasses.add(cname);
				i++;
				continue;
			}
			if(supername.equals("Lcom/android/server/SystemService")){				
				if(DEBUG_SERVICE)
					System.out.println("Class: "+cname+"   ,Super Class: " + supername);
				SSClasses.add(cname);
				j++;
			}
		}
		if(DEBUG_SERVICE){
			System.out.println("\nSummary: found " + i + " system services extending $Stub.");
			System.out.println("         found " + j + " system services extending Lcom/android/server/SystemService.\n");

		}
		return SSClasses;
	}*/
	
	
}
