package com.residue.analysis.JavaUtil;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import com.ibm.wala.classLoader.BytecodeClass;
import com.ibm.wala.classLoader.IBytecodeMethod;
import com.ibm.wala.classLoader.IClass;
import com.ibm.wala.classLoader.IField;
import com.ibm.wala.classLoader.IMethod;
import com.ibm.wala.ipa.callgraph.CGNode;
import com.ibm.wala.ipa.callgraph.CallGraph;
import com.ibm.wala.ipa.callgraph.impl.DefaultEntrypoint;
import com.ibm.wala.ipa.slicer.Statement;
import com.ibm.wala.shrikeBT.IInstruction;
import com.ibm.wala.shrikeCT.InvalidClassFileException;
import com.ibm.wala.ssa.IR;
import com.ibm.wala.ssa.SSAAbstractInvokeInstruction;
import com.ibm.wala.ssa.SSACheckCastInstruction;
import com.ibm.wala.ssa.SSAGetInstruction;
import com.ibm.wala.ssa.SSAInstruction;
import com.ibm.wala.ssa.SSANewInstruction;
import com.ibm.wala.types.TypeReference;
import com.ibm.wala.util.CancelException;
import com.residue.driver.Configuration;

public class JavaUtilSavingAnalysis {
	private Entry<String,String> mService;

	public JavaUtilSavingAnalysis(Entry<String,String> service) {
		mService = service;
		System.out.println("Analyzing JavaUtil Saving: " + service.getKey());
	}


	/*
	 * Analyze the usage of saving APIs from the given call graph.
	 * 		Iterate all IR nodes on the call graph;
	 * 		Iterate all instructions inside each IR;
	 * 		Find usage of saving APIs;
	 * 		Resolve critical entry info from the API usage;
	 */
	public HashMap<Statement, String> analyzeSavingAPIUsage(CallGraph cg,
			com.ibm.wala.ipa.callgraph.CallGraphBuilder builder) 
					throws InvalidClassFileException, IllegalArgumentException, CancelException{

		HashMap<Statement, String> seeds = new HashMap<Statement, String>();

		for (Iterator<CGNode> it = cg.iterator(); it.hasNext();) {
			CGNode nd = it.next();
			if(nd.getIR()==null)continue;

			for(String str : JavaUtilAPIs.SAVING_API){
				HashMap<Statement, String> statementSet = findCallTo(nd, str, cg, builder);				
				if(statementSet!=null){
					seeds.putAll(statementSet);
				}
			}
		}

		return seeds;

	}


	/*
	 * Copied from http://wala.sourceforge.net/wiki/index.php/UserGuide:Slicer
	 * Given a CGNode, and a methodName(seed), return the statement within CGNode
	 * that contains the seed method.
	 */
	public HashMap<Statement, String> findCallTo(CGNode n, String methodName, CallGraph cg,
			com.ibm.wala.ipa.callgraph.CallGraphBuilder builder) throws IllegalArgumentException, CancelException {

		HashMap<Statement, String> candidates = new HashMap<Statement, String>();

		// Skipping those CGNode that are not in the debugging service
		IClass declaringClass = n.getMethod().getDeclaringClass();
		String declaringClassName = declaringClass.getName().toString();
		String outerClassName = declaringClassName;
		int innerIndex = declaringClassName.indexOf('$');
		if(innerIndex!=-1)
			outerClassName = declaringClassName.substring(0, innerIndex);

		if(outerClassName.length()>mService.getKey().length() ||
				!outerClassName.equals(mService.getKey().substring(0, outerClassName.length())))
			return null;

		IR ir = n.getIR();

		for (Iterator<SSAInstruction> it = ir.iterateAllInstructions(); it.hasNext();) {
			SSAInstruction s = it.next();

			if (s instanceof com.ibm.wala.ssa.SSAAbstractInvokeInstruction) {
				com.ibm.wala.ssa.SSAAbstractInvokeInstruction call = (com.ibm.wala.ssa.SSAAbstractInvokeInstruction) s;

				if (call.getCallSite().getDeclaredTarget().getSignature().toString().equals(methodName)
						&& isGlobal(call, n, cg)) {						
					
					String entry = resolveJavaUtilSavingEntryEasy(n, ir, call, cg);
					//System.err.println("Instruction: " + s);
					System.err.println("Saving Entry: " + entry);

					if(Configuration.DEBUG_SAVING && entry==null)
						dumpSavingInstructionInfo(n, ir, call, s);

					com.ibm.wala.util.intset.IntSet indices = ir.getCallInstructionIndices(call.getCallSite());
					com.ibm.wala.util.debug.Assertions.productionAssertion(indices.size() == 1, "expected 1 but got " + indices.size());

					com.ibm.wala.ipa.slicer.NormalStatement normalStatement = new com.ibm.wala.ipa.slicer.NormalStatement(n, indices.intIterator().next());

					candidates.put(normalStatement, entry);
				}
			}
		}
		return candidates;
	}


	/*
	 * Given a invoke instruction on a Java util object, check whether that object
	 * is a global object or not. Local object is not of our concern.
	 */
	private boolean isGlobal(SSAAbstractInvokeInstruction call, CGNode n, CallGraph cg){
		try{		
			if(isImplementingInterfaces(call, n, cg)) return false;
			int id = call.getUse(0);
			SSAInstruction def = n.getDU().getDef(id);				
			if(def instanceof com.ibm.wala.ssa.SSAGetInstruction ||
					def.toString().contains("access$")){
				System.out.println(def);
				return true;
			}
			else return false;
		}catch(Exception e){
			return false;
		}		
	}
	
	private boolean isImplementingInterfaces(SSAAbstractInvokeInstruction call, CGNode n, CallGraph cg){
		/*if(n.getMethod().getSignature().toString().contains("applyRequirementsLocked")){
			System.out.println(n.getIR());
			System.out.println(call);
		}*/
		
		TypeReference type = getElementType(call, n);
		IClass cls = cg.getClassHierarchy().lookupClass(type);
		System.out.println("type: " + type.getName());
		System.out.println("cls: " + cls.getName());
		System.out.println("interfaces: " + cls.getAllImplementedInterfaces());
		boolean containsName = false;
		if(cls!=null){
			if(cls.getName().toString().equals("Ljava/lang/String"))return false;
			if(cls.isInterface()) return true;
			for(IClass mInterface : cls.getAllImplementedInterfaces()){
				if(mInterface.getName().toString().equals("Landroid/os/Parcelable")
						|| !mInterface.getName().toString().contains("android"))
					continue;
				else return true;
			}
			if(cls.isArrayClass()) return false;
			
			for(IField field : cls.getAllFields()){
				TypeReference typeReference = field.getFieldTypeReference();
				String fieldName = field.getName().toString();
				if(fieldName.contains("Name") || fieldName.contains("name"))
					containsName = true;
				System.out.println("Field = " + field);
				System.out.println("Field Name = " + fieldName);
				System.out.println("Field Class = " + typeReference);	

				if(typeReference.getName().toString().equals("Landroid/os/IBinder")
						|| typeReference.getName().toString().equals("Landroid/os/IInterface"))
					return true;	
				IClass fieldClass = cg.getClassHierarchy().lookupClass(typeReference);
				if(fieldClass!=null){
					System.out.println("Field Interfaces = " + fieldClass.getAllImplementedInterfaces());				
					for(IClass fieldInterface : fieldClass.getAllImplementedInterfaces()){
						if(fieldInterface.getName().toString().equals("Landroid/os/IBinder")
								|| fieldInterface.getName().toString().equals("Landroid/os/IInterface"))
							return true;
					}
				}
			}
			
		}
		System.out.println("containsName: " + containsName);
		return !containsName;
	}
	
	
	private TypeReference getElementType(SSAAbstractInvokeInstruction call, CGNode n){
		System.out.println("call = " + call);
		int id = call.getUse(1);	
		//System.out.println(call.getNumberOfUses());
		//System.out.println(call.getDeclaredTarget().getNumberOfParameters());	
		IR ir = n.getIR();
		if(ir == null)return null;
		for(int i=0;i<n.getIR().getNumberOfParameters();i++){
			if(n.getIR().getParameter(i) == id){
				System.out.println(n.getIR().getParameterType(i).getName());
				return ir.getParameterType(i);
			}
		}
		SSAInstruction use = n.getDU().getDef(id);
		System.out.println("use = " + use);
		if(use instanceof com.ibm.wala.ssa.SSAGetInstruction){
			System.out.println("1: " + ((SSAGetInstruction)use).getDeclaredFieldType().getName());
			return ((SSAGetInstruction)use).getDeclaredFieldType();
		} else if(use instanceof com.ibm.wala.ssa.SSACheckCastInstruction){
			System.out.println("2: " + ((SSACheckCastInstruction)use).getDeclaredResultTypes()[0].getName());
			return ((SSACheckCastInstruction)use).getDeclaredResultTypes()[0];
		} else if(use instanceof com.ibm.wala.ssa.SSAAbstractInvokeInstruction){
			System.out.println("3: " + ((SSAAbstractInvokeInstruction)use).getDeclaredResultType().getName());
			return ((SSAAbstractInvokeInstruction)use).getDeclaredResultType();
		} else if(use instanceof com.ibm.wala.ssa.SSANewInstruction){
			System.out.println("4: " + ((SSANewInstruction)use).getConcreteType().getName());
			return ((SSANewInstruction)use).getConcreteType();
		}
		return null;
	}
	
	/*
	 * Resolve the entry name for the Database saving API if it's a direct String usage.
	 */
	private String resolveJavaUtilSavingEntryEasy(CGNode n, IR ir, 
			com.ibm.wala.ssa.SSAAbstractInvokeInstruction call, CallGraph cg){
		try{
			int id = call.getUse(0);
			SSAInstruction def = n.getDU().getDef(id);
			if(def instanceof com.ibm.wala.ssa.SSAGetInstruction){
				SSAGetInstruction getDef = (SSAGetInstruction)def;	
				return getDef.getDeclaredField().getName().toString();
			} else if (def instanceof com.ibm.wala.ssa.SSAAbstractInvokeInstruction
					&& def.toString().contains("access$")){
				SSAAbstractInvokeInstruction invokeDef = (SSAAbstractInvokeInstruction)def;
				String access = invokeDef.getDeclaredTarget().getName().toString();
					Iterator<CGNode> succNodes = cg.getSuccNodes(n);
					while(succNodes.hasNext()){
						CGNode succNode = succNodes.next();
						if(succNode.getMethod().getName().toString().equals(access)){
							IR perIR = succNode.getIR();
							if(perIR!=null){
								for(SSAInstruction perIns : perIR.getInstructions()){
									if(perIns instanceof com.ibm.wala.ssa.SSAGetInstruction){
										SSAGetInstruction perDef = (SSAGetInstruction)perIns;			
										return perDef.getDeclaredField().getName().toString();
									}
								}
							}
						}
					}
			}

		}catch(Exception e){
			return "NA";
		}
		return "NA";
	}



	/*
	 * Dump out detailed information regarding one saving API in the context of
	 * current CGNode.
	 */
	private void dumpSavingInstructionInfo(CGNode n, IR ir, 
			com.ibm.wala.ssa.SSAAbstractInvokeInstruction call, SSAInstruction s){
		System.out.println("\n====== DEBUGGING SAVING INSTRUCTION INFO ======");

		//System.err.println("IR: " + ir.toString());
		System.err.println("IR method: " + n.getMethod().getDeclaringClass().getName().toString());
		System.err.println("CALL Instruction: " + s.toString());
		System.err.println("CALL Instruction getuses: " + call.getUse(1));
		System.err.println("CALL Instruction def: " + call.getDef());
		System.err.println("---callsite: " + call.getCallSite().toString());
		System.err.println("---callsite class: " + call.getCallSite().getClass().toString());
		System.err.println("---callsite declared target: " + call.getCallSite().getDeclaredTarget().toString());
		System.err.println("---callsite declared target method signature: " + call.getCallSite().getDeclaredTarget().getSignature().toString());
		System.err.println("---callsite declared target method descriptor: " + call.getCallSite().getDeclaredTarget().getDescriptor().toString());
		System.err.println("---callsite declared target name: " + call.getCallSite().getDeclaredTarget().getName().toString());
		System.err.println("---callsite declared target declaring class: " + call.getCallSite().getDeclaredTarget().getDeclaringClass().getName().toString());
		System.out.println("====== DONE ======\n\n");
	}
}
