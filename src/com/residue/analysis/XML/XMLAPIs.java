package com.residue.analysis.XML;

public class XMLAPIs {
	/* trace potential data residue candidate */
	// Storage operation 
	public final static String[] SAVING_API = {
		"com.android.internal.util.FastXmlSerializer.setOutput(Ljava/io/Writer;)V",
		"org.xmlpull.v1.XmlSerializer.setOutput(Ljava/io/OutputStream;Ljava/lang/String;)V",
		"org.xmlpull.v1.XmlSerializer.setOutput(Ljava/io/Writer;)V",
		"com.android.internal.util.FastXmlSerializer.setOutput(Ljava/io/OutputStream;Ljava/lang/String;)V"
		};

	// public AIDL function 
	public final static String[] DELETING_API = {
		"com.android.internal.util.FastXmlSerializer.setOutput(Ljava/io/Writer;)V",
		"org.xmlpull.v1.XmlSerializer.setOutput(Ljava/io/OutputStream;Ljava/lang/String;)V",
		"org.xmlpull.v1.XmlSerializer.setOutput(Ljava/io/Writer;)V",
		"com.android.internal.util.FastXmlSerializer.setOutput(Ljava/io/OutputStream;Ljava/lang/String;)V"
		};
}
