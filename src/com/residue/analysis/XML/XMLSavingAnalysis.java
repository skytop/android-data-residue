package com.residue.analysis.XML;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import com.ibm.wala.classLoader.IClass;
import com.ibm.wala.ipa.callgraph.CGNode;
import com.ibm.wala.ipa.callgraph.CallGraph;
import com.ibm.wala.ipa.slicer.NormalStatement;
import com.ibm.wala.ipa.slicer.Statement;
import com.ibm.wala.shrikeCT.InvalidClassFileException;
import com.ibm.wala.ssa.DefUse;
import com.ibm.wala.ssa.IR;
import com.ibm.wala.ssa.SSAAbstractInvokeInstruction;
import com.ibm.wala.ssa.SSAInstruction;
import com.ibm.wala.ssa.SymbolTable;
import com.ibm.wala.util.CancelException;
import com.residue.driver.Configuration;
import com.residue.slice.SliceUtility;

public class XMLSavingAnalysis {
	private Entry<String,String> mService;

	public XMLSavingAnalysis(Entry<String,String> service) {
		mService = service;
		System.out.println("Analyzing XML Saving: " + service.getKey());
	}


	/*
	 * Analyze the usage of saving APIs from the given call graph.
	 * 		Iterate all IR nodes on the call graph;
	 * 		Iterate all instructions inside each IR;
	 * 		Find usage of saving APIs;
	 * 		Resolve critical entry info from the API usage;
	 */
	public HashMap<Statement, String> analyzeSavingAPIUsage(CallGraph cg,
			com.ibm.wala.ipa.callgraph.CallGraphBuilder builder) 
					throws InvalidClassFileException, IllegalArgumentException, CancelException{

		HashMap<Statement, String> seeds = new HashMap<Statement, String>();

		for (Iterator<CGNode> it = cg.iterator(); it.hasNext();) {
			CGNode nd = it.next();
			if(nd.getIR()==null)continue;

			for(String str : XMLAPIs.SAVING_API){
				HashMap<Statement, String> statementSet = findCallTo(nd, str, cg, builder);				
				if(statementSet!=null){
					seeds.putAll(statementSet);
				}
			}
		}

		return seeds;

	}


	/*
	 * Copied from http://wala.sourceforge.net/wiki/index.php/UserGuide:Slicer
	 * Given a CGNode, and a methodName(seed), return the statement within CGNode
	 * that contains the seed method.
	 */
	public HashMap<Statement, String> findCallTo(CGNode n, String methodName, CallGraph cg,
			com.ibm.wala.ipa.callgraph.CallGraphBuilder builder) throws IllegalArgumentException, CancelException {

		HashMap<Statement, String> candidates = new HashMap<Statement, String>();

		// Skipping those CGNode that are not in the debugging service
		IClass declaringClass = n.getMethod().getDeclaringClass();
		String declaringClassName = declaringClass.getName().toString();
		String outerClassName = declaringClassName;
		int innerIndex = declaringClassName.indexOf('$');
		if(innerIndex!=-1)
			outerClassName = declaringClassName.substring(0, innerIndex);

		if(outerClassName.length()>mService.getKey().length() ||
				!outerClassName.equals(mService.getKey().substring(0, outerClassName.length())))
			return null;

		IR ir = n.getIR();

		for (Iterator<SSAInstruction> it = ir.iterateAllInstructions(); it.hasNext();) {
			SSAInstruction s = it.next();

			if (s instanceof com.ibm.wala.ssa.SSAAbstractInvokeInstruction) {
				com.ibm.wala.ssa.SSAAbstractInvokeInstruction call = (com.ibm.wala.ssa.SSAAbstractInvokeInstruction) s;

				if (call.getCallSite().getDeclaredTarget().getSignature().toString().equals(methodName)) {					

					//System.err.println("writing to file? : " + isWritingToFile(n,ir,call));
					if(!isWritingToFile(n,ir,call))
						continue;					

					com.ibm.wala.util.intset.IntSet indices = ir.getCallInstructionIndices(call.getCallSite());
					com.ibm.wala.util.debug.Assertions.productionAssertion(indices.size() == 1, "expected 1 but got " + indices.size());

					com.ibm.wala.ipa.slicer.NormalStatement normalStatement = new com.ibm.wala.ipa.slicer.NormalStatement(n, indices.intIterator().next());
					//System.err.println("normal instruction: " + normalStatement);
					
					String entry = resolveXMLSavingEntryEasy(normalStatement, n);
					
					System.err.println("Saving Entry: " + entry);
					if(Configuration.DEBUG_SAVING && entry==null)
						dumpSavingInstructionInfo(n, ir, call, s);
					
					candidates.put(normalStatement, entry);
				}
			}
		}
		return candidates;
	}


	private boolean isWritingToFile(CGNode n, IR ir, 
			com.ibm.wala.ssa.SSAAbstractInvokeInstruction call){
		int index = call.getUse(1);
		SSAInstruction def = n.getDU().getDef(index);
		if(def!=null){
			if(def.toString().contains("FileOutputStream") ||
					def.toString().contains("BufferedOutputStream"))
				return true;
		}
		for(int i=0; i< ir.getNumberOfParameters(); i++){
			int para = ir.getParameter(i);
			if(para == index){
				if(ir.getParameterType(i).getName().toString().contains("FileOutputStream")
						|| ir.getParameterType(i).getName().toString().contains("BufferedOutputStream"))
					return true;
			}
			//System.err.println(i + " : " + ir.getParameter(i) + " : " + ir.getParameterType(i));
		}
		
		return false;
	}
	
	
	/*
	 * Resolve the entry name for the Database saving API if it's a direct String usage.
	 */
	private String resolveXMLSavingEntryEasy(NormalStatement normalStatement, CGNode node){
		if(node==null)return "NA";
		IR ir = node.getIR();
		if(ir==null)return "NA";
		SSAInstruction[] instructions = ir.getInstructions();
		if(instructions == null) return "NA";
		for(SSAInstruction instruction : instructions){
			if(instruction==null)continue;
			if(instruction.toString().contains("startTag")){
				int def = instruction.getUse(2);			
				try{
					return ir.getSymbolTable().getStringValue(def);
				}catch(Exception e){
					return "NA";
				}
			}
		}
		
		//System.err.println("\n" + node.getDU().getUses(def));
		/*
		SliceUtility slicer = new SliceUtility();			
		Collection<Statement> slice = slicer.sliceCGNode(normalStatement, cg, builder);
		try{	
			for (Statement s : slice){
				slicer.dumpSlice(slice, "./out/slice.txt");
				if(s.toString().contains("< Application, Ljava/io/File, <init>(Ljava/lang/String;)V")){
					NormalStatement normal = (NormalStatement)s;
					//System.err.println(normal);
					int index = normal.getInstruction().getUse(1);
					//System.err.println(index);
					SymbolTable table = normal.getNode().getIR().getSymbolTable();
					//System.err.println(table.getStringValue(index));
					return table.getStringValue(index);
				}
			}
		} catch(Exception e){
			return "NA";
		}*/
		
		return "NA";
	}


	/*
	 * Dump out detailed information regarding one saving API in the context of
	 * current CGNode.
	 */
	private void dumpSavingInstructionInfo(CGNode n, IR ir, 
			com.ibm.wala.ssa.SSAAbstractInvokeInstruction call, SSAInstruction s){
		System.out.println("\n====== DEBUGGING SAVING INSTRUCTION INFO ======");

		//System.err.println("IR: " + ir.toString());
		System.err.println("IR method: " + n.getMethod().getDeclaringClass().getName().toString());
		System.err.println("CALL Instruction: " + s.toString());
		System.err.println("CALL Instruction getuses: " + call.getUse(1));
		System.err.println("CALL Instruction def: " + call.getDef());
		System.err.println("---callsite: " + call.getCallSite().toString());
		System.err.println("---callsite class: " + call.getCallSite().getClass().toString());
		System.err.println("---callsite declared target: " + call.getCallSite().getDeclaredTarget().toString());
		System.err.println("---callsite declared target method signature: " + call.getCallSite().getDeclaredTarget().getSignature().toString());
		System.err.println("---callsite declared target method descriptor: " + call.getCallSite().getDeclaredTarget().getDescriptor().toString());
		System.err.println("---callsite declared target name: " + call.getCallSite().getDeclaredTarget().getName().toString());
		System.err.println("---callsite declared target declaring class: " + call.getCallSite().getDeclaredTarget().getDeclaringClass().getName().toString());
		System.out.println("====== DONE ======\n\n");
	}
}
