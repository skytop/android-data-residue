package com.residue.analysis.ContentProvider;

public class ContentProviderAPIs {

	/* trace potential data residue candidate */
	// Storage operation 
	public final static String[] SAVING_API = {
		"android.content.ContentResolver.update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I",
		"android.content.ContentResolver.insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;",
		"android.content.ContentResolver.bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I"
		};

	// public AIDL function 
	public final static String[] DELETING_API = {
		"android.content.ContentResolver.update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I",
		"android.content.ContentResolver.delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I"
		};
}
