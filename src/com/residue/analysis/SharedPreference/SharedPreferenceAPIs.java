package com.residue.analysis.SharedPreference;

public class SharedPreferenceAPIs {
	/* trace potential data residue candidate */
	// Storage operation 
	public final static String[] SAVING_API = {
		"android.content.SharedPreferences$Editor.apply()V",
		"android.content.SharedPreferences$Editor.commit()Z"
		};

	// public AIDL function 
	public final static String[] DELETING_API = {
		"android.content.SharedPreferences$Editor.apply()V",
		"android.content.SharedPreferences$Editor.commit()Z"
		};
}
