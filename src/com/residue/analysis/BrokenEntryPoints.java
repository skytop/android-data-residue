package com.residue.analysis;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;

import com.ibm.wala.classLoader.IClass;
import com.ibm.wala.classLoader.IMethod;
import com.ibm.wala.ipa.callgraph.Entrypoint;
import com.ibm.wala.ipa.callgraph.impl.DefaultEntrypoint;
import com.ibm.wala.ipa.cha.IClassHierarchy;

public class BrokenEntryPoints {

	private IClassHierarchy cha;
	private String rewritingService;
	private HashMap<IClass, Collection<IClass>> allInterfaces =
			new HashMap<IClass, Collection<IClass>>();
	private HashMap<IClass, Collection<IClass>> allAbstracts =
			new HashMap<IClass, Collection<IClass>>();
	private ArrayList<IClass> otherInteralClasses = new ArrayList<IClass>();

	public BrokenEntryPoints(IClassHierarchy arg, String str){
		cha = arg;
		rewritingService = findOuterClassName(str);
	}

	/*
	 * Get a list of broken entry points for single system service.
	 */
	public ArrayList<Entrypoint> getBrokenEntryPointsForService() throws IOException{
		checkBrokenPoints(rewritingService);		
		final ArrayList<Entrypoint> entrypoints = collectBrokenEntryPoints();
		return entrypoints;
	}

	
	/*
	 * Collect all broken methods from the interfaces/abstract classes
	 * that all internal classes are extending/implementing
	 */
	private ArrayList<Entrypoint> collectBrokenEntryPoints(){
		ArrayList<Entrypoint> entrypoints = new ArrayList<Entrypoint>();
		Collection<String> superMethods = new HashSet<String>();
		for(IClass c : allInterfaces.keySet()){
			
			for(IClass cls : allInterfaces.get(c)){
				if(cls.getName().toString().equals("Landroid/os/Handler$Callback")
						|| cls.getName().toString().equals("Ljava/lang/Runnable")
						|| cls.getName().toString().equals("Lcom/android/internal/os/HandlerCaller$Callback"))
					continue;
				for(IMethod mth : cls.getDeclaredMethods())
					superMethods.add(mth.getName().toString());
			}
			
			for(IMethod m : c.getDeclaredMethods()){
				if((m.isPublic() || m.isProtected())
						&& (superMethods.contains(m.getName().toString())) || m.getName().toString().contains("init>")
						|| m.isBridge()){
					DefaultEntrypoint de = new DefaultEntrypoint(m,cha);
					entrypoints.add(de);
				}
			}
			superMethods.clear();
		}
		
		
		
		for(IClass c : allAbstracts.keySet()){
			for(IClass cls : allAbstracts.get(c)){
				if(cls.getName().toString().equals("Landroid/os/Handler$Callback")
						|| cls.getName().toString().equals("Ljava/lang/Runnable")
						|| cls.getName().toString().equals("Lcom/android/internal/os/HandlerCaller$Callback")
						|| cls.getName().toString().equals("Landroid/os/AsyncTask")){
					continue;
				}
				for(IMethod mth : cls.getDeclaredMethods())
					superMethods.add(mth.getName().toString());
			}			

			for(IMethod m : c.getDeclaredMethods()){				
				if((m.isPublic() || m.isProtected())
						&& (superMethods.contains(m.getName().toString())) || m.getName().toString().contains("init>")
						|| m.isBridge()){
					DefaultEntrypoint de = new DefaultEntrypoint(m,cha);
					entrypoints.add(de);
				}
			}
			superMethods.clear();
		}
		
		for(IClass c : this.otherInteralClasses){
			for(IMethod m : c.getDeclaredMethods()){
				if(m.getName().toString().contains("init>")){
					DefaultEntrypoint de = new DefaultEntrypoint(m,cha);
					entrypoints.add(de);
				}
			}
		}
		return entrypoints;
	}

	/*
	 * Get a list of internal classes from the given SystemService class, which
	 * potentially can lead to broken links.
	 */
	public void checkBrokenPoints(String serviceName){
		//System.out.println("service class: " + serviceName);
		Iterator<IClass> iterator = cha.iterator();
		while(iterator.hasNext()){
			IClass c = iterator.next();
			String cname = c.getName().toString();
		    if(cname.contains(findOuterClassName(serviceName))){
				//System.err.println("--->internal class: " + cname);
				if(c.getDirectInterfaces().size()>0){
					Collection<IClass> interfaces = (Collection<IClass>) c.getDirectInterfaces();
					allInterfaces.put(c, interfaces);
				}
				Collection<IClass> collection = extendsAbstractClass(c);
				if(collection.size()>0){
					allAbstracts.put(c, collection);
				}
				if(c.getDirectInterfaces().size()==0 && collection.size()==0)
					otherInteralClasses.add(c);
			}
		}
		//debugBrokenPoints();
	}

	/*
	 * Is the given class extending an abstract class at some point on
	 * the class hierarchy?
	 */
	private Collection<IClass> extendsAbstractClass(IClass cls){
		Collection<IClass> collection = new HashSet<IClass>();
		while(cls.getSuperclass()!=null){
			cls = cls.getSuperclass();
			if(cls.isAbstract()){
				collection.add(cls);
				collection.addAll(cls.getAllImplementedInterfaces());
			}
		}
		return collection;
	}

	/*
	 * Given a class name, find the outer class name.
	 * Used to detect all internal classes.
	 */
	private String findOuterClassName(String cname){
		String target = cname;
		int index = target.indexOf("$");
		if(index==-1)index=target.length();
		target = target.substring(0, index);
		return target;
	}

	/*
	 * Debug the collected broken points. Print them out in the console.
	 */
	private void debugBrokenPoints(){			
		for(Entry<IClass,  Collection<IClass>> broken : allInterfaces.entrySet()){	
			String cname = broken.getKey().getName().toString();
			System.err.println(cname + ": " + broken.getValue() + " (interface)");
		}
		for(Entry<IClass,  Collection<IClass>> broken : allAbstracts.entrySet()){	
			String cname = broken.getKey().getName().toString();
			System.err.println(cname + ": " + broken.getValue() + " (abstract)");
		}
	}
}
