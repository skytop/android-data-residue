package com.residue.analysis.Database;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import com.ibm.wala.classLoader.IClass;
import com.ibm.wala.ipa.callgraph.CGNode;
import com.ibm.wala.ipa.callgraph.CallGraph;
import com.ibm.wala.ipa.slicer.Statement;
import com.ibm.wala.shrikeCT.InvalidClassFileException;
import com.ibm.wala.ssa.IR;
import com.ibm.wala.ssa.SSAInstruction;
import com.ibm.wala.util.CancelException;
import com.residue.driver.Configuration;
import com.residue.slice.SliceUtility;

public class DatabaseSavingAnalysis {

	private Entry<String,String> mService;
	
	public DatabaseSavingAnalysis(Entry<String,String> service) {
		mService = service;
		System.out.println("Analyzing Database Saving: " + service.getKey());
	}
	

	/*
	 * Analyze the usage of saving APIs from the given call graph.
	 * 		Iterate all IR nodes on the call graph;
	 * 		Iterate all instructions inside each IR;
	 * 		Find usage of saving APIs;
	 * 		Resolve critical entry info from the API usage;
	 */
	public HashMap<Statement, String> analyzeSavingAPIUsage(CallGraph cg,
			com.ibm.wala.ipa.callgraph.CallGraphBuilder builder) 
					throws InvalidClassFileException, IllegalArgumentException, CancelException{

		HashMap<Statement, String> seeds = new HashMap<Statement, String>();

		for (Iterator<CGNode> it = cg.iterator(); it.hasNext();) {
			CGNode nd = it.next();
			if(nd.getIR()==null)continue;
			
			for(String str : DatabaseAPIs.SAVING_API){
				HashMap<Statement, String> statementSet = findCallTo(nd, str, cg, builder);				
				if(statementSet!=null){
					seeds.putAll(statementSet);
				}
			}
		}
		
		return seeds;
		
	}
	
	
	/*
	 * Copied from http://wala.sourceforge.net/wiki/index.php/UserGuide:Slicer
	 * Given a CGNode, and a methodName(seed), return the statement within CGNode
	 * that contains the seed method.
	 */
	public HashMap<Statement, String> findCallTo(CGNode n, String methodName, CallGraph cg,
			com.ibm.wala.ipa.callgraph.CallGraphBuilder builder) throws IllegalArgumentException, CancelException {

		HashMap<Statement, String> candidates = new HashMap<Statement, String>();
		
		// Skipping those CGNode that are not in the debugging service
		IClass declaringClass = n.getMethod().getDeclaringClass();
		String declaringClassName = declaringClass.getName().toString();
		String outerClassName = declaringClassName;
		int innerIndex = declaringClassName.indexOf('$');
		if(innerIndex!=-1)
			outerClassName = declaringClassName.substring(0, innerIndex);
		
		if(outerClassName.length()>mService.getKey().length() ||
				!outerClassName.equals(mService.getKey().substring(0, outerClassName.length())))
			return null;
		
		IR ir = n.getIR();
		
		for (Iterator<SSAInstruction> it = ir.iterateAllInstructions(); it.hasNext();) {
			SSAInstruction s = it.next();
					
			if (s instanceof com.ibm.wala.ssa.SSAAbstractInvokeInstruction) {
				com.ibm.wala.ssa.SSAAbstractInvokeInstruction call = (com.ibm.wala.ssa.SSAAbstractInvokeInstruction) s;
										
				if (call.getCallSite().getDeclaredTarget().getSignature().toString().equals(methodName)) {					
					
					String entry = resolveDBSavingEntryEasy(n, ir, call, cg);
					//System.err.println("Instruction: " + s);
					System.err.println("Saving Entry: " + entry);
					
					if(Configuration.DEBUG_SAVING && entry==null)
						dumpSavingInstructionInfo(n, ir, call, s);
					
					com.ibm.wala.util.intset.IntSet indices = ir.getCallInstructionIndices(call.getCallSite());
					com.ibm.wala.util.debug.Assertions.productionAssertion(indices.size() == 1, "expected 1 but got " + indices.size());

					com.ibm.wala.ipa.slicer.NormalStatement normalStatement = new com.ibm.wala.ipa.slicer.NormalStatement(n, indices.intIterator().next());
					
					candidates.put(normalStatement, entry);
				}
			}
		}
		return candidates;
	}
	
	
	/*
	 * Resolve the entry name for the Database saving API if it's a direct String usage.
	 */
	private String resolveDBSavingEntryEasy(CGNode n, IR ir, 
			com.ibm.wala.ssa.SSAAbstractInvokeInstruction call, CallGraph cg){
		
		int index = call.getUse(1);
		String entryName = null;
		try{
			entryName = ir.getSymbolTable().getStringValue(index);
		} catch(IllegalArgumentException e){			
		}

		/*if(call.getDeclaredTarget().getName().toString().equals("insert")){
			int contentvalues = call.getUse(3);
			System.err.println("Index: " + contentvalues);
			Iterator<SSAInstruction>values = n.getDU().getUses(contentvalues);
			System.err.println("values: " + values);
			while(values.hasNext()){
				SSAInstruction s = values.next();
				if (s instanceof com.ibm.wala.ssa.SSAAbstractInvokeInstruction) {
					com.ibm.wala.ssa.SSAAbstractInvokeInstruction value = (com.ibm.wala.ssa.SSAAbstractInvokeInstruction) s;
					System.out.println("instruction: " + value);
					if(value.getDeclaredTarget().getName().toString().equals("put")){
						int mark = value.getUse(1);	
						System.err.println("Index: " + mark);
						System.out.println("value: " + ir.getSymbolTable().getStringValue(mark));
					}
					
				}
			}
		}*/
		if(entryName!=null)return entryName;
		
		boolean fromParameter = false;
		int[] parameters = ir.getParameterValueNumbers();
		for(int para : parameters){
			if(index == para){
				if(Configuration.DEBUG_SAVING){
					System.out.println("Entry comes from the argument!");
					System.out.println(cg.getPredNodeCount(n) + " invocations to this method");
					System.out.println(cg.getPredNodes(n).next());
					System.out.println(cg.getEntrypointNodes().contains(n));						
				}
				fromParameter = true;
				break;
			}
		}		
		if(!fromParameter) entryName = null;
		else if(cg.getEntrypointNodes().contains(n))
			entryName = "*";
		else{
			entryName = "NA";
		}
		return entryName;
	}
	
	
	
	/*
	 * Dump out detailed information regarding one saving API in the context of
	 * current CGNode.
	 */
	private void dumpSavingInstructionInfo(CGNode n, IR ir, 
			com.ibm.wala.ssa.SSAAbstractInvokeInstruction call, SSAInstruction s){
		System.out.println("\n====== DEBUGGING SAVING INSTRUCTION INFO ======");
		
		//System.err.println("IR: " + ir.toString());
		System.err.println("IR method: " + n.getMethod().getDeclaringClass().getName().toString());
		System.err.println("CALL Instruction: " + s.toString());
		System.err.println("CALL Instruction getuses: " + call.getUse(1));
		System.err.println("CALL Instruction def: " + call.getDef());
		System.err.println("---callsite: " + call.getCallSite().toString());
		System.err.println("---callsite class: " + call.getCallSite().getClass().toString());
		System.err.println("---callsite declared target: " + call.getCallSite().getDeclaredTarget().toString());
		System.err.println("---callsite declared target method signature: " + call.getCallSite().getDeclaredTarget().getSignature().toString());
		System.err.println("---callsite declared target method descriptor: " + call.getCallSite().getDeclaredTarget().getDescriptor().toString());
		System.err.println("---callsite declared target name: " + call.getCallSite().getDeclaredTarget().getName().toString());
		System.err.println("---callsite declared target declaring class: " + call.getCallSite().getDeclaredTarget().getDeclaringClass().getName().toString());
		System.out.println("====== DONE ======\n\n");
	}
}
