package com.residue.analysis.Database;

public class DatabaseAPIs {
	/* trace potential data residue candidate */
	// Storage operation 
	public final static String[] SAVING_API = {
		"android.database.sqlite.SQLiteDatabase.insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J",
		"android.database.sqlite.SQLiteDatabase.insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J",
		"android.database.sqlite.SQLiteDatabase.replace(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J",
		"android.database.sqlite.SQLiteDatabase.insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J",
		"android.database.sqlite.SQLiteDatabase.update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I",
		"android.database.sqlite.SQLiteDatabase.updateWithOnConflict(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;I)I",
		"android.database.sqlite.SQLiteDatabase.execSQL(Ljava/lang/String;)V",
		"android.database.sqlite.SQLiteDatabase.create(Landroid/database/sqlite/SQLiteDatabase$CursorFactory;)Landroid/database/sqlite/SQLiteDatabase;",
		"android.database.sqlite.SQLiteDatabase.execSQL(Ljava/lang/String;[Ljava/lang/Object;)V",
		"android.database.sqlite.SQLiteDatabase.replaceOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J"
		};

	// public AIDL function 
	public final static String[] DELETING_API = {
		"android.database.sqlite.SQLiteDatabase.insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J",
		"android.database.sqlite.SQLiteDatabase.deleteDatabase(Ljava/io/File;)Z",
		"android.database.sqlite.SQLiteDatabase.insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J",
		"android.database.sqlite.SQLiteDatabase.replace(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J",
		"android.database.sqlite.SQLiteDatabase.insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J",
		"android.database.sqlite.SQLiteDatabase.update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I",
		"android.database.sqlite.SQLiteDatabase.updateWithOnConflict(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;I)I",
		"android.database.sqlite.SQLiteDatabase.execSQL(Ljava/lang/String;)V",
		"android.database.sqlite.SQLiteDatabase.delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I",
		"android.database.sqlite.SQLiteDatabase.execSQL(Ljava/lang/String;[Ljava/lang/Object;)V",
		"android.database.sqlite.SQLiteDatabase.replaceOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J"
		};
}
