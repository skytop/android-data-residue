package com.residue.analysis.Settings;

public class SettingsAPIs {

	/* trace potential data residue candidate */
	// Storage operation 
	public final static String[] SAVING_API = {
		"android.provider.Settings$Secure.putFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)Z",
		"android.provider.Settings$Secure.setLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;Z)V",
		"android.provider.Settings$System.putConfigurationForUser(Landroid/content/ContentResolver;Landroid/content/res/Configuration;I)Z",
		"android.provider.Settings$System.putFloatForUser(Landroid/content/ContentResolver;Ljava/lang/String;FI)Z",
		"android.provider.Settings$System.setShowGTalkServiceStatus(Landroid/content/ContentResolver;Z)V",
		"android.provider.Settings$System.putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z",
		"android.provider.Settings$Secure.putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z",
		"android.provider.Settings$Global.getBluetoothHeadsetPriorityKey(Ljava/lang/String;)Ljava/lang/String;",
		"java.lang.Throwable.setStackTrace([Ljava/lang/StackTraceElement;)V",
		"android.provider.Settings$Global.putLong(Landroid/content/ContentResolver;Ljava/lang/String;J)Z",
		"android.provider.Settings$Secure.putLong(Landroid/content/ContentResolver;Ljava/lang/String;J)Z",
		"android.provider.Settings$System.setShowGTalkServiceStatusForUser(Landroid/content/ContentResolver;ZI)V",
		"android.provider.Settings$Secure.putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z",
		"android.provider.Settings$Global.putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z",
		"android.provider.Settings$Secure.setLocationProviderEnabledForUser(Landroid/content/ContentResolver;Ljava/lang/String;ZI)Z",
		"android.provider.Settings$Global.getBluetoothInputDevicePriorityKey(Ljava/lang/String;)Ljava/lang/String;",
		"android.provider.Settings$NameValueCache.putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z",
		"android.provider.Settings$System.putLong(Landroid/content/ContentResolver;Ljava/lang/String;J)Z",
		"android.provider.Settings$System.putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z",
		"android.provider.Settings$Secure.putFloatForUser(Landroid/content/ContentResolver;Ljava/lang/String;FI)Z",
		"android.provider.Settings$Global.putFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)Z",
		"android.provider.Settings$System.putConfiguration(Landroid/content/ContentResolver;Landroid/content/res/Configuration;)Z",
		"java.lang.Throwable.addSuppressed(Ljava/lang/Throwable;)V",
		"android.provider.Settings$Secure.putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z",
		"android.provider.Settings$System.putFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)Z",
		"android.provider.Settings$System.putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z",
		"android.provider.Settings$Global.putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z",
		"android.provider.Settings$System.putLongForUser(Landroid/content/ContentResolver;Ljava/lang/String;JI)Z",
		"android.provider.Settings$Secure.putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z",
		"android.provider.Settings$Secure.putLongForUser(Landroid/content/ContentResolver;Ljava/lang/String;JI)Z",
		"android.provider.Settings$System.putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z",
		"android.provider.Settings$Global.putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z",
		"android.provider.Settings$Bookmarks.add(Landroid/content/ContentResolver;Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;CI)Landroid/net/Uri;"
	};

	// public AIDL function 
	public final static String[] DELETING_API = {
		"android.provider.Settings$Secure.putFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)Z",
		"android.provider.Settings$Secure.setLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;Z)V",
		"android.provider.Settings$System.putConfigurationForUser(Landroid/content/ContentResolver;Landroid/content/res/Configuration;I)Z",
		"android.provider.Settings$System.putFloatForUser(Landroid/content/ContentResolver;Ljava/lang/String;FI)Z",
		"android.provider.Settings$System.setShowGTalkServiceStatus(Landroid/content/ContentResolver;Z)V",
		"android.provider.Settings$System.putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z",
		"android.provider.Settings$Secure.putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z",
		"android.provider.Settings$Global.getBluetoothHeadsetPriorityKey(Ljava/lang/String;)Ljava/lang/String;",
		"java.lang.Throwable.setStackTrace([Ljava/lang/StackTraceElement;)V",
		"android.provider.Settings$Global.putLong(Landroid/content/ContentResolver;Ljava/lang/String;J)Z",
		"android.provider.Settings$Secure.putLong(Landroid/content/ContentResolver;Ljava/lang/String;J)Z",
		"android.provider.Settings$System.setShowGTalkServiceStatusForUser(Landroid/content/ContentResolver;ZI)V",
		"android.provider.Settings$Secure.putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z",
		"android.provider.Settings$Global.putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z",
		"android.provider.Settings$Secure.setLocationProviderEnabledForUser(Landroid/content/ContentResolver;Ljava/lang/String;ZI)Z",
		"android.provider.Settings$Global.getBluetoothInputDevicePriorityKey(Ljava/lang/String;)Ljava/lang/String;",
		"android.provider.Settings$NameValueCache.putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z",
		"android.provider.Settings$System.putLong(Landroid/content/ContentResolver;Ljava/lang/String;J)Z",
		"android.provider.Settings$System.putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z",
		"android.provider.Settings$Secure.putFloatForUser(Landroid/content/ContentResolver;Ljava/lang/String;FI)Z",
		"android.provider.Settings$Global.putFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)Z",
		"android.provider.Settings$System.putConfiguration(Landroid/content/ContentResolver;Landroid/content/res/Configuration;)Z",
		"java.lang.Throwable.addSuppressed(Ljava/lang/Throwable;)V",
		"android.provider.Settings$Secure.putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z",
		"android.provider.Settings$System.putFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)Z",
		"android.provider.Settings$System.putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z",
		"android.provider.Settings$Global.putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z",
		"android.provider.Settings$System.putLongForUser(Landroid/content/ContentResolver;Ljava/lang/String;JI)Z",
		"android.provider.Settings$Secure.putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z",
		"android.provider.Settings$Secure.putLongForUser(Landroid/content/ContentResolver;Ljava/lang/String;JI)Z",
		"android.provider.Settings$System.putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z",
		"android.provider.Settings$Global.putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z",
		"android.provider.Settings$Bookmarks.add(Landroid/content/ContentResolver;Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;CI)Landroid/net/Uri;"
	};
}
