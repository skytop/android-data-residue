package com.residue.analysis;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.ibm.wala.classLoader.IClass;
import com.ibm.wala.classLoader.IMethod;
import com.ibm.wala.ipa.callgraph.AnalysisScope;
import com.ibm.wala.ipa.callgraph.CGNode;
import com.ibm.wala.ipa.callgraph.CallGraph;
import com.ibm.wala.ipa.callgraph.Entrypoint;
import com.ibm.wala.ipa.callgraph.impl.DefaultEntrypoint;
import com.ibm.wala.ipa.cha.IClassHierarchy;
import com.ibm.wala.ipa.slicer.Statement;
import com.ibm.wala.shrikeCT.InvalidClassFileException;
import com.ibm.wala.ssa.IR;
import com.ibm.wala.ssa.SSAAbstractThrowInstruction;
import com.ibm.wala.ssa.SSABinaryOpInstruction;
import com.ibm.wala.ssa.SSAComparisonInstruction;
import com.ibm.wala.ssa.SSAConditionalBranchInstruction;
import com.ibm.wala.ssa.SSAGetCaughtExceptionInstruction;
import com.ibm.wala.ssa.SSAGotoInstruction;
import com.ibm.wala.ssa.SSAInstruction;
import com.ibm.wala.ssa.SSAReturnInstruction;
import com.ibm.wala.ssa.SSASwitchInstruction;
import com.ibm.wala.util.CancelException;
import com.residue.analysis.ContentProvider.ContentProviderDeletingAnalysis;
import com.residue.analysis.ContentProvider.ContentProviderSavingAnalysis;
import com.residue.analysis.Database.DatabaseDeletingAnalysis;
import com.residue.analysis.Database.DatabaseSavingAnalysis;
import com.residue.analysis.File.FileDeletingAnalysis;
import com.residue.analysis.File.FileSavingAnalysis;
import com.residue.analysis.JavaUtil.JavaUtilDeletingAnalysis;
import com.residue.analysis.JavaUtil.JavaUtilSavingAnalysis;
import com.residue.analysis.Settings.SettingsDeletingAnalysis;
import com.residue.analysis.Settings.SettingsSavingAnalysis;
import com.residue.analysis.SharedPreference.SharedPreferenceDeletingAnalysis;
import com.residue.analysis.SharedPreference.SharedPreferenceSavingAnalysis;
import com.residue.analysis.XML.XMLDeletingAnalysis;
import com.residue.analysis.XML.XMLSavingAnalysis;
import com.residue.driver.Configuration;
import com.residue.driver.Test;

public class Analysis {

	private static int num = 0;
	private Document doc;
	private Element image;

	public Analysis(Document d, Element e){
		doc = d;
		image = e;
	}
	/*
	 * Analyze one system service:
	 * 		We analyze the saving API usage from the CallGraph based on AIDL, life cycle functions and other entry points;
	 * 		We analyze the deleting API usage from the CallGraph based on application un-installation handling functions;
	 * 		We analyze the relationship between saving APIs and deleting APIs
	 */
	public void analyzeService(Entry<String,String> serviceEntry, IClassHierarchy cha, 
			AnalysisScope scope) throws IOException, IllegalArgumentException, CancelException, InvalidClassFileException{

		CallGraphHelper helper = new CallGraphHelper();
		ArrayList<Entrypoint> savingSet = helper.collectEntryPoints(serviceEntry, cha, scope);	
		Set<Entrypoint> hs = new HashSet<Entrypoint>();
		hs.addAll(savingSet);
		savingSet.clear();
		savingSet.addAll(hs);
		ArrayList<Entrypoint> deletingSet = helper.splitEntryPoints(savingSet);	

		//debugEntries(deletingSet,savingSet);

		// Construct the call graphs for saving analysis and deleting analysis
		CallGraph savingCG = helper.build(savingSet, cha, scope);	
		ArrayList<Entrypoint> newSet = patchCallGraph(savingCG);
		if(newSet.size()>0){
			savingSet.addAll(newSet);
			savingCG = helper.build(savingSet, cha, scope);	
		}

		CallGraphHelper deletingHelper = new CallGraphHelper();
		CallGraph deletingCG = deletingHelper.build(deletingSet, cha, scope);			
		newSet = patchCallGraph(deletingCG);
		if(newSet.size()>0){
			deletingSet.addAll(newSet);
			deletingCG = deletingHelper.build(deletingSet, cha, scope);	
		}
		//debugEntries(deletingSet,savingSet);
		//debugCG(savingCG);
		//debugCG(deletingCG);			

		// Analyze the Setting residue instances
		if(Configuration.SETTINGS_ENABLED){
			SettingsSavingAnalysis settingsSavingAnalysis = new SettingsSavingAnalysis(serviceEntry);
			HashMap<Statement, String> settingsSavings = settingsSavingAnalysis.analyzeSavingAPIUsage(savingCG);
			SettingsDeletingAnalysis settingsDeletingAnalysis = new SettingsDeletingAnalysis(serviceEntry);
			HashMap<Statement, String> settingsDeletings = settingsDeletingAnalysis.analyzeDeletingAPIUsage(deletingCG);
			dumpSavingAPIs(Configuration.SETTINGS_REPORT, settingsSavings, settingsDeletings, serviceEntry);
		}

		// Analyze the Database residue instances
		if(Configuration.DATABASE_ENABLED){
			DatabaseSavingAnalysis databaseSavingAnalysis = new DatabaseSavingAnalysis(serviceEntry);
			HashMap<Statement, String> databaseSavings = databaseSavingAnalysis.analyzeSavingAPIUsage(savingCG, helper.getCGBuilder());
			DatabaseDeletingAnalysis databaseDeletingAnalysis = new DatabaseDeletingAnalysis(serviceEntry);
			HashMap<Statement, String> databaseDeletings = databaseDeletingAnalysis.analyzeDeletingAPIUsage(deletingCG);
			dumpSavingAPIs(Configuration.DATABASE_REPORT, databaseSavings, databaseDeletings, serviceEntry);
		}

		// Analyze the SharedPreference residue instances
		if(Configuration.SHARED_PREFERENCE_ENABLED){
			SharedPreferenceSavingAnalysis sharedPreferenceSavingAnalysis = new SharedPreferenceSavingAnalysis(serviceEntry);
			HashMap<Statement, String> sharedPreferenceSavings = sharedPreferenceSavingAnalysis.analyzeSavingAPIUsage(savingCG, helper.getCGBuilder());
			SharedPreferenceDeletingAnalysis sharedPreferenceDeletingAnalysis = new SharedPreferenceDeletingAnalysis(serviceEntry);
			HashMap<Statement, String> sharedPreferenceDeletings = sharedPreferenceDeletingAnalysis.analyzeDeletingAPIUsage(deletingCG);
			dumpSavingAPIs(Configuration.SHARED_PREFERENCE_REPORT, sharedPreferenceSavings, sharedPreferenceDeletings, serviceEntry);
		}

		// Analyze the XML residue instances
		if(Configuration.XML_ENABLED){
			XMLSavingAnalysis xmlSavingAnalysis = new XMLSavingAnalysis(serviceEntry);
			HashMap<Statement, String> xmlSavings = xmlSavingAnalysis.analyzeSavingAPIUsage(savingCG, helper.getCGBuilder());
			XMLDeletingAnalysis xmlDeletingAnalysis = new XMLDeletingAnalysis(serviceEntry);
			HashMap<Statement, String> xmlDeletings = xmlDeletingAnalysis.analyzeDeletingAPIUsage(deletingCG, deletingHelper.getCGBuilder());
			dumpSavingAPIs(Configuration.XML_REPORT, xmlSavings, xmlDeletings, serviceEntry);
		}

		// Analyze the File residue instances
		if(Configuration.FILE_ENABLED){
			FileSavingAnalysis fileSavingAnalysis = new FileSavingAnalysis(serviceEntry);
			HashMap<Statement, String> fileSavings = fileSavingAnalysis.analyzeSavingAPIUsage(savingCG, helper.getCGBuilder());
			FileDeletingAnalysis fileDeletingAnalysis = new FileDeletingAnalysis(serviceEntry);
			HashMap<Statement, String> fileDeletings = fileDeletingAnalysis.analyzeDeletingAPIUsage(deletingCG);
			dumpSavingAPIs(Configuration.FILE_REPORT, fileSavings, fileDeletings, serviceEntry);
		}

		// Analyze the ContentProvider residue instances
		if(Configuration.CONTENT_PROVIDER_ENABLED){
			ContentProviderSavingAnalysis contentProviderSavingAnalysis = new ContentProviderSavingAnalysis(serviceEntry);
			HashMap<Statement, String> contentProviderSavings = contentProviderSavingAnalysis.analyzeSavingAPIUsage(savingCG, helper.getCGBuilder());
			ContentProviderDeletingAnalysis contentProviderDeletingAnalysis = new ContentProviderDeletingAnalysis(serviceEntry);
			HashMap<Statement, String> contentProviderDeletings = contentProviderDeletingAnalysis.analyzeDeletingAPIUsage(deletingCG);
			dumpSavingAPIs(Configuration.CONTENT_PROVIDER_REPORT, contentProviderSavings, contentProviderDeletings, serviceEntry);
		}

		// Analyze the ContentProvider residue instances
		if(Configuration.JAVA_UTIL_ENABLED){
			JavaUtilSavingAnalysis javaUtilSavingAnalysis = new JavaUtilSavingAnalysis(serviceEntry);
			HashMap<Statement, String> javaUtilSavings = javaUtilSavingAnalysis.analyzeSavingAPIUsage(savingCG, helper.getCGBuilder());
			JavaUtilDeletingAnalysis javaUtilDeletingAnalysis = new JavaUtilDeletingAnalysis(serviceEntry);
			HashMap<Statement, String> javaUtilDeletings = javaUtilDeletingAnalysis.analyzeDeletingAPIUsage(deletingCG);
			dumpSavingAPIs(Configuration.JAVA_UTIL_REPORT, javaUtilSavings, javaUtilDeletings, serviceEntry);
		}
	}

	/*
	 * Debug entry points for saving and deleting separately
	 */
	private void debugEntries(ArrayList<Entrypoint> deletingSet, ArrayList<Entrypoint> savingSet){
		System.err.println("===> Saving <===");
		for(Entrypoint entry : savingSet)
			System.err.println("	" + entry.getMethod().getSignature().toString());
		System.err.println("===> deleting <===");
		for(Entrypoint entry : deletingSet)
			System.err.println("	" + entry.getMethod().getSignature().toString());
	}


	/*
	 * The CallGraph may break at certain point when a internal class
	 * tries to access a private function within the outer class.
	 * Thus, we use the approach in CHEX paper to add those broken points
	 * as entry points, and rebuild the CallGraph.
	 */
	private ArrayList<Entrypoint> patchCallGraph(CallGraph cg){
		ArrayList<Entrypoint> newEntries = new ArrayList<Entrypoint>();
		for (Iterator<CGNode> it = cg.iterator(); it.hasNext();) {
			CGNode nd = it.next();
			if(nd.getIR()==null)
				continue;
			else{
				IR ir = nd.getIR();
				if(ir.getMethod().getName().toString().contains("access$")){
					if(cg.getSuccNodeCount(nd)==0){
						for(SSAInstruction s : ir.getInstructions()){
							if (s instanceof com.ibm.wala.ssa.SSAAbstractInvokeInstruction) {
								com.ibm.wala.ssa.SSAAbstractInvokeInstruction call = (com.ibm.wala.ssa.SSAAbstractInvokeInstruction) s;								
								IMethod meth = cg.getClassHierarchy().resolveMethod(call.getDeclaredTarget());
								DefaultEntrypoint de = new DefaultEntrypoint(meth,cg.getClassHierarchy());
								newEntries.add(de);
							}
						}
					}
				}				
			}
		}
		return newEntries;
	}

	/*
	 * Debug call graph
	 */
	private void debugCG(CallGraph cg) throws InvalidClassFileException{

		System.out.println("====== DEBUGGING CG ======");
		/*for(CGNode node : cg.getEntrypointNodes()){
			IMethod method = node.getMethod();
			System.out.println("Entry Point Node: " + method.getSignature().toString());
			if(method.getDeclaringClass().getName().toString().contains("PackageMonitor")){
				IClass cls = method.getDeclaringClass();
				for(IMethod m : cls.getAllMethods())
						System.out.println("	===> " + m.getSignature().toString());
			}
		}*/

		for (Iterator<CGNode> it = cg.iterator(); it.hasNext();) {
			CGNode nd = it.next();
			if(nd.getIR()==null)
				continue;
			else{
				IR ir = nd.getIR();
				if(ir.toString().contains("readStorageListLocked")){
					System.out.println(ir);
					System.out.println("$$$$$$$$$$$$$$$$$");	

					Iterator<CGNode> nodeIterator = cg.getSuccNodes(nd);
					while(nodeIterator.hasNext()){
						CGNode next = nodeIterator.next();
						System.out.println("succ node: " + next.getMethod().getName());	
						if(next.getMethod().getName().toString().contains("access$700"))
							System.out.println(next.getIR());
						//if(next.getIR()!=null)System.out.println(next.getIR().toString());	
					}

				}

			}
		}
		System.out.println("====== DONE ======\n");

	}

	public void dumpSavingAPIs(String filename, HashMap<Statement, String> savings, 
			HashMap<Statement, String> deletings, Entry<String,String> mService){
		long start = System.currentTimeMillis();
		Element finishedService = doc.createElement("service");
		image.appendChild(finishedService);

		// set ibinder attribute 
		Attr attr1 = doc.createAttribute("ibinder");
		attr1.setValue(mService.getKey());
		finishedService.setAttributeNode(attr1);
		// set name attribute 
		Attr attr2 = doc.createAttribute("name");
		attr2.setValue(mService.getValue());
		finishedService.setAttributeNode(attr2);
		// set size attribute 
		Attr attr3 = doc.createAttribute("size");
		if(savings==null || savings.isEmpty())
			attr3.setValue("N/A");
		else {
			for (Entry<Statement, String> entry : savings.entrySet()){
				Statement stm = entry.getKey();
				IClass serviceClass = stm.getNode().getMethod().getDeclaringClass();
				attr3.setValue(Integer.toString(serviceClass.getDeclaredInstanceFields().size() +
						serviceClass.getDeclaredMethods().size() + 
						serviceClass.getDeclaredStaticFields().size()));
			}
		}
		finishedService.setAttributeNode(attr3);
		// set finished attribute 
		Attr attr4 = doc.createAttribute("finished");
		attr4.setValue("true");
		finishedService.setAttributeNode(attr4);
		// set type attribute 
		Attr attr5 = doc.createAttribute("type");
		if(Test.appServices.containsKey(mService.getKey()))
			attr5.setValue("appservice");
		else attr5.setValue("frameworkservice");
		finishedService.setAttributeNode(attr5);
		// set category attribute
		Attr attr6 = doc.createAttribute("category");
		attr6.setValue(filename);
		finishedService.setAttributeNode(attr6);

		for (Entry<Statement, String> entry : savings.entrySet()){
			if(entry.getValue().equals("NA"))continue;
			Element residue = doc.createElement("residue");
			finishedService.appendChild(residue);

			Attr a1 = doc.createAttribute("name");
			a1.setValue(entry.getValue());
			residue.setAttributeNode(a1);

			Attr a2 = doc.createAttribute("savingInstructions");
			a2.setValue(entry.getKey().toString());
			residue.setAttributeNode(a2);

			//TODO: change the logic below to remove unnecessary residue instances.
			if(deletings.containsValue(entry.getValue())){
				for (Entry<Statement, String> deletingEntry : deletings.entrySet()){
					if(deletingEntry.getValue().equals(entry.getValue())){
						Attr a3 = doc.createAttribute("deletingInstructions");
						a3.setValue(deletingEntry.getKey().toString());
						residue.setAttributeNode(a3);

						Attr a4 = doc.createAttribute("complexity");
						IR deletingIR = deletingEntry.getKey().getNode().getIR();
						SSAInstruction[] instructions = deletingIR.getInstructions();
						int startingComplexity = 1;
						if(instructions!=null){
							for(SSAInstruction ins : instructions){
								if(ins instanceof SSAComparisonInstruction ||
										ins instanceof SSAConditionalBranchInstruction ||
										ins instanceof SSAGetCaughtExceptionInstruction ||
										ins instanceof SSAGotoInstruction ||
										ins instanceof SSAReturnInstruction ||
										ins instanceof SSASwitchInstruction ||
										ins instanceof SSABinaryOpInstruction ||
										ins instanceof SSAAbstractThrowInstruction){
									startingComplexity++;
								}
							}
						}
						//TODO: measure the complexity of this deleting function
						// https://en.wikipedia.org/wiki/Cyclomatic_complexity
						a4.setValue(Integer.toString(startingComplexity));
						residue.setAttributeNode(a4);
					}
				}

			} else {
				Attr a3 = doc.createAttribute("deletingInstructions");
				a3.setValue("N/A");
				residue.setAttributeNode(a3);

				Attr a4 = doc.createAttribute("complexity");
				//TODO: set a high value for instances that do not have deleting logic
				// More likely, they are real data residue instances.
				a4.setValue("1000");
				residue.setAttributeNode(a4);
			}
		}
		long end = System.currentTimeMillis();
		Attr attr7 = doc.createAttribute("output_time_cost");
		attr7.setValue(Long.toString(end-start));
		finishedService.setAttributeNode(attr7);
	}

	/*
	 * Dump the details of all saving APIs for the current examining system service.
	 */	
	/*public void dumpSavingAPIs(String filename, HashMap<Statement, String> savings, 
			HashMap<Statement, String> deletings, Entry<String,String> mService){
		try
		{
			HashSet<String> residue = new HashSet<String>();

			FileWriter fw = new FileWriter(filename,true); 

			fw.write("Service: " + mService.getValue() + "\n");
			fw.write("IBinder: " + mService.getKey() + "\n");

			fw.write("===> Savings <===" + "\n");
			for (Entry<Statement, String> entry : savings.entrySet()){
				if(!entry.getValue().contains("$"))
					residue.add(entry.getValue());
				fw.write("	" + entry.getKey() +  "\n");
				fw.write("	" + entry.getValue() +  "\n");
			}
			fw.write("===> Deletings <===" + "\n");
			for (Entry<Statement, String> entry : deletings.entrySet()){
				residue.remove(entry.getValue());
				fw.write("	" + entry.getKey() +  "\n");
				fw.write("	" + entry.getValue() +  "\n");
			}

			if(residue.size()>0){
				fw.write("!!! Need Examination: " + residue.toString() + "\n");
				num = num + residue.size();
				fw.write("Total (up to here): " + num + "\n");
			}
			fw.write("\n");
			fw.close();
		} catch(IOException ioe){
			System.err.println("IOException: " + ioe.getMessage());
		}
	}*/

}
