package com.residue.analysis;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map.Entry;

import com.ibm.wala.classLoader.IClass;
import com.ibm.wala.classLoader.IMethod;
import com.ibm.wala.ipa.callgraph.AnalysisScope;
import com.ibm.wala.ipa.callgraph.Entrypoint;
import com.ibm.wala.ipa.callgraph.impl.DefaultEntrypoint;
import com.ibm.wala.ipa.cha.IClassHierarchy;
import com.residue.driver.Configuration;

public class LifeCycleEntryPoints {

	/*
	 * Get a list of entry points for single system service.
	 * Approach:
	 * 		add all public AIDL functions and service lifecycle events as entry points;
	 */
	public ArrayList<Entrypoint> getLifeCycleEntryPointsForService(Entry<String,String> serviceEntry, 
			IClassHierarchy cha, AnalysisScope scope) throws IOException{
		final ArrayList<Entrypoint> entrypoints = new ArrayList<Entrypoint>();

		// try to find all related classes from the hierarchy
		IClass binderClass = null;
		IClass serviceClass = null;
		
		if(Configuration.DEBUG_SAVING){
			System.out.println("entry key class: " + serviceEntry.getKey().toString());
			System.out.println("entry value class: " + serviceEntry.getValue().toString() + "\n");
		}
			
		Iterator<IClass> iterator = cha.iterator();
		
		iterator = cha.iterator();
		while(iterator.hasNext()){
			IClass c = iterator.next();
			String cname = c.getName().toString();			
			if(cname.equals(serviceEntry.getKey())) {			
				binderClass = c;
				if(Configuration.DEBUG_SAVING){
					System.out.println("binder class: " + binderClass.getName().toString());
					System.out.println("	super class: " + binderClass.getSuperclass().getName().toString());
				}
				
			} 
			if (cname.equals(serviceEntry.getValue())){
				
				serviceClass = c;
				if(Configuration.DEBUG_SAVING){
					System.out.println("service class: " + serviceClass.getName().toString());
					System.out.println("	super class: " + serviceClass.getSuperclass().getName().toString());
				}
				
			}
			// both classes inside the service entry have been detected, exit.
			if(binderClass!=null && serviceClass!=null) 
				break;		
		}

		// add entry points from the binder class
		if(binderClass!=null){
			if(binderClass.getSuperclass().getName().toString().equals("Lcom/android/server/SystemService")){
				for (IMethod m : binderClass.getDeclaredMethods()) {				
					if(isLifeCycleEvent(m) || m.getName().toString().contains("init>")){
						DefaultEntrypoint de = new DefaultEntrypoint(m,cha);
						entrypoints.add(de);
					}
				}
			}
		}

		// add entry points from the service class
		if(serviceClass!=null){
			if(serviceClass.getSuperclass().getName().toString().equals("Lcom/android/server/SystemService")){
				for (IMethod m : serviceClass.getDeclaredMethods()) {				
					if(isLifeCycleEvent(m) || m.getName().toString().contains("init>")){
						DefaultEntrypoint de = new DefaultEntrypoint(m,cha);
						entrypoints.add(de);
					}
				}
			}
		}

		return entrypoints;
	}

	/*
	 * Check whether the given method is part of system service's life cycle.
	 */
	private boolean isLifeCycleEvent(IMethod m){
		if(!m.getSignature().contains("android") || !m.isPublic())return false;
		for(String callback : Configuration.SystemServiceLifeCycleEvents){
			if(m.getSignature().toString().contains(callback)){
				return true;
			}
		}
		return false;
	}
	
	
}
