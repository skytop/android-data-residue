package com.residue.utility;

import java.io.FileWriter;
import java.io.IOException;

// Nothing here, place holder
public class Utilities {
	/*
	public static void record(String str){
		try
		{
			FileWriter fw = new FileWriter("progress.txt",true); 	
			fw.write(str + ": " + System.currentTimeMillis());
			fw.close();
		} catch(IOException ioe){
			System.err.println("IOException: " + ioe.getMessage());
		}
	}
	*/
}
/*
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import com.ibm.wala.classLoader.IClass;
import com.ibm.wala.classLoader.IClassLoader;
import com.ibm.wala.classLoader.IMethod;
import com.ibm.wala.ipa.callgraph.AnalysisScope;
import com.ibm.wala.ipa.callgraph.CGNode;
import com.ibm.wala.ipa.callgraph.CallGraph;
import com.ibm.wala.ipa.cha.IClassHierarchy;
import com.ibm.wala.ipa.slicer.NormalStatement;
import com.ibm.wala.ipa.slicer.Statement;
import com.ibm.wala.ssa.IR;
import com.ibm.wala.types.TypeName;
import com.ibm.wala.util.debug.Assertions;
import com.residue.driver.Configuration;

public class Utilities {

	
	 * <VISITED> traverse call graph to find interesting node 
	 
	public CGNode traverseNode(CallGraph cg,String name){
		for (Iterator<? extends CGNode> it = cg.getSuccNodes(cg.getFakeRootNode()); it.hasNext();) {
			CGNode n = it.next();
			if (n.getMethod().getName().toString().equals(name) ) {
				return n;
			}
		}
		// if it's not a successor of fake root, just iterate over everything
		for (CGNode n : cg) {
			//System.out.println(n.getMethod().getName().toString());
			if (n.getMethod().getName().toString().equals(name) ) {
				System.out.println(n.getIR().toString());
				return n;
			}
		}
		Assertions.UNREACHABLE("failed to find method " + name);
		return null;
	}

	 <VISITED>
	 * get implicit intent setAction parameter   
	 * TODO: not path sensitive 
	 
	private String getIntentAction(CGNode n){
		String[] split_IR = n.getIR().toString().split("\n");
		for(String str:split_IR){
			if(str.contains("setAction")){
				int actionIndex = str.indexOf('#')+1; 
				String actStr ="";
				while(str.charAt(actionIndex) != ' '){
					actStr += str.charAt(actionIndex);
					actionIndex ++; 
				}
				System.err.println(actStr);
				return actStr;
			}
		}
		return null;
	}

	
	 * <VISITED> Filter in interesting CGNode by look at whether 
	 * node contain sliced_seed
	 
	private boolean findStrFromNode(CGNode node, String[] arr) {
		if( node.getIR() == null) return false;
		String ir = node.getIR().toString();
		for(String str : arr){
			if(ir.contains(str)){
				return true;
			}
		}
		return false;
	}

	
	 * <VISITED> Find interesting seeds
	 
	public ArrayList<Statement> findStmt(CGNode n)
	{
		ArrayList<Statement> s = new ArrayList<Statement>();
		IR ir = n.getIR();

		// maybe use ir.getInstructions() instead?
		String[] splited_ir = ir.toString().split("\n");
		for( String key:Configuration.SETTINGS_SAVING_API)
		{
			int temp_ir = getIR(splited_ir, key);

			if(temp_ir!=-1)
			{
				//TODO: temporary solve the problem by try catch. need to correct the line number
				//System.err.println("ying: "+splited_ir[temp_ir].split(" ")[0]);
				int pc = 0; 
				try{
					pc = Integer.parseInt(splited_ir[temp_ir].split(" ")[0]);

					//XIAO: Move following two lines of code into try block
					NormalStatement ns = new NormalStatement(n,pc);
					s.add(ns);
				} catch (NumberFormatException e){
					System.err.println(e.toString());
				}
			}
		}
		return s;
	}

	// <VISITED> Return the first occurrence of key in ir[]
	public int getIR(String[] ir, String key)
	{
		int i =0; 
		for (String s: ir)
		{
			if(s.contains(key)){
				return i;
			}
			i += 1;
		}
		return -1;
	}
	
	
	 * <VISITED> Traverse all classes
	 
	private void traverseClass(IClassLoader loader, AnalysisScope scope) {
		Iterator<IClass> classIt = loader.iterateAllClasses();
		while (classIt.hasNext()) {
			IClass clazz = classIt.next();
			if (clazz.isInterface()) {
				continue;
			}
			TypeName clname = clazz.getName();				
			for (IMethod method : clazz.getAllMethods()) {
				String desc = method.getSignature();
				IClass declClass = method.getDeclaringClass();
				if (method.isAbstract() || method.isSynthetic()
						|| (declClass.isAbstract() && method.isInit())
						|| (declClass.isAbstract() && !method.isStatic())) {
					continue;
				}
				if (method.getDeclaringClass().getClassLoader().getReference()
						.equals(scope.getApplicationLoader())) {
					String junitDesc = method.getSignature();
					System.out.println(junitDesc);
				}
			}
		}
	}

	

	
	 * <VISITED> Traverse ClassHierarchy
	 
	public void traverseCHA(IClassHierarchy cha, String method){
		if(Configuration.DEBUG)
			System.out.println("====== DEBUGGING CHA ======");
		for (IClass c : cha) {
			String cname = c.getName().toString();
			if(cname.contains("DreamManagerService"))System.out.println("Class: "+cname+"   ,Super Class: " + c.getSuperclass());
			for (IMethod m : c.getAllMethods()) {
				String mname = m.getName().toString();
				if(mname.contains(method)){
					System.out.println("Class: "+cname);
					System.out.println("	--- Method:" + m.getSignature());
				}
			}
		}
		if(Configuration.DEBUG)
			System.out.println("====== DONE ======\n");
	}

	
	 * <VISITED> Get main entry of Call Graph
	 
	public void getMainEntry(CallGraph cg){
		Collection<CGNode> entrys = cg.getEntrypointNodes();
		for(CGNode n : entrys){
			System.out.println("Entry point: "+n.getMethod().getSignature().toString());
		}
	}


	
	 * <VISITED> print statement information
	 
	private void printStatement(Statement s, CallGraph cg) 
	{
		if(s.getKind() == Statement.Kind.NORMAL)
		{
			int insindex = ((NormalStatement) s).getInstructionIndex();
			int byindex = s.getNode().getMethod().getLineNumber(insindex);

			 source info 
			try{
				System.out.println("line number "+byindex +" ir index "+insindex+", statement: "+s.toString());
			}catch(Exception e){
				System.err.println(e.toString());
			}
		}
		else
		{
			System.out.println(s.toString());
		}
	}


	
	 * <VISITED> Add entry points from interface class 
	 
	public void addEntrypointFromCHA(IClassHierarchy cha, ArrayList<AIDLEntryPoints> EntrypointList){
		if(Configuration.DEBUG)
			System.out.println("====== ADDING AIDL CLASS ======");
		for (IClass c : cha) {
			String cname = c.getName().toString();

			for (String aidlName : AIDLEntryPoints.AIDLNameList){

				if(cname.equals(aidlName)){
					if(Configuration.DEBUG)
						System.out.println("Class: "+cname+"   ,Super Class: " + c.getSuperclass());
					// Add all methods from the AIDL file
					for (IMethod m : c.getAllMethods()) {
						String mname = m.getName().toString();
						//TEST
						if(!m.getSignature().startsWith("java.") && m.isPublic()){
							AIDLEntryPoints aep = new AIDLEntryPoints(mname,cname,m.getSignature());
							EntrypointList.add(aep);
						}
					}
				}
			}
		}
		if(Configuration.DEBUG)
			System.out.println("====== DONE ======\n");
	}


	
	 * <VISITED> Traverse CallGraph
	 
	private void traverseCG(CallGraph cg, String method){
		for(CGNode n : cg){ 
			if(n.getIR() !=null){
				if(n.getIR().toString().contains(method))
					System.out.println(n.getIR().toString());
			}
		}
	}


	//XIAO: Get a list of entry points from application Manifest.
	//Skipped at this stage.
	
	AndroidEntryPoints aep = new AndroidEntryPoints();		
	File fXmlFile = new File(manifestPath);
    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
    Document doc = dBuilder.parse(fXmlFile);
    ArrayList<Document> doclist = new ArrayList<Document>();
    for(String xmlpath: xmlpaths) {
    	File AxmlFile = new File(xmlpath);
    	Document Adoc = dBuilder.parse(AxmlFile);
    	doclist.add(Adoc);
    }
	 

	
	CallGraphBuilder builder = Util.makeZeroContainerCFABuilder(options, new AnalysisCache(), cha, scope);	 
	CallGraphBuilder builder = Util.makeZeroOneCFABuilder(options, new AnalysisCache(), cha, scope);
	CallGraphBuilder builder = Util.makeVanillaZeroOneCFABuilder(options, new AnalysisCache(), cha, scope);
	 
}
*/