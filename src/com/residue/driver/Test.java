package com.residue.driver;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import com.ibm.wala.classLoader.JarFileModule;
import com.ibm.wala.classLoader.Module;
import com.ibm.wala.core.tests.callGraph.CallGraphTestUtil;
import com.ibm.wala.ipa.callgraph.AnalysisScope;
import com.ibm.wala.ipa.cha.ClassHierarchy;
import com.ibm.wala.ipa.cha.ClassHierarchyException;
import com.ibm.wala.ipa.cha.IClassHierarchy;
import com.ibm.wala.shrikeCT.InvalidClassFileException;
import com.ibm.wala.types.ClassLoaderReference;
import com.ibm.wala.util.CancelException;
import com.residue.analysis.Analysis;
import com.residue.analysis.SystemServiceDetection;
import com.residue.api.APIPairHelper;
import com.residue.rewriting.Rewriter;
import com.residue.utility.Utilities;

public class Test {	

	// Comment out for jar generation
	// private static String jarpath = "./dat/data_residue/input.txt"; 

	private static String jarpath = "./input.txt"; 
	private static String frameworkJarPath = "./frameworkInput.txt"; 
	private static AnalysisScope scope;
	private static IClassHierarchy cha;
	public static HashMap<String, String> SSClasses;
	public static long startServiceDetection;
	public static long endServiceDetection;
	public static long startRewriting;
	public static long endRewriting;
	public static long startResidueDetection;
	public static long endResidueDetection;
	public static HashMap<String, String> appServices;

	public static void main(String args[]) throws IOException, ClassHierarchyException, IllegalArgumentException, CancelException, ParserConfigurationException, SAXException, InvalidClassFileException, InterruptedException, TransformerException{

		final Test t = new Test();
		//Utilities.record("Collect System Services");
		scope = CallGraphTestUtil.makeJ2SEAnalysisScope(frameworkJarPath, CallGraphTestUtil.REGRESSION_EXCLUSIONS);
		cha = ClassHierarchy.make(scope);

		// Get a list of system service classes from the examined image
		startServiceDetection = System.currentTimeMillis();
		SystemServiceDetection serviceDetection = new SystemServiceDetection();
		SSClasses = serviceDetection.getSystemServiceClassesAccurate(cha,scope);
		endServiceDetection = System.currentTimeMillis();

		scope = CallGraphTestUtil.makeJ2SEAnalysisScope(jarpath, CallGraphTestUtil.REGRESSION_EXCLUSIONS);
		cha = ClassHierarchy.make(scope);

		appServices = t.readAppServices();
		SSClasses.putAll(appServices);

		//System.err.println(SSClasses.toString());

		startRewriting = System.currentTimeMillis();
		// pre-process the JAR to connect broken links
		if(Configuration.PREPROCESS){	
			//Utilities.record("Rewrite JARs");
			ClassLoaderReference reference = scope.getApplicationLoader();		
			// A list of jar files from the input file
			List<Module> modules = scope.getModules(reference);
			for(final Module module : modules){
				// We start a new thread for handling each JAR file. 
				// The reason behind is that, bytecode rewriting sometimes will
				// throw exception, and stop the program execution. By moving
				// the excution to a new thread, our main thread will not be affected.
				long startEachJar = System.currentTimeMillis();
				final Status threadState = new Status(0);
				Thread t1 = new Thread(new Runnable() {
					public void run()
					{
						JarFileModule jar = (JarFileModule)module;
						String path = jar.getAbsolutePath();	
						System.err.println("rewrite jar path: " + jar.getAbsolutePath());
						Rewriter rewriter = new Rewriter(cha, path);
						try {
							rewriter.rewrite();
							threadState.setStatus(1);
						} catch (IllegalStateException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							threadState.setStatus(2);
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							threadState.setStatus(2);
						} catch (InvalidClassFileException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							threadState.setStatus(2);
						} finally {
							threadState.setStatus(2);
						}
					}});  
				t1.start();	
				// Make sure t1 is interrupted (finished or crashed), then we proceed to the next JAR.
				while(true){
					long endEachJar = System.currentTimeMillis();
					if(threadState.getStatus() !=0 || (startEachJar-endEachJar)>Configuration.timeout){
						break;
					} 
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			//Utilities.record("DONE");
		} 	
		endRewriting = System.currentTimeMillis();

		// start analysis
		if(Configuration.DETECTION){
			//Utilities.record("Detect Data Residue");
			String manifestPath = "";
			ArrayList<String> xmlpaths = new ArrayList<String>();
			t.startBuild(manifestPath, xmlpaths);
			//Utilities.record("DONE");
		}		
	}

	/*
	 * start the analysis on system services.
	 */
	public void startBuild(String manifestPath, ArrayList<String> xmlpaths) 
			throws IOException, ClassHierarchyException, IllegalArgumentException, 
			CancelException, ParserConfigurationException, SAXException, 
			InvalidClassFileException, TransformerException{	

		// Get a list of saving/deleting API pairs
		if(Configuration.BUILD_API_PAIR){
			APIPairHelper apiHelper = new APIPairHelper(cha);
			apiHelper.detectAPIPairs();
		}

		// Save results to XML
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
		// root elements
		final Document doc = docBuilder.newDocument();
		final Element rootElement = doc.createElement("image");
		doc.appendChild(rootElement);
		// add dummy image info
		addImageInfo(doc, rootElement);

		//TODO: we need to set a time out for each system service. 2 minutes maybe?
		startResidueDetection = System.currentTimeMillis();
		final Analysis mDataResidueAnalysis = new Analysis(doc, rootElement);
		int serviceIndex = 0;
		int totalServices = SSClasses.entrySet().size();
		// Analyze each individual service for data residue
		for(final Entry<String,String> service : SSClasses.entrySet()){
			// We start a new thread for handling each system service. 
			// The reason behind is that, system service handling sometimes
			// takes too long. We use a thread for timeout purpose.
			// Right now, the timeout benchmark is 60s (1 minutes).
			System.out.println("!!! Analyze Service " + service.getKey() + " !!! (" + (serviceIndex++) + "/" + totalServices + ")");
			long startEachService = System.currentTimeMillis();
			final Status threadState = new Status(0);
			Thread t1 = new Thread(new Runnable() {
				public void run()
				{
					//if(service.getKey().toString().equals(Configuration.DEBUGGING_SERVICE)){
					//System.out.println("entry value class: " + service.getValue().toString());
					try{
						//Utilities.record("---Analyze Service " + service.getValue());
						mDataResidueAnalysis.analyzeService(service, cha, scope);
						System.out.println("===== DONE ====");
						threadState.setStatus(1);
						//Utilities.record("---DONE");
					} catch(Exception e){
						//Utilities.record("---DONE");
						threadState.setStatus(2);
						e.printStackTrace();
						System.out.println("===== NEED MORE WORK =====");
					} 
					//}
				}
			});
			t1.start();
			// Make sure t1 is finished or crashed or executed for more than timeout, 
			// then we proceed to the next service.
			while(true){
				long endEachService = System.currentTimeMillis();
				if(threadState.getStatus() == 2 ||
						(threadState.getStatus() == 0 && (endEachService-startEachService)>Configuration.timeout)){
					addUnfinishedService(doc, rootElement, service);
					break;
				} else if(threadState.getStatus() == 1){
					break;
				}
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}	
		}
		endResidueDetection = System.currentTimeMillis();

		// add time cost in result
		addTimeCost(doc, rootElement);

		// write the content into xml file
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(doc);
		StreamResult result = new StreamResult(new File("./result.xml"));

		// Output to console for testing
		// StreamResult result = new StreamResult(System.out);

		transformer.transform(source, result);
		// Now we have the results in hand. In case of unfinished thread running in background, we close the program.
		System.exit(0);
	}

	// append dummy image metadata information
	private void addImageInfo(Document doc, Element image){
		// set name attribute 
		Attr attr1 = doc.createAttribute("name");
		attr1.setValue("xyz");
		image.setAttributeNode(attr1);
		// set version attribute 
		Attr attr2 = doc.createAttribute("version");
		attr2.setValue("xyz");
		image.setAttributeNode(attr2);
		// set region attribute 
		Attr attr3 = doc.createAttribute("region");
		attr3.setValue("xyz");
		image.setAttributeNode(attr3);
		// set carrier attribute 
		Attr attr4 = doc.createAttribute("carrier");
		attr4.setValue("xyz");
		image.setAttributeNode(attr4);
		// set model attribute 
		Attr attr5 = doc.createAttribute("model");
		attr5.setValue("xyz");
		image.setAttributeNode(attr5);
		// set manufacturer attribute 
		Attr attr6 = doc.createAttribute("manufacturer");
		attr6.setValue("xyz");
		image.setAttributeNode(attr6);
	}

	// append time cost in each phase: service detection, jar rewriting and residue detection.
	private void addTimeCost(Document doc, Element image){
		long duration = endServiceDetection - startServiceDetection;
		Element serviceDetection = doc.createElement("serviceDetectionCost");
		serviceDetection.appendChild(doc.createTextNode(Long.toString(duration)));
		image.appendChild(serviceDetection);

		duration = endRewriting - startRewriting;
		Element rewriting = doc.createElement("rewritingCost");
		rewriting.appendChild(doc.createTextNode(Long.toString(duration)));
		image.appendChild(rewriting);

		duration = endResidueDetection - startResidueDetection;
		Element residueDetection = doc.createElement("residueDetectionCost");
		residueDetection.appendChild(doc.createTextNode(Long.toString(duration)));
		image.appendChild(residueDetection);
	}

	// The service is not finished within the specified timeout period.
	// We add the corresponding information
	private void addUnfinishedService(Document doc, Element image, 
			Entry<String,String> service){
		System.err.println("adding unfinished service: " 
			+ service.getKey() + " : " + service.getValue());
		for(int i = 0; i<7; i++){
			Element unfinishedService = doc.createElement("service");
			image.appendChild(unfinishedService);

			// set ibinder attribute 
			Attr attr1 = doc.createAttribute("ibinder");
			attr1.setValue(service.getKey());
			unfinishedService.setAttributeNode(attr1);
			// set name attribute 
			Attr attr2 = doc.createAttribute("name");
			attr2.setValue(service.getValue());
			unfinishedService.setAttributeNode(attr2);
			// set LOC attribute 
			Attr attr3 = doc.createAttribute("size");
			attr3.setValue("N/A");
			unfinishedService.setAttributeNode(attr3);
			// set finished attribute 
			Attr attr4 = doc.createAttribute("finished");
			attr4.setValue("false");
			unfinishedService.setAttributeNode(attr4);
			// set type attribute 
			Attr attr5 = doc.createAttribute("type");
			if(appServices.containsKey(service.getKey()))
				attr5.setValue("appservice");
			else attr5.setValue("frameworkservice");
			unfinishedService.setAttributeNode(attr5);
			// set category attribute
			Attr attr6 = doc.createAttribute("category");
			if(i==0)
				attr6.setValue(Configuration.CONTENT_PROVIDER_REPORT);
			else if(i==1)
				attr6.setValue(Configuration.DATABASE_REPORT);
			else if(i==2)
				attr6.setValue(Configuration.FILE_REPORT);
			else if(i==3)
				attr6.setValue(Configuration.XML_REPORT);
			else if(i==4)
				attr6.setValue(Configuration.JAVA_UTIL_REPORT);
			else if(i==5)
				attr6.setValue(Configuration.SHARED_PREFERENCE_REPORT);
			else if(i==6)
				attr6.setValue(Configuration.SETTINGS_REPORT);
			unfinishedService.setAttributeNode(attr6);
		}
	}

	/*
	 * read system app services list.
	 */	
	private HashMap<String, String> readAppServices() throws FileNotFoundException, IOException{
		HashMap<String, String> services = new HashMap<String, String>();
		try (BufferedReader br = new BufferedReader(new FileReader(Configuration.APP_SERVICE_FILE))) {
			String line;
			while ((line = br.readLine()) != null) {
				// process the line.
				services.put(line, line);
			}
		}
		return services;
	}

}
