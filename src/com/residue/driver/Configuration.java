package com.residue.driver;

public class Configuration {

	public static boolean DEBUG = false;

	public static boolean DEBUG_SERVICE = false;

	public static boolean DEBUG_SAVING = false;

	public static boolean BUILD_API_PAIR = false;

	public static boolean DEBUG_HANDLER_REWRITE = false;
	public static boolean DEBUG_THREAD_REWRITE = false;
	public static boolean DEBUG_ASYNC_REWRITE = false;
	public static boolean DEBUG_SERVICE_REWRITE = false;

	public static boolean ENABLE_GENERIC_REWRITER = false;

	// need pre-processing?
	public static boolean PREPROCESS = true;

	// start detection?
	public static boolean DETECTION = true;

	// analyze settings residue
	public static boolean SETTINGS_ENABLED = true;

	// analyze database residue
	public static boolean DATABASE_ENABLED = true;

	// analyze file residue
	public static boolean FILE_ENABLED = true;

	// analyze java util residue
	public static boolean JAVA_UTIL_ENABLED = true;

	// analyze xml residue
	public static boolean XML_ENABLED = true;

	// analyze shared preference residue
	public static boolean SHARED_PREFERENCE_ENABLED = true;

	// analyze database residue
	public static boolean CONTENT_PROVIDER_ENABLED = true;

	// time out for each service analysis
	public static long timeout = 1000 * 300;

	public static String SYSTEM_SERVICES = "./systemservices_debug.txt";
	public static String ENTRY_POINT = "./";
	public static String SETTINGS_REPORT = "settings";
	public static String DATABASE_REPORT = "database";
	public static String SHARED_PREFERENCE_REPORT = "shared_preference";
	public static String XML_REPORT = "xml";
	public static String JAVA_UTIL_REPORT = "java_util";
	public static String FILE_REPORT = "file";
	public static String CONTENT_PROVIDER_REPORT = "content_provider";
	/*public static String SETTINGS_REPORT = "./settings_report.txt";
	public static String DATABASE_REPORT = "./database_report.txt";
	public static String SHARED_PREFERENCE_REPORT = "./shared_preference_report.txt";
	public static String XML_REPORT = "./xml_report.txt";
	public static String JAVA_UTIL_REPORT = "./java_util_report.txt";
	public static String FILE_REPORT = "./file_report.txt";
	public static String CONTENT_PROVIDER_REPORT = "./content_provider_report.txt";*/

	public static String APP_SERVICE_FILE = "./appservices.txt";

	//TODO: Call graph contains handler breaking links
	public static String DEBUGGING_SERVICE ="Lcom/android/server/appwidget/AppWidgetServiceImpl";

	public static final String[] SystemServiceLifeCycleEvents ={
		"onBootPhase(I)V",
		"onCleanupUser(I)V",
		"onStart()V",
		"onStartUser(I)V",
		"onStopUser(I)V",
		"onSwitchUser(I)V",
		"systemRunning(",
		"systemReady()V"
	};

	public static final String PackageMonitorClass = "Lcom/android/internal/content/PackageMonitor";	
	public static final String[] PackageMonitorAPIs ={
		"onSomePackagesChanged()V",
		"onBeginPackageChanges()V",
		"onFinishPackageChanges()V",
		"onPackageChanged(Ljava/lang/String;I[Ljava/lang/String;)Z",
		"onPackageDisappeared(Ljava/lang/String;I)V",
		"onPackageRemoved(Ljava/lang/String;I)V",
		"onPackageRemovedAllUsers(Ljava/lang/String;I)V",
		"onReceive(Landroid/content/Context;Landroid/content/Intent;)V",
		"onUidRemoved(I)V",
		"onHandleForceStop(Landroid/content/Intent;[Ljava/lang/String;IZ)Z"
	};

	public static final String BinderDeathClass = "Landroid/os/IBinder$DeathRecipient";	
	public static final String BinderDeathAPIs = "binderDied()V";

	public static final String BroadcastReceiverClass = "Landroid/content/BroadcastReceiver";	
	public static final String BroadcastReceiverAPIs = "onReceive(Landroid/content/Context;Landroid/content/Intent;)V";

	public static final String RegisteredServicesCacheClass = "Landroid/content/pm/RegisteredServicesCacheListener";	
	public static final String RegisteredServicesCacheAPIs = "onServiceChanged(Ljava/lang/Object;IZ)V";

}
