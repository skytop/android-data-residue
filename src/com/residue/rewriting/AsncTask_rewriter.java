package com.residue.rewriting;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.NoSuchElementException;

import org.apache.commons.io.FilenameUtils;

import com.ibm.wala.classLoader.IBytecodeMethod;
import com.ibm.wala.classLoader.IClass;
import com.ibm.wala.classLoader.IClassLoader;
import com.ibm.wala.classLoader.IMethod;
import com.ibm.wala.ipa.cha.IClassHierarchy;
import com.ibm.wala.shrikeBT.ConstantInstruction;
import com.ibm.wala.shrikeBT.Constants;
import com.ibm.wala.shrikeBT.DupInstruction;
import com.ibm.wala.shrikeBT.GetInstruction;
import com.ibm.wala.shrikeBT.IInstruction;
import com.ibm.wala.shrikeBT.Instruction;
import com.ibm.wala.shrikeBT.InvokeInstruction;
import com.ibm.wala.shrikeBT.LoadInstruction;
import com.ibm.wala.shrikeBT.MethodData;
import com.ibm.wala.shrikeBT.MethodEditor;
import com.ibm.wala.shrikeBT.NewInstruction;
import com.ibm.wala.shrikeBT.PopInstruction;
import com.ibm.wala.shrikeBT.StoreInstruction;
import com.ibm.wala.shrikeBT.shrikeCT.ClassInstrumenter;
import com.ibm.wala.shrikeBT.shrikeCT.OfflineInstrumenter;
import com.ibm.wala.shrikeCT.ClassReader;
import com.ibm.wala.shrikeCT.ClassWriter;
import com.ibm.wala.shrikeCT.InvalidClassFileException;
import com.ibm.wala.types.ClassLoaderReference;
import com.ibm.wala.types.TypeReference;
import com.residue.driver.Configuration;


/*
 * Utility class for rewriting the ansycTask logic in system jar files.
 * It is based on the WALA shrike functionality, and reconnects
 * the missing link between ansycTask start and ansycTask run.
 */
public class AsncTask_rewriter {

	int total = 0;
	int work = 0;
	private OfflineInstrumenter instrumenter;
	private IClassHierarchy cha;
	private String JAR_PATH;
	private File input, output;
	public static HashMap<String,String> asyncList = new HashMap<String,String>();

	public AsncTask_rewriter(IClassHierarchy arg, String path){
		JAR_PATH = path;
		cha = arg;
	}

	/*
	 *  Prepare for the bytecode instrumentation
	 */
	private void prepare() throws IOException{
		instrumenter = new OfflineInstrumenter(false);
		//instrumenter.addInputClass(new File("./dat/data_residue/"), new File("./dat/data_residue/AlarmManagerService.class"));
		input = new File(JAR_PATH);
		instrumenter.addInputJar(input);
		instrumenter.setPassUnmodifiedClasses(true);
		instrumenter.beginTraversal();
		int pos = JAR_PATH.lastIndexOf(".");
		String name = JAR_PATH.substring(0, pos);
		output = new File(name + "_fixed.jar");
		if(output.exists())output.delete();
		instrumenter.setOutputJar(output);
	}


	/*
	 *  Rewrite each class in the jar. Connect from the execute() API
	 *  to the doInBackground() API and other APIs. Finally, generate the new jar.
	 */
	public void rewrite() throws IllegalStateException, IOException, InvalidClassFileException{
		prepare();
		ClassInstrumenter ci;
		while ((ci = instrumenter.nextClass()) != null) {
			final String className = ci.getReader().getName();

			// We skip the android/os/AsyncTask base class.
			if(className.contains("Landroid/os/AsyncTask"))
				continue;

			if(Configuration.DEBUG_ASYNC_REWRITE)
				System.out.println("Class: " + className + "\n");
			for (int m = 0; m < ci.getReader().getMethodCount(); m++) {
				MethodData d = ci.visitMethod(m);
				if (d != null ) {
					MethodEditor me = new MethodEditor(d);	
					ArrayList<Integer> list = checkBrokenLinks(me);
					me.beginPass();
					if(list.size()>0){
						if(Configuration.DEBUG_ASYNC_REWRITE)
							System.out.println("method: " + ci.getReader().getMethodName(m).toString() 
								+ " in class: " + className + "\n");
						for(Integer index : list){
							fixMethod(me, (int)index); // fix the target function
						}
					}else {
						retainMethod(me); // leave other functions unchanged
					}
					me.applyPatches();
					me.endPass();
				}

			}

			ClassWriter cw = ci.emitClass();
			instrumenter.outputModifiedClass(ci, cw);	
		}
		System.out.println("AsyncTask rewriting stats: " + work + "/" + total);
		instrumenter.close();

		if(output.renameTo(input)){
			System.out.println("Rewrite succesful");
		}else{
			System.out.println("Rewrite failed");
		}
	}

	/*
	 * Is the current instrumenting method containing AsyncTask.execute() APIs?
	 * If so, we need to byte code rewrite this method.
	 * Otherwise, retain this method.
	 */
	private ArrayList<Integer> checkBrokenLinks(MethodEditor me) throws InvalidClassFileException{

		ArrayList<Integer> list = new ArrayList<Integer>();
		IInstruction[] instr = me.getInstructions();
		for (int i = 0; i < instr.length; i++) {
			if((instr[i].toString().contains("executeOnExecutor,(Ljava/util/concurrent/Executor;")
					|| instr[i].toString().contains("execute,([Ljava/lang/Object;)")) 
					&& instr[i].toString().contains("Landroid/os/AsyncTask;")){
				if(Configuration.DEBUG_ASYNC_REWRITE)
					System.out.println("Instruction " + instr[i]);	
				list.add(new Integer(i));
			}
		}
		return list;
	}


	/*
	 * Connect the call rom the execute() API to the doInBackground() API and other APIs
	 */
	private void fixMethod(MethodEditor me, final int index){
		final IInstruction[] instr = me.getInstructions();
		int i;
		for (i = 0; i < instr.length; i++) {
			if(Configuration.DEBUG_ASYNC_REWRITE)
				System.out.println("Instruction " + instr[i] + "\n");

			if(i == index){
				final String cname = me.getData().getClassType();
				if(Configuration.DEBUG_ASYNC_REWRITE)
					System.out.println("Method <" + me.getData().getName() + 
							"> in Class <" + cname + ">");
				final IInstruction instruct = instr[index];
				me.insertAfter(i,new MethodEditor.Patch() {
					@Override
					public void emitTo(MethodEditor.Output w) {
						String target = resolveAsyncTaskTypefromInstruction(instr, index, cname);
						total++;
						if(target!=null)work++;
						else target = "Landroid/os/AsyncTask;";

						//System.err.println("Instruction: " + instruct + "\n");
						//System.err.println("Target Type: " + target + "\n");

						HashMap<String, String> paras = resolveParaTypefromInstruction(target);

						asyncList.put(target,cname);					
					
						NewInstruction para_object = NewInstruction.make("Ljava/lang/Object;", 0);
						//System.out.println("para_object Instruction " + para_constant + "\n");
						w.emit(para_object);

						// save the Message object
						StoreInstruction store = StoreInstruction.make("Ljava/lang/Object;", 15);
						//System.out.println("store Instruction " + store + "\n");
						w.emit(store);

						// load the Message object as an argument for handleMessage() api
						LoadInstruction load = LoadInstruction.make("Ljava/lang/Object;", 15);
						//System.out.println("load Instruction " + load + "\n");

						// invoke the onPreExecute()
						if(paras.containsKey("onPreExecute")){
							String[] combo = paras.get("onPreExecute").split("&");
							int count = Integer.parseInt(combo[0]);
							String descriptor = combo[1];
							for(int i=0;i<count;i++)
								w.emit(load);
							InvokeInstruction onPreExecute_invoke = InvokeInstruction.make(descriptor, 
									target, 
									"onPreExecute", 
									com.ibm.wala.shrikeBT.IInvokeInstruction.Dispatch.STATIC);
							//System.out.println("onPreExecute_invoke Instruction " + onPreExecute_invoke + "\n");
							w.emit(onPreExecute_invoke);
						}
						
						// invoke the doInBackground(Params... params)
						if(paras.containsKey("doInBackground")){
							String[] combo = paras.get("doInBackground").split("&");
							int count = Integer.parseInt(combo[0]);
							String descriptor = combo[1];
							for(int i=0;i<count;i++)
								w.emit(load);

							InvokeInstruction doInBackground_invoke = InvokeInstruction.make(descriptor, 
									target, 
									"doInBackground", 
									com.ibm.wala.shrikeBT.IInvokeInstruction.Dispatch.STATIC);
							//System.out.println("doInBackground_invoke Instruction " + doInBackground_invoke + "\n");
							w.emit(doInBackground_invoke);
						}

						if(paras.containsKey("onPostExecute")){
							// invoke the onPostExecute(Result result)
							String[] combo = paras.get("onPostExecute").split("&");
							int count = Integer.parseInt(combo[0]);
							String descriptor = combo[1];
							for(int i=0;i<count;i++)
								w.emit(load);

							InvokeInstruction onPostExecute_invoke = InvokeInstruction.make(descriptor,
									target, 
									"onPostExecute", 
									com.ibm.wala.shrikeBT.IInvokeInstruction.Dispatch.STATIC);
							//System.out.println("onPostExecute_invoke Instruction " + onPostExecute_invoke + "\n");
							w.emit(onPostExecute_invoke);
						}

						if(paras.containsKey("onProgressUpdate")){
							// invoke the onProgressUpdate(Progress... values)
							String[] combo = paras.get("onProgressUpdate").split("&");
							int count = Integer.parseInt(combo[0]);
							String descriptor = combo[1];
							for(int i=0;i<count;i++)
								w.emit(load);

							InvokeInstruction onProgressUpdate_invoke = InvokeInstruction.make(descriptor, 
									target, 
									"onProgressUpdate", 
									com.ibm.wala.shrikeBT.IInvokeInstruction.Dispatch.STATIC);
							//System.out.println("onProgressUpdate_invoke Instruction " + onProgressUpdate_invoke + "\n");
							w.emit(onProgressUpdate_invoke);
						}

						if(paras.containsKey("onCancelledWithParameter")){
							// invoke the onCancelled(Result result)
							String[] combo = paras.get("onCancelledWithParameter").split("&");
							int count = Integer.parseInt(combo[0]);
							String descriptor = combo[1];
							for(int i=0;i<count;i++)
								w.emit(load);

							InvokeInstruction onCancelled_invoke = InvokeInstruction.make(descriptor, 
									target, 
									"onCancelled", 
									com.ibm.wala.shrikeBT.IInvokeInstruction.Dispatch.STATIC);
							//System.out.println("onCancelled_invoke Instruction " + onCancelled_invoke + "\n");
							w.emit(onCancelled_invoke);
						}

						if(paras.containsKey("onCancelledNoParameter")){
							// invoke the onCancelled()		
							String[] combo = paras.get("onCancelledNoParameter").split("&");
							int count = Integer.parseInt(combo[0]);
							String descriptor = combo[1];
							for(int i=0;i<count;i++)
								w.emit(load);
							InvokeInstruction emptyCancelled_invoke = InvokeInstruction.make(descriptor,
									target, 
									"onCancelled", 
									com.ibm.wala.shrikeBT.IInvokeInstruction.Dispatch.STATIC);
							//System.out.println("emptyCancelled_invoke Instruction " + emptyCancelled_invoke + "\n");
							w.emit(emptyCancelled_invoke);
						}

					}
				});		
			} 
		}
		//System.out.println("done\n");

	}

	/*
	 * Resolve the Parameter type used in the AsyncTask implementation from the given instructions.
	 */
	private HashMap<String, String> resolveParaTypefromInstruction(String target){
		HashMap<String, String> paras = new HashMap<String, String>();
		Iterator<IClass> iterator = cha.iterator();
		while(iterator.hasNext()){
			IClass cls = iterator.next();
			String cname = cls.getName().toString();
			if((cname+";").equals(target)){
				Collection<IMethod> methods = cls.getDeclaredMethods();
				for(IMethod method : methods){
					if(isPreDefined(method))continue;
					if(method.getName().toString().equals("doInBackground")){
						paras.put("doInBackground", method.getNumberOfParameters() + "&" + method.getDescriptor().toString());
						//System.err.println("doInBackground: " + method.getDescriptor());					
					} else if(method.getName().toString().equals("onPreExecute")){
						paras.put("onPreExecute", method.getNumberOfParameters() + "&" + method.getDescriptor().toString());
						//System.err.println("onPreExecute: " + method.getDescriptor());					
					} else if(method.getName().toString().equals("onPostExecute")){
						paras.put("onPostExecute", method.getNumberOfParameters() + "&" + method.getDescriptor().toString());
						//System.err.println("onPostExecute: " + method.getDescriptor());					
					} else if(method.getName().toString().equals("onCancelled") && method.getNumberOfParameters()!=0){
						paras.put("onCancelledWithParameter", method.getNumberOfParameters() + "&" + method.getDescriptor().toString());
						//System.err.println("onCancelled with parameter: " + method.getDescriptor());					
					} else if(method.getName().toString().equals("onCancelled") && method.getNumberOfParameters()==0){
						paras.put("onCancelledNoParameter", method.getNumberOfParameters() + "&" + method.getDescriptor().toString());
						//System.err.println("onCancelled without parameter: " + method.getDescriptor());					
					} else if(method.getName().toString().equals("onProgressUpdate")){
						paras.put("onProgressUpdate", method.getNumberOfParameters() + "&" + method.getDescriptor().toString());
						//System.err.println("onProgressUpdate: " + method.getDescriptor());					
					}
				}
			}
		}
		return paras;
	}


	/*
	 * The AsyncTask implementation will automatically add the pre-defined 
	 * doInBackground/onPreExecute/onPostExecute/onCancelled/onCancelled(Result result)/onProgressUpdate 
	 * functions. We want to tell whether the current examined function is one of those.
	 * 
	 */
	private boolean isPreDefined(IMethod method){
		IBytecodeMethod m = (IBytecodeMethod)method;
		try {	
			if(m.getInstructions()==null)return false;
			for (IInstruction instruct : m.getInstructions()){
				if(instruct.toString().contains("doInBackground") ||
						instruct.toString().contains("onPreExecute") ||
						instruct.toString().contains("onPostExecute") ||
						instruct.toString().contains("onCancelled") ||
						instruct.toString().contains("onProgressUpdate")){
					return true;
				}								
			}
		} catch (InvalidClassFileException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}


	/*
	 * Resolve the AsyncTask type from the given instructions.
	 */
	private String resolveAsyncTaskTypefromInstruction(IInstruction[] instr, int index, String cname){
		IInstruction target = instr[index];
		if(target instanceof InvokeInstruction){
			InvokeInstruction invoke = (InvokeInstruction)target;
			//if(invoke.getClassType().contains("LocationWorkerHandler"))
			String classType = invoke.getClassType();
			if(classType.equals("Landroid/os/AsyncTask;")){
				//System.err.println("instruction: " + instr[index]);
				String outerName = findOuterClassName(cname);
				classType = lookupClass(outerName);
				//System.err.println("outerclass: " + outerName);
				//System.err.println("class type-2: " + classType);
				return classType;
			} else {
				//System.err.println("class type-3: " + classType + "\n");
				return classType;
			}

		}
		return null;
	}

	/*
	 * Given a class name, find the outer class name.
	 */
	private String findOuterClassName(String cname){
		String target = cname.substring(0, cname.length()-1);
		int index = target.indexOf("$");
		if(index==-1)index=target.length();
		target = target.substring(0, index);
		return target;
	}

	/*
	 * Check whether the given class collections contain the class of
	 * <Application,Ljava/lang/Runnable>.
	 */
	private boolean isImplementingRunnableCallback(Collection<IClass> collection){
		if(collection == null)return false;
		for(IClass cls : collection){
			if(cls.getName().toString().equals("Ljava/lang/Runnable"))
				return true;
		}
		return false;
	}

	/*
	 * Get super-most class name of the given class.
	 */
	private String getSuperMostClassName(IClass cls){
		while(cls.getSuperclass()!=null && !cls.getSuperclass().getName().toString().equals("Ljava/lang/Object"))
			cls = cls.getSuperclass();
		return cls.getName().toString();
	}

	/*
	 * Given the class name, return the internal class (including itself)
	 * if they extends AsyncTask.
	 */
	private String lookupClass(String name){
		Iterator<IClass> iterator = cha.iterator();
		while(iterator.hasNext()){
			IClass cls = iterator.next();
			String cname = cls.getName().toString();
			if(cname.contains(name)){
				//System.err.println("class: " + cname);
				//System.err.println("getSuperMostClassName: " + getSuperMostClassName(cls));
				if(getSuperMostClassName(cls).equals("Landroid/os/AsyncTask")){
					//System.err.println("super class: " + cls.getSuperclass().getName());
					return cname+";";
				}
			}
			//return cls;
		}
		return null;
	}

	/*
	 * Keep the original copy of the method. 
	 * This is a workaround for empty method block.
	 * If we do not apply pathes for one method, the body of that method disappears.
	 */
	private void retainMethod(MethodEditor me){
		IInstruction[] instr = me.getInstructions();
		for (int i = 0; i < instr.length; i++) {
			if(Configuration.DEBUG_ASYNC_REWRITE)
				System.out.println("Instruction " + instr[i] + "\n");
			final IInstruction dup = instr[i];
			me.replaceWith(i,new MethodEditor.Patch() {
				@Override
				public void emitTo(MethodEditor.Output w) {
					w.emit((Instruction) dup);
				}
			});
		}
	}
}
