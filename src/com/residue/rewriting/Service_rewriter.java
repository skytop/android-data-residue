package com.residue.rewriting;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import com.ibm.wala.classLoader.IClass;
import com.ibm.wala.ipa.cha.IClassHierarchy;
import com.ibm.wala.shrikeBT.IInstruction;
import com.ibm.wala.shrikeBT.Instruction;
import com.ibm.wala.shrikeBT.InvokeInstruction;
import com.ibm.wala.shrikeBT.LoadInstruction;
import com.ibm.wala.shrikeBT.MethodData;
import com.ibm.wala.shrikeBT.MethodEditor;
import com.ibm.wala.shrikeBT.NewInstruction;
import com.ibm.wala.shrikeBT.StoreInstruction;
import com.ibm.wala.shrikeBT.shrikeCT.ClassInstrumenter;
import com.ibm.wala.shrikeBT.shrikeCT.OfflineInstrumenter;
import com.ibm.wala.shrikeCT.ClassWriter;
import com.ibm.wala.shrikeCT.InvalidClassFileException;
import com.residue.driver.Configuration;

/*
 * Utility class for rewriting the handler logic in system jar files.
 * It is based on the WALA shrike functionality.
 */
public class Service_rewriter {

	int total = 0;
	int work = 0;
	private OfflineInstrumenter instrumenter;
	private IClassHierarchy cha;
	private String JAR_PATH;
	private File input, output;
	public static HashMap<String,String> serviceList = new HashMap<String,String>();

	public Service_rewriter(IClassHierarchy arg, String path){
		JAR_PATH = path;
		cha = arg;
	}

	/*
	 *  Prepare for the bytecode instrumentation
	 */
	private void prepare() throws IOException{
		instrumenter = new OfflineInstrumenter(false);
		input = new File(JAR_PATH);
		instrumenter.addInputJar(input);
		instrumenter.setPassUnmodifiedClasses(true);
		instrumenter.beginTraversal();
		int pos = JAR_PATH.lastIndexOf(".");
		String name = JAR_PATH.substring(0, pos);
		output = new File(name + "_fixed.jar");
		if(output.exists())output.delete();
		instrumenter.setOutputJar(output);
	}


	/*
	 *  Rewrite each class in the jar. Connect from the bindService API
	 *  to the onServiceConnected API. Also, from the unbindService API
	 *  to the onServiceDisconnected API. Finally, generate the new jar.
	 */
	public void rewrite() throws IllegalStateException, IOException, InvalidClassFileException{
		prepare();
		ClassInstrumenter ci;
		while ((ci = instrumenter.nextClass()) != null) {
			// We skip the android/os/Handler base class.
			if(ci.getReader().getName().equals("android/content/Context"))
				continue;

			final String className = ci.getReader().getName();
			if(Configuration.DEBUG_SERVICE_REWRITE)
				System.out.println("Class: " + className + "\n");

			for (int m = 0; m < ci.getReader().getMethodCount(); m++) {
				MethodData d = ci.visitMethod(m);
				if (d != null ) {
					MethodEditor me = new MethodEditor(d);	
					ArrayList<Integer> list = checkBrokenLinks(me);
					if(Configuration.DEBUG_SERVICE_REWRITE)
						System.out.println("method name: " + ci.getReader().getMethodName(m).toString() + "\n");
					me.beginPass();
					if(list.size()>0){
						for(Integer index : list){
							fixMethod(me, (int)index); // fix the target function
						}
					}else {
						retainMethod(me); // leave other functions unchanged
					}
					me.applyPatches();
					me.endPass();
				}

			}

			ClassWriter cw = ci.emitClass();
			instrumenter.outputModifiedClass(ci, cw);	
		}
		System.out.println("Service rewriting stats: " + work + "/" + total);
		instrumenter.close();

		if(output.renameTo(input)){
			System.out.println("Rewrite succesful");
		}else{
			System.out.println("Rewrite failed");
		}
	}

	/*
	 * Is the current instrumenting method containing sendMessageXYZ() APIs?
	 * If so, we need to bytecode rewrite this method.
	 * Otherwise, retain this method.
	 */
	private ArrayList<Integer> checkBrokenLinks(MethodEditor me) throws InvalidClassFileException{

		ArrayList<Integer> list = new ArrayList<Integer>();
		IInstruction[] instr = me.getInstructions();
		for (int i = 0; i < instr.length; i++) {
			if(instr[i].toString().contains("bindServiceAsUser,(Landroid/content/Intent;Landroid/content/ServiceConnection;ILandroid/os/UserHandle;)Z")
					|| instr[i].toString().contains("unbindService,(Landroid/content/ServiceConnection;)V")
					|| instr[i].toString().contains("bindService,(Landroid/content/Intent;Landroid/content/ServiceConnection;)Z")){
				//if(Configuration.DEBUG_SERVICE_REWRITE)
				//System.out.println("Instruction " + instr[i]);	
				list.add(new Integer(i));
			}
		}
		return list;
	}


	/*
	 * Connect the call from handler.sendMessage() to handler.handleMessage()
	 */
	private void fixMethod(MethodEditor me, final int index){
		final IInstruction[] instr = me.getInstructions();
		int i;
		for (i = 0; i < instr.length; i++) {
			if(Configuration.DEBUG_SERVICE_REWRITE)
				System.out.println("Instruction " + instr[i] + "\n");
			final IInstruction dup = instr[i];

			if(i == index){
				final String cname = me.getData().getClassType();
				if(Configuration.DEBUG_SERVICE_REWRITE)
					System.out.println("Method <" + me.getData().getName() + 
							"> in Class <" + cname + ">\n");

				me.insertAfter(i,new MethodEditor.Patch() {
					@Override
					public void emitTo(MethodEditor.Output w) {
						String target = resolveServiceConnectionTypefromInstruction(instr, index, cname);
						//System.err.println("target: " + target);
						total++;
						if(target!=null)work++;
						else target = "Landroid/content/ServiceConnection;";

						serviceList.put(target,cname);

						NewInstruction para_object = NewInstruction.make("Ljava/lang/Object;", 0);
						//System.out.println("para_object Instruction " + para_object + "\n");
						w.emit(para_object);

						StoreInstruction store = StoreInstruction.make("Ljava/lang/Object;", 25);
						//System.out.println("store Instruction " + store + "\n");
						w.emit(store);

						LoadInstruction load = LoadInstruction.make("Ljava/lang/Object;", 25);						
						w.emit(load);

						if(dup.toString().contains("unbindService")){
							InvokeInstruction invoke = InvokeInstruction.make("(Landroid/content/ComponentName;)V",
									target, 
									"onServiceDisconnected", 
									com.ibm.wala.shrikeBT.IInvokeInstruction.Dispatch.STATIC);
							//System.out.println("invoke Instruction " + invoke + "\n");
							w.emit(invoke);
						} else {
							w.emit(load);
							InvokeInstruction invoke = InvokeInstruction.make("(Landroid/content/ComponentName;Landroid/os/IBinder;)V",
									target, 
									"onServiceConnected", 
									com.ibm.wala.shrikeBT.IInvokeInstruction.Dispatch.STATIC);
							//System.out.println("invoke Instruction " + invoke + "\n");
							w.emit(invoke);
						}

					}
				});		
			} 
		}

	}

	/*
	 * Resolve the handler type from the given instructions.
	 */
	private String resolveServiceConnectionTypefromInstruction(IInstruction[] instr, int index, String cname){
		IInstruction target = instr[index];
		if(target instanceof InvokeInstruction){
			InvokeInstruction invoke = (InvokeInstruction)target;
			String classType = invoke.getClassType();
			String outerName = findOuterClassName(cname);
			classType = lookupClass(outerName);
			return classType;
		}
		return null;
	}

	/*
	 * Given a class name, find the outer class name.
	 */
	private String findOuterClassName(String cname){
		String target = cname.substring(0, cname.length()-1);
		int index = target.indexOf("$");
		if(index==-1)index=target.length();
		target = target.substring(0, index);
		return target;
	}

	/*
	 * Check whether the given class collections contain the class of
	 * <Application,Landroid/content/ServiceConnection>.
	 */
	private boolean isImplementingServiceCallback(Collection<IClass> collection){
		if(collection == null)return false;
		for(IClass cls : collection){
			if(cls.getName().toString().equals("Landroid/content/ServiceConnection")){
				return true;
			}
		}
		return false;
	}

	/*
	 * Given the class name, return the internal class (including itself)
	 * if they extends Handler or implements Handler$Callback.
	 */
	private String lookupClass(String name){
		Iterator<IClass> iterator = cha.iterator();
		while(iterator.hasNext()){
			IClass cls = iterator.next();
			String cname = cls.getName().toString();
			if(cname.contains(name)){
				if(isImplementingServiceCallback(cls.getAllImplementedInterfaces())){
					return cname+";";
				}
			}
		}
		return null;
	}

	/*
	 * Keep the original copy of the method. 
	 * This is a workaround for empty method block.
	 * If we do not apply pathes for one method, the body of that method disappears.
	 */
	private void retainMethod(MethodEditor me) throws IOException{
		IInstruction[] instr = me.getInstructions();			

		for (int i = 0; i < instr.length; i++) {
			if(Configuration.DEBUG_SERVICE_REWRITE)
				System.out.println("Instruction " + instr[i] + "\n");			
			final IInstruction dup = instr[i];
			me.replaceWith(i,new MethodEditor.Patch() {
				@Override
				public void emitTo(MethodEditor.Output w) {
					w.emit((Instruction) dup);
				}
			});
		}
	}
}
