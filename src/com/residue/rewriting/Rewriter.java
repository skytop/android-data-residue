package com.residue.rewriting;

import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import com.ibm.wala.classLoader.IClass;
import com.ibm.wala.ipa.cha.IClassHierarchy;
import com.ibm.wala.shrikeCT.InvalidClassFileException;
import com.residue.driver.Configuration;
import com.residue.driver.Test;

public class Rewriter {
	private IClassHierarchy cha;
	private String JAR_PATH;

	public Rewriter(IClassHierarchy arg, String path){
		JAR_PATH = path;
		cha = arg;
	}

	/*
	 * Given a class name, find the outer class name.
	 * Used to detect all internal classes.
	 */
	private String findOuterClassName(String cname){
		String target = cname;
		int index = target.indexOf("$");
		if(index==-1)index=target.length();
		target = target.substring(0, index);
		return target;
	}

	public void rewrite() throws IllegalStateException, IOException, InvalidClassFileException{

		Handler_rewriter handler_rewriter = new Handler_rewriter(cha, JAR_PATH);
		handler_rewriter.rewrite();
		System.out.println("Handler size: " + Handler_rewriter.handlerList.keySet().size());

		Thread_rewriter thread_rewriter = new Thread_rewriter(cha, JAR_PATH);
		thread_rewriter.rewrite();
		System.out.println("Thread size: " + Thread_rewriter.threadList.size());

		AsncTask_rewriter async_rewriter = new AsncTask_rewriter(cha, JAR_PATH);
		async_rewriter.rewrite();
		System.out.println("AsyncTask size: " + AsncTask_rewriter.asyncList.size());
		
		Service_rewriter service_rewriter = new Service_rewriter(cha, JAR_PATH);
		service_rewriter.rewrite();
		System.out.println("Service size: " + Service_rewriter.serviceList.size());

		// Only rewrite functions inside services-dex2jar to bridge broken links if the flag is turned on
		if(Configuration.ENABLE_GENERIC_REWRITER && JAR_PATH.contains("services-dex2jar.jar")){
			Set<String> services = Test.SSClasses.keySet();
			Collection<String> targets = new HashSet<String>();
			for(String service : services)
				targets.add(findOuterClassName(service));
			for(String service : targets){
				Generic_rewriter generic_rewriter = new Generic_rewriter(cha, JAR_PATH, service);
				generic_rewriter.rewrite();
			}
		}

	}
}
