package com.residue.rewriting;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.commons.io.FilenameUtils;

import com.ibm.wala.classLoader.IClass;
import com.ibm.wala.ipa.cha.IClassHierarchy;
import com.ibm.wala.shrikeBT.DupInstruction;
import com.ibm.wala.shrikeBT.GetInstruction;
import com.ibm.wala.shrikeBT.IInstruction;
import com.ibm.wala.shrikeBT.Instruction;
import com.ibm.wala.shrikeBT.InvokeInstruction;
import com.ibm.wala.shrikeBT.LoadInstruction;
import com.ibm.wala.shrikeBT.MethodData;
import com.ibm.wala.shrikeBT.MethodEditor;
import com.ibm.wala.shrikeBT.NewInstruction;
import com.ibm.wala.shrikeBT.PopInstruction;
import com.ibm.wala.shrikeBT.StoreInstruction;
import com.ibm.wala.shrikeBT.shrikeCT.ClassInstrumenter;
import com.ibm.wala.shrikeBT.shrikeCT.OfflineInstrumenter;
import com.ibm.wala.shrikeCT.ClassWriter;
import com.ibm.wala.shrikeCT.InvalidClassFileException;
import com.ibm.wala.types.ClassLoaderReference;
import com.ibm.wala.types.TypeReference;
import com.residue.driver.Configuration;


/*
 * Utility class for rewriting the thread logic in system jar files.
 * It is based on the WALA shrike functionality, and reconnects
 * the missing link between thread start and thread run.
 */
public class Thread_rewriter {

	int total = 0;
	int work = 0;
	private OfflineInstrumenter instrumenter;
	private IClassHierarchy cha;
	private String JAR_PATH;
	private File input, output;
	public static HashMap<String,String> threadList = new HashMap<String,String>();
	
	public Thread_rewriter(IClassHierarchy arg, String path){
		JAR_PATH = path;
		cha = arg;
	}
	
	/*
	 *  Prepare for the bytecode instrumentation
	 */
	private void prepare() throws IOException{
		//lookupClass("KeyguardUpdateMonitor");
		instrumenter = new OfflineInstrumenter(false);
		//instrumenter.addInputClass(new File("./dat/data_residue/"), new File("./dat/data_residue/AlarmManagerService.class"));
		input = new File(JAR_PATH);
		instrumenter.addInputJar(input);
		instrumenter.setPassUnmodifiedClasses(true);
		instrumenter.beginTraversal();
		int pos = JAR_PATH.lastIndexOf(".");
		String name = JAR_PATH.substring(0, pos);
		output = new File(name + "_fixed.jar");
		if(output.exists())output.delete();
		instrumenter.setOutputJar(output);
	}


	/*
	 *  Rewrite each class in the jar. Connect from the start() API
	 *  to the run() API. Finally, generate the new jar.
	 */
	public void rewrite() throws IllegalStateException, IOException, InvalidClassFileException{
		prepare();
		ClassInstrumenter ci;
		while ((ci = instrumenter.nextClass()) != null) {
			// We skip the android/os/Handler base class.
			if(ci.getReader().getName().equals("java/lang/Thread"))
				continue;

			final String className = ci.getReader().getName();
			if(Configuration.DEBUG_THREAD_REWRITE)
				System.out.println("Class: " + className + "\n");
			for (int m = 0; m < ci.getReader().getMethodCount(); m++) {
				MethodData d = ci.visitMethod(m);
				if (d != null ) {
					MethodEditor me = new MethodEditor(d);	
					ArrayList<Integer> list = checkBrokenLinks(me);
					if(Configuration.DEBUG_THREAD_REWRITE)
						System.out.println("method name: " + ci.getReader().getMethodName(m).toString() + "\n");
					me.beginPass();
					if(list.size()>0){
						//System.out.println("method name: " + ci.getReader().getMethodName(m).toString());
						//System.out.println("Class: " + className + "\n");
						for(Integer index : list){
							fixMethod(me, (int)index); // fix the target function
						}
					}else {
						retainMethod(me); // leave other functions unchanged
					}
					me.applyPatches();
					me.endPass();
				}
				
			}

			ClassWriter cw = ci.emitClass();
			instrumenter.outputModifiedClass(ci, cw);	
		}
		System.out.println("Thread rewriting stats: " + work + "/" + total);
		instrumenter.close();
		
		if(output.renameTo(input)){
			System.out.println("Rewrite succesful");
		}else{
			System.out.println("Rewrite failed");
		}
	}

	/*
	 * Is the current instrumenting method containing Thread.start() APIs?
	 * If so, we need to bytecode rewrite this method.
	 * Otherwise, retain this method.
	 */
	private ArrayList<Integer> checkBrokenLinks(MethodEditor me) throws InvalidClassFileException{

		ArrayList<Integer> list = new ArrayList<Integer>();
		IInstruction[] instr = me.getInstructions();
		for (int i = 0; i < instr.length; i++) {
			if(instr[i].toString().contains("start,()V")
					|| instr[i].toString().contains("execute,(Ljava/lang/Runnable;)V")
					|| instr[i].toString().contains("post,(Ljava/lang/Runnable;)Z")){
				if(Configuration.DEBUG_THREAD_REWRITE)
					System.out.println("Instruction " + instr[i]);	
				list.add(new Integer(i));
			}
		}
		return list;
	}


	/*
	 * Connect the call from Thread.start() to Thread.run()
	 */
	private void fixMethod(MethodEditor me, final int index){
		final IInstruction[] instr = me.getInstructions();
		int i;
		for (i = 0; i < instr.length; i++) {
			if(Configuration.DEBUG_THREAD_REWRITE)
				System.out.println("Instruction " + instr[i] + "\n");
			//final IInstruction dup = instr[i];

			if(i == index){
				final String cname = me.getData().getClassType();
				if(Configuration.DEBUG_THREAD_REWRITE)
					System.out.println("Method <" + me.getData().getName() + 
						"> in Class <" + cname + ">\n");
				
				me.insertAfter(i,new MethodEditor.Patch() {
					@Override
					public void emitTo(MethodEditor.Output w) {
						String target = resolveThreadTypefromInstruction(instr, index, cname);
						total++;
						if(target!=null)work++;
						else target = "Ljava/lang/Thread;";
						
						//System.err.println("Target Type: " + target);
						
						threadList.put(target,cname);
						
						// invoke the Thread.run() api as a static function
						InvokeInstruction invoke1 = InvokeInstruction.make("()V", 
								target, 
								"run", 
								com.ibm.wala.shrikeBT.IInvokeInstruction.Dispatch.STATIC);
						//System.out.println("invoke Instruction " + invoke1 + "\n");
						w.emit(invoke1);

					}
				});		
			} 
		}

	}

	/*
	 * Resolve the Thread type from the given instructions.
	 */
	private String resolveThreadTypefromInstruction(IInstruction[] instr, int index, String cname){
		IInstruction target = instr[index];
		if(target instanceof InvokeInstruction){
			InvokeInstruction invoke = (InvokeInstruction)target;
			//if(invoke.getClassType().contains("LocationWorkerHandler"))
			String classType = invoke.getClassType();
			if(classType.equals("Ljava/lang/Thread;") ||
					classType.equals("Landroid/os/AsyncTask;") || 
					classType.equals("Landroid/os/Handler;")){
				//System.err.println("instruction: " + instr[index]);
				String outerName = findOuterClassName(cname);
				classType = lookupClass(outerName);
				//System.err.println("outerclass: " + outerName);
				//System.err.println("class type-2: " + classType);
				return classType;
			} else {
				//System.err.println("class type-3: " + classType + "\n");
				return classType;
			}
			
		}
		return null;
	}

	/*
	 * Given a class name, find the outer class name.
	 */
	private String findOuterClassName(String cname){
		String target = cname.substring(0, cname.length()-1);
		int index = target.indexOf("$");
		if(index==-1)index=target.length();
		target = target.substring(0, index);
		return target;
	}
	
	/*
	 * Check whether the given class collections contain the class of
	 * <Application,Ljava/lang/Runnable>.
	 */
	private boolean isImplementingRunnableCallback(Collection<IClass> collection){
		if(collection == null)return false;
		for(IClass cls : collection){
			if(cls.getName().toString().equals("Ljava/lang/Runnable"))
				return true;
		}
		return false;
	}
	
	/*
	 * Get super-most class name of the given class.
	 */
	private String getSuperMostClassName(IClass cls){
		while(cls.getSuperclass()!=null)cls = cls.getSuperclass();
		return cls.getName().toString();
	}
	
	/*
	 * Given the class name, return the internal class (including itself)
	 * if they extends Thread or implements Runnable.
	 */
	private String lookupClass(String name){
		Iterator<IClass> iterator = cha.iterator();
		while(iterator.hasNext()){
			IClass cls = iterator.next();
			String cname = cls.getName().toString();
			if(cname.contains(name)){
				//System.err.println("class: " + cname);
				//System.err.println("superclass: " + cls.getSuperclass().getName());
				if(getSuperMostClassName(cls).equals("Ljava/lang/Thread") ||
						isImplementingRunnableCallback(cls.getAllImplementedInterfaces())){
					return cname+";";
				}
			}
				//return cls;
		}
		return null;
	}
	
	/*
	 * Keep the original copy of the method. 
	 * This is a workaround for empty method block.
	 * If we do not apply pathes for one method, the body of that method disappears.
	 */
	private void retainMethod(MethodEditor me){
		IInstruction[] instr = me.getInstructions();
		for (int i = 0; i < instr.length; i++) {
			if(Configuration.DEBUG_HANDLER_REWRITE)
				System.out.println("Instruction " + instr[i] + "\n");
			final IInstruction dup = instr[i];
			me.replaceWith(i,new MethodEditor.Patch() {
				@Override
				public void emitTo(MethodEditor.Output w) {
					w.emit((Instruction) dup);
				}
			});
		}
	}
}
