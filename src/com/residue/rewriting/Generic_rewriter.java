package com.residue.rewriting;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

import com.ibm.wala.classLoader.IBytecodeMethod;
import com.ibm.wala.classLoader.IClass;
import com.ibm.wala.classLoader.IMethod;
import com.ibm.wala.ipa.cha.IClassHierarchy;
import com.ibm.wala.ipa.slicer.Statement;
import com.ibm.wala.shrikeBT.ArrayLoadInstruction;
import com.ibm.wala.shrikeBT.ArrayStoreInstruction;
import com.ibm.wala.shrikeBT.IInstruction;
import com.ibm.wala.shrikeBT.Instruction;
import com.ibm.wala.shrikeBT.InvokeInstruction;
import com.ibm.wala.shrikeBT.LoadInstruction;
import com.ibm.wala.shrikeBT.MethodData;
import com.ibm.wala.shrikeBT.MethodEditor;
import com.ibm.wala.shrikeBT.NewInstruction;
import com.ibm.wala.shrikeBT.StoreInstruction;
import com.ibm.wala.shrikeBT.shrikeCT.ClassInstrumenter;
import com.ibm.wala.shrikeBT.shrikeCT.OfflineInstrumenter;
import com.ibm.wala.shrikeCT.ClassWriter;
import com.ibm.wala.shrikeCT.InvalidClassFileException;
import com.ibm.wala.types.TypeReference;
import com.residue.driver.Configuration;
import com.residue.driver.Test;

public class Generic_rewriter {

	private OfflineInstrumenter instrumenter;
	private File input, output;
	private IClassHierarchy cha;
	private String JAR_PATH;
	private FileWriter fw;
	private String rewritingService;
	private HashMap<IClass, Collection<IClass>> allInterfaces =
			new HashMap<IClass, Collection<IClass>>();
	private HashMap<IClass, Collection<IClass>> allAbstracts =
			new HashMap<IClass, Collection<IClass>>();	
	private HashMap<String, Collection<IClass>> targetMethods = 
			new HashMap<String, Collection<IClass>>();

	public Generic_rewriter(IClassHierarchy arg, String path, String str){
		JAR_PATH = path;
		cha = arg;
		rewritingService = str;
	}	

	/*
	 *  Prepare for the bytecode instrumentation
	 */
	private void prepare() throws IOException{
		instrumenter = new OfflineInstrumenter(false);
		input = new File(JAR_PATH);
		instrumenter.addInputJar(input);
		instrumenter.setPassUnmodifiedClasses(true);
		instrumenter.beginTraversal();
		int pos = JAR_PATH.lastIndexOf(".");
		String name = JAR_PATH.substring(0, pos);
		output = new File(name + "_fixed.jar");
		if(output.exists())output.delete();
		instrumenter.setOutputJar(output);

		try
		{
			String filename= "./out/DEBUG/Instructions.txt";
			fw = new FileWriter(filename,true); 
		} catch(IOException ioe){
			System.err.println("IOException: " + ioe.getMessage());
		}
	}

	/*
	 *  Rewrite each class in the jar. Connect from the sendMessageXYZ() API
	 *  to the handleMessage() API. Finally, generate the new jar.
	 */
	public void rewrite() throws IllegalStateException, IOException, InvalidClassFileException, ArrayIndexOutOfBoundsException{
		prepare();
		fw.write("====== System Service: " + rewritingService + "\n");
		checkBrokenPoints(rewritingService);
		collectTargetInstructions(rewritingService);

		ClassInstrumenter ci;
		while ((ci = instrumenter.nextClass()) != null) {
			
			final String className = ci.getReader().getName();
						
			if(!className.contains(rewritingService.substring(1, rewritingService.length())))
				continue;
			fw.write("Class: " + className + "\n");
			for (int m = 0; m < ci.getReader().getMethodCount(); m++) {
				MethodData d = ci.visitMethod(m);
				if (d != null ) {
					String classType = d.getClassType();
					classType = classType.replace('/', '.');
					String name = classType.substring(1, classType.length()-1) + "." + d.getName() + d.getSignature();
					MethodEditor me = new MethodEditor(d);	
					//fw.write("method name: " + ci.getReader().getMethodName(m).toString() + "\n");
					me.beginPass();	
					if(targetMethods.containsKey(name) && targetMethods.get(name).size()>0){
						fixMethod(me, name);
					} else {
						retainMethod(me); // leave other functions unchanged
					}
					me.applyPatches();
					me.endPass();
				}

			}
			ClassWriter cw = ci.emitClass();
			instrumenter.outputModifiedClass(ci, cw);	
		}
		
		instrumenter.close();
		fw.close();
		
		if(output.renameTo(input)){
			System.out.println("Rewrite succesful");
		}else{
			System.out.println("Rewrite failed");
		}
	}

	/*
	 * Bridge all broken connections.
	 */
	private void fixMethod(MethodEditor me, String signature) throws IOException{
		Collection<IClass> brokenClasses = targetMethods.get(signature);
		Collection<IMethod> missingMethods = new HashSet<IMethod>();
		fw.write("fixing method: " + signature + "\n");
		for(IClass c : brokenClasses){
			Collection<String> superMethods = new HashSet<String>();
			Collection<IClass> superClasses = new HashSet<IClass>();
			Set<String> services = Test.SSClasses.keySet();
			if(allInterfaces.get(c)!=null){
				for(IClass cls : allInterfaces.get(c)){
					if(!services.contains(cls.getName().toString()))
						superClasses.add(cls);
				}
			}
			if(allAbstracts.get(c)!=null){
				for(IClass cls : allAbstracts.get(c)){
					if(!services.contains(cls.getName().toString()))
						superClasses.add(cls);
				}
			}
			for(IClass cls : superClasses){
				for(IMethod mth : cls.getDeclaredMethods())
					superMethods.add(mth.getName().toString());
			}
			for(IMethod m : c.getDeclaredMethods()){
				if(m.isPublic() && !m.isSynthetic()
						&& superMethods.contains(m.getName().toString()))
					missingMethods.add(m);
			}
		}
		for(final IMethod mm : missingMethods){
			final int numOfArgs = mm.getNumberOfParameters();
			final String descriptor = mm.getDescriptor().toString();
			//if(descriptor.contains("["))continue;
			final String methodName = mm.getName().toString();
			final String callingClass =  mm.getDeclaringClass().getName().toString() + ";";
			
			me.insertAtStart(new MethodEditor.Patch() {
				@Override
				public void emitTo(MethodEditor.Output w) {
				
					System.out.println("INFO: " + callingClass + " : " + methodName + " : " + descriptor + " : " + numOfArgs + "\n");
					
					NewInstruction para_object = NewInstruction.make("Ljava/lang/Object;", 0);
					System.out.println("para_object Instruction " + para_object + "\n");
					w.emit(para_object);

					StoreInstruction store = StoreInstruction.make("Ljava/lang/Object;", 25);
					System.out.println("store Instruction " + store + "\n");
					w.emit(store);

					LoadInstruction load = LoadInstruction.make("Ljava/lang/Object;", 25);
					//System.out.println("load Instruction " + load + "\n");
					for(int i=0; i< numOfArgs; i++){
						System.out.println("type: " + mm.getParameterType(i).getName().toString() + "\n");
						System.out.println("class: " + mm.getDeclaringClass().getName().toString() + "\n");
						if(i==0 && mm.getDeclaringClass().getName().toString().equals(mm.getParameterType(i).getName().toString()))
							continue;
						System.out.println("load Instruction " + load + "\n");
						w.emit(load);
					}
					/*if(!callingClass.contains("$"))	{
						System.out.println("load Instruction " + load + "\n");
						w.emit(load);	
					}*/
					
					InvokeInstruction invoke = InvokeInstruction.make(descriptor,
							callingClass, 
							methodName, 
							com.ibm.wala.shrikeBT.IInvokeInstruction.Dispatch.STATIC);
					System.out.println("invoke Instruction " + invoke + "\n");
					w.emit(invoke);
					
					if(descriptor.charAt(descriptor.length()-1)!='V'){
						StoreInstruction store2 = StoreInstruction.make("Ljava/lang/Object;", 25);
						System.out.println("store Instruction " + store2 + "\n");
						w.emit(store2);
					}

				}
			});	
			fw.write("     calling class: " + callingClass + "\n");
			fw.write("     adding method: " + mm.getSignature() + "\n");
		}
	}
	
	/*
	 * Collect all instructions that may register the broken points.
	 * Record their method names, and then rewrite those methods later on.
	 */
	private void collectTargetInstructions(String className){
		for(IClass cls : cha){
			if(cls.getName().toString().contains(findOuterClassName(rewritingService))){
				for (IMethod m : cls.getDeclaredMethods()){	
					String methodName = m.getSignature().toString();
					IBytecodeMethod method = (IBytecodeMethod)m;
					try {
						if(method!=null){							
							fw.write("Method: " + methodName + "\n");
							IInstruction instr[] = method.getInstructions();
							if(instr==null)continue;
							for (IInstruction ins : instr) {
								if(ins.getClass().getName().contains("com.ibm.wala.shrikeBT.InvokeInstruction")){
									com.ibm.wala.shrikeBT.InvokeInstruction call = (com.ibm.wala.shrikeBT.InvokeInstruction) ins;
									String callingClass = call.getClassType();
									callingClass = callingClass.substring(0, callingClass.length()-1);
									String signature = call.getMethodSignature();
									ArrayList<String> args = getArgsFromSignature(signature);
									/*fw.write("	" + call + "\n");
									fw.write("		classType: " + callingClass + "\n");
									fw.write("		methodName: " + call.getMethodName() + "\n");
									if(args.size()>0)
										fw.write("		args: " + args + "\n");*/

									/*
									 *  We compare the calling class name against all the broken abstract class names.
									 *  Whenever there is a match, we save it for future rewriting inside the current method.
									 */									
									Collection<IClass> needRewrite = new HashSet<IClass>();
									if(call.getMethodName().equals("<init>")){
										for (IClass abstractClass : allAbstracts.keySet()){
											if(abstractClass.getName().toString().equals(callingClass)){
												needRewrite.add(abstractClass);
											}
										}
									}

									/*
									 *  We compare the args name against all the broken interface names.
									 *  Whenever there is a match, we save it for future rewriting inside the current method.
									 */
									for (String arg : args){
										for (IClass interfaceClass : allInterfaces.keySet()){
											Collection<IClass> classes = allInterfaces.get(interfaceClass);
											for(IClass perClass : classes){
												if(perClass.getName().toString().equals(arg)){
													needRewrite.add(interfaceClass);
												}
											}
										}
									}

									/*
									 * Save all broken classes that need to be rewritten in
									 * the current method for future handling. 
									 */
									if(!targetMethods.containsKey(methodName)){
										targetMethods.put(methodName, needRewrite);										
									}else {
										Collection<IClass> union = targetMethods.get(methodName);
										union.addAll(needRewrite);
										targetMethods.put(methodName, union);
									}									

								}
							}
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					try {
						Collection<IClass> debugClasses = targetMethods.get(methodName);
						if(debugClasses!=null && debugClasses.size()>0){
							for(IClass debugClass : debugClasses)
								fw.write("---" + debugClass + "\n");
						}
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
	}

	/*
	 * Given a method signature, return its argumetn types.
	 */
	private ArrayList<String> getArgsFromSignature(String signature){
		int start = signature.indexOf('(');
		int end = signature.indexOf(')');
		String types = signature.substring(start, end);
		String results[] = types.split(";");
		if(results==null)return null;
		ArrayList<String> args = new ArrayList<String>();
		for(int i=0;i<results.length;i++){
			if(!results[i].contains("android"))
				continue;
			int begin = results[i].indexOf('L');
			if(begin!=-1)
				args.add(results[i].substring(begin));
		}
		return args;
	}

	/*
	 * Debug the collected broken points. Print them out in the console.
	 */
	private void debugBrokenPoints(){			
		for(Entry<IClass,  Collection<IClass>> broken : allInterfaces.entrySet()){	
			String cname = broken.getKey().getName().toString();
			System.err.println(cname + ": " + broken.getValue() + " (interface)");
		}
		for(Entry<IClass,  Collection<IClass>> broken : allAbstracts.entrySet()){	
			String cname = broken.getKey().getName().toString();
			System.err.println(cname + ": " + broken.getValue() + " (abstract)");
		}
	}

	/*
	 * Get a list of internal classes from the given SystemService class, which
	 * potentially can lead to broken links.
	 */
	private void checkBrokenPoints(String serviceName){
		System.out.println("service class: " + serviceName);
		Iterator<IClass> iterator = cha.iterator();
		while(iterator.hasNext()){
			IClass c = iterator.next();
			String cname = c.getName().toString();
			if(cname.equals(serviceName)){
				//System.err.println("--->internal class: " + cname);
				if(c.getDirectInterfaces().size()>0){
					Collection<IClass> interfaces = (Collection<IClass>) c.getDirectInterfaces();
					Collection<IClass> dup = new HashSet<IClass>(interfaces);
					for(IClass mInterface : interfaces){
						if(mInterface.getName().toString().equals("Landroid/os/Handler$Callback")
								|| mInterface.getName().toString().equals("Ljava/lang/Runnable")
								|| mInterface.getName().toString().equals("Lcom/android/internal/os/HandlerCaller$Callback"))
							dup.remove(mInterface);
					}
					allInterfaces.put(c, dup);
					//System.err.println("	--->interfaces: " + c.getDirectInterfaces());
				}
			} else if(cname.contains(findOuterClassName(serviceName))){
				//System.err.println("--->internal class: " + cname);
				if(c.getDirectInterfaces().size()>0){
					Collection<IClass> interfaces = (Collection<IClass>) c.getDirectInterfaces();
					Collection<IClass> dup = new HashSet<IClass>(interfaces);
					for(IClass mInterface : interfaces){
						if(mInterface.getName().toString().equals("Landroid/os/Handler$Callback")
								|| mInterface.getName().toString().equals("Ljava/lang/Runnable"))
							dup.remove(mInterface);
					}
					allInterfaces.put(c, dup);
				}
				Collection<IClass> collection = extendsAbstractClass(c);
				if(collection.size()>0){
					allAbstracts.put(c, collection);
				}
			}
		}
		debugBrokenPoints();
	}

	/*
	 * Is the given class containing any method with concrete implementation?
	 * The constructors are not considered
	 */
	private boolean containsConcreteMethod(IClass cls){
		for (IMethod m : cls.getDeclaredMethods()){
			IBytecodeMethod method = (IBytecodeMethod)m;
			try {
				if(method!=null && !method.getName().toString().contains("init")
						&& method.getInstructions()!=null && method.getInstructions().length>1){	
					return true;
				}
			} catch (InvalidClassFileException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.err.println("No concrete method implementation in class: " + cls.getName());
		return false;
	}

	/*
	 * Is the given class extending an abstract class at some point on
	 * the class hierarchy?
	 */
	private Collection<IClass> extendsAbstractClass(IClass cls){
		Collection<IClass> collection = new HashSet<IClass>();
		while(cls.getSuperclass()!=null){
			cls = cls.getSuperclass();
			if(cls.isAbstract()){
				collection.add(cls);
			}
		}
		return collection;
	}

	/*
	 * Given a class name, find the outer class name.
	 * Used to detect all internal classes.
	 */
	private String findOuterClassName(String cname){
		String target = cname;
		int index = target.indexOf("$");
		if(index==-1)index=target.length();
		target = target.substring(0, index);
		return target;
	}

	/*
	 * Keep the original copy of the method. 
	 * This is a workaround for empty method block.
	 * If we do not apply pathes for one method, the body of that method disappears.
	 */
	private void retainMethod(MethodEditor me) throws IOException{
		IInstruction[] instr = me.getInstructions();			

		for (int i = 0; i < instr.length; i++) {
			if(Configuration.DEBUG_HANDLER_REWRITE)
				fw.write("Instruction: " + instr[i] + "\n");			
			final IInstruction dup = instr[i];
			me.replaceWith(i,new MethodEditor.Patch() {
				@Override
				public void emitTo(MethodEditor.Output w) {
					w.emit((Instruction) dup);
				}
			});
		}
	}


}
